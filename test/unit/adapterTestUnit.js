/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-delinea',
      type: 'Delinea',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Delinea = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Delinea Adapter Test', () => {
  describe('Delinea Class Tests', () => {
    const a = new Delinea(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('delinea'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('delinea'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Delinea', pronghornDotJson.export);
          assert.equal('Delinea', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-delinea', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('delinea'));
          assert.equal('Delinea', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-delinea', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-delinea', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#activationsServiceActivate - errors', () => {
      it('should have a activationsServiceActivate function', (done) => {
        try {
          assert.equal(true, typeof a.activationsServiceActivate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activeDirectoryServiceSynchronize - errors', () => {
      it('should have a activeDirectoryServiceSynchronize function', (done) => {
        try {
          assert.equal(true, typeof a.activeDirectoryServiceSynchronize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#apiTokenServiceGetToken - errors', () => {
      it('should have a apiTokenServiceGetToken function', (done) => {
        try {
          assert.equal(true, typeof a.apiTokenServiceGetToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appClientsServiceGetClient - errors', () => {
      it('should have a appClientsServiceGetClient function', (done) => {
        try {
          assert.equal(true, typeof a.appClientsServiceGetClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.appClientsServiceGetClient('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-appClientsServiceGetClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appClientsServiceUpdateClient - errors', () => {
      it('should have a appClientsServiceUpdateClient function', (done) => {
        try {
          assert.equal(true, typeof a.appClientsServiceUpdateClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.appClientsServiceUpdateClient(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-appClientsServiceUpdateClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appClientsServiceDeleteClient - errors', () => {
      it('should have a appClientsServiceDeleteClient function', (done) => {
        try {
          assert.equal(true, typeof a.appClientsServiceDeleteClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.appClientsServiceDeleteClient(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-appClientsServiceDeleteClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appClientsServiceStub - errors', () => {
      it('should have a appClientsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.appClientsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appClientsServiceCreateClient - errors', () => {
      it('should have a appClientsServiceCreateClient function', (done) => {
        try {
          assert.equal(true, typeof a.appClientsServiceCreateClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationAccountsServiceLookupAccount - errors', () => {
      it('should have a applicationAccountsServiceLookupAccount function', (done) => {
        try {
          assert.equal(true, typeof a.applicationAccountsServiceLookupAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationRequestServiceSearchRequestsByStatus - errors', () => {
      it('should have a applicationRequestServiceSearchRequestsByStatus function', (done) => {
        try {
          assert.equal(true, typeof a.applicationRequestServiceSearchRequestsByStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationRequestServiceGetRequestByDeviceId - errors', () => {
      it('should have a applicationRequestServiceGetRequestByDeviceId function', (done) => {
        try {
          assert.equal(true, typeof a.applicationRequestServiceGetRequestByDeviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.applicationRequestServiceGetRequestByDeviceId(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-applicationRequestServiceGetRequestByDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationRequestServiceUpdateRequest - errors', () => {
      it('should have a applicationRequestServiceUpdateRequest function', (done) => {
        try {
          assert.equal(true, typeof a.applicationRequestServiceUpdateRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.applicationRequestServiceUpdateRequest(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-applicationRequestServiceUpdateRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationRequestServiceGetAudits - errors', () => {
      it('should have a applicationRequestServiceGetAudits function', (done) => {
        try {
          assert.equal(true, typeof a.applicationRequestServiceGetAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceGetList - errors', () => {
      it('should have a categorizedListsServiceGetList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceGetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceGetList(null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceGetList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceUpdateList - errors', () => {
      it('should have a categorizedListsServiceUpdateList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceUpdateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceUpdateList(null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceUpdateList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceDeleteList - errors', () => {
      it('should have a categorizedListsServiceDeleteList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceDeleteList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceDeleteList(null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceDeleteList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceGetListItems - errors', () => {
      it('should have a categorizedListsServiceGetListItems function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceGetListItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceGetListItems(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceGetListItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceUpdateItemsInList - errors', () => {
      it('should have a categorizedListsServiceUpdateItemsInList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceUpdateItemsInList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceUpdateItemsInList(null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceUpdateItemsInList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceAddItemsToList - errors', () => {
      it('should have a categorizedListsServiceAddItemsToList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceAddItemsToList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceAddItemsToList(null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceAddItemsToList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceRemoveItemsFromList - errors', () => {
      it('should have a categorizedListsServiceRemoveItemsFromList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceRemoveItemsFromList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceRemoveItemsFromList(null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceRemoveItemsFromList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceRemoveItemFromList - errors', () => {
      it('should have a categorizedListsServiceRemoveItemFromList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceRemoveItemFromList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing listId', (done) => {
        try {
          a.categorizedListsServiceRemoveItemFromList(null, null, (data, error) => {
            try {
              const displayE = 'listId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceRemoveItemFromList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.categorizedListsServiceRemoveItemFromList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'optionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceRemoveItemFromList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceSearch - errors', () => {
      it('should have a categorizedListsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceCreateList - errors', () => {
      it('should have a categorizedListsServiceCreateList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceCreateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceGetAllListsUserMaySee - errors', () => {
      it('should have a categorizedListsServiceGetAllListsUserMaySee function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceGetAllListsUserMaySee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceGetCategoriesForList - errors', () => {
      it('should have a categorizedListsServiceGetCategoriesForList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceGetCategoriesForList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceGetCategoriesForList(null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceGetCategoriesForList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceSearchListAudit - errors', () => {
      it('should have a categorizedListsServiceSearchListAudit function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceSearchListAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceSearchListAudit(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceSearchListAudit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceUpdateItemInList - errors', () => {
      it('should have a categorizedListsServiceUpdateItemInList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceUpdateItemInList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceUpdateItemInList(null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceUpdateItemInList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceAddItemToList - errors', () => {
      it('should have a categorizedListsServiceAddItemToList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceAddItemToList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceAddItemToList(null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceAddItemToList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceAddItemsToListWithCategory - errors', () => {
      it('should have a categorizedListsServiceAddItemsToListWithCategory function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceAddItemsToListWithCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceAddItemsToListWithCategory(null, null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceAddItemsToListWithCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing category', (done) => {
        try {
          a.categorizedListsServiceAddItemsToListWithCategory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'category is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceAddItemsToListWithCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceAddItemsToListFromFile - errors', () => {
      it('should have a categorizedListsServiceAddItemsToListFromFile function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceAddItemsToListFromFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceAddItemsToListFromFile(null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceAddItemsToListFromFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#categorizedListsServiceReplaceItemsInList - errors', () => {
      it('should have a categorizedListsServiceReplaceItemsInList function', (done) => {
        try {
          assert.equal(true, typeof a.categorizedListsServiceReplaceItemsInList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categorizedListId', (done) => {
        try {
          a.categorizedListsServiceReplaceItemsInList(null, null, (data, error) => {
            try {
              const displayE = 'categorizedListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-categorizedListsServiceReplaceItemsInList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#characterSetsServiceSearchCharacterSets - errors', () => {
      it('should have a characterSetsServiceSearchCharacterSets function', (done) => {
        try {
          assert.equal(true, typeof a.characterSetsServiceSearchCharacterSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#characterSetsServiceCreate - errors', () => {
      it('should have a characterSetsServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.characterSetsServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#characterSetsServiceGet - errors', () => {
      it('should have a characterSetsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.characterSetsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.characterSetsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-characterSetsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#characterSetsServicePatch - errors', () => {
      it('should have a characterSetsServicePatch function', (done) => {
        try {
          assert.equal(true, typeof a.characterSetsServicePatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.characterSetsServicePatch(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-characterSetsServicePatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#characterSetsServiceGetSiteAudits - errors', () => {
      it('should have a characterSetsServiceGetSiteAudits function', (done) => {
        try {
          assert.equal(true, typeof a.characterSetsServiceGetSiteAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.characterSetsServiceGetSiteAudits(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-characterSetsServiceGetSiteAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetConfigurationState - errors', () => {
      it('should have a configurationServiceGetConfigurationState function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetConfigurationState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetConfigurationAudit - errors', () => {
      it('should have a configurationServiceGetConfigurationAudit function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetConfigurationAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetGeneralConfigurationV2 - errors', () => {
      it('should have a configurationServiceGetGeneralConfigurationV2 function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetGeneralConfigurationV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetLocalPasswordConfiguration - errors', () => {
      it('should have a configurationServiceGetLocalPasswordConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetLocalPasswordConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchLocalPasswordConfiguration - errors', () => {
      it('should have a configurationServicePatchLocalPasswordConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchLocalPasswordConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetSecurityConfiguration - errors', () => {
      it('should have a configurationServiceGetSecurityConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetSecurityConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchSecurityConfiguration - errors', () => {
      it('should have a configurationServicePatchSecurityConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchSecurityConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetLoginConfigurationV2 - errors', () => {
      it('should have a configurationServiceGetLoginConfigurationV2 function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetLoginConfigurationV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchLoginConfigurationV2 - errors', () => {
      it('should have a configurationServicePatchLoginConfigurationV2 function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchLoginConfigurationV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetLoginPolicyConfiguration - errors', () => {
      it('should have a configurationServiceGetLoginPolicyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetLoginPolicyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchLoginPolicyConfiguration - errors', () => {
      it('should have a configurationServicePatchLoginPolicyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchLoginPolicyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetSamlConfiguration - errors', () => {
      it('should have a configurationServiceGetSamlConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetSamlConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchSamlConfiguration - errors', () => {
      it('should have a configurationServicePatchSamlConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchSamlConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetSamlIdentityProviderConfiguration - errors', () => {
      it('should have a configurationServiceGetSamlIdentityProviderConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetSamlIdentityProviderConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.configurationServiceGetSamlIdentityProviderConfiguration(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-configurationServiceGetSamlIdentityProviderConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetRpcConfiguration - errors', () => {
      it('should have a configurationServiceGetRpcConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetRpcConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchRpcConfiguration - errors', () => {
      it('should have a configurationServicePatchRpcConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchRpcConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetSessionRecordingAdvancedConfiguration - errors', () => {
      it('should have a configurationServiceGetSessionRecordingAdvancedConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetSessionRecordingAdvancedConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchSessionRecordingAdvancedConfiguration - errors', () => {
      it('should have a configurationServicePatchSessionRecordingAdvancedConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchSessionRecordingAdvancedConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetSiteConnectors - errors', () => {
      it('should have a configurationServiceGetSiteConnectors function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetSiteConnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetInternalSiteConfiguration - errors', () => {
      it('should have a configurationServiceGetInternalSiteConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetInternalSiteConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchInternalSiteConfiguration - errors', () => {
      it('should have a configurationServicePatchInternalSiteConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchInternalSiteConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetUnlimitedAdmin - errors', () => {
      it('should have a configurationServiceGetUnlimitedAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetUnlimitedAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceUpdateUnlimitedAdmin - errors', () => {
      it('should have a configurationServiceUpdateUnlimitedAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceUpdateUnlimitedAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetSecretSearchIndexerConfiguration - errors', () => {
      it('should have a configurationServiceGetSecretSearchIndexerConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetSecretSearchIndexerConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchSecretSearchIndexerConfiguration - errors', () => {
      it('should have a configurationServicePatchSecretSearchIndexerConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchSecretSearchIndexerConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetPlatformConfiguration - errors', () => {
      it('should have a configurationServiceGetPlatformConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetPlatformConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchPlatformConfiguration - errors', () => {
      it('should have a configurationServicePatchPlatformConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchPlatformConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetPlatformConfigurationAudits - errors', () => {
      it('should have a configurationServiceGetPlatformConfigurationAudits function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetPlatformConfigurationAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetAutoExportConfiguration - errors', () => {
      it('should have a configurationServiceGetAutoExportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetAutoExportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchAutomaticExportConfiguration - errors', () => {
      it('should have a configurationServicePatchAutomaticExportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchAutomaticExportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetAutoExportConfigurationAudits - errors', () => {
      it('should have a configurationServiceGetAutoExportConfigurationAudits function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetAutoExportConfigurationAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetAutoExportLogs - errors', () => {
      it('should have a configurationServiceGetAutoExportLogs function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetAutoExportLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetAutoExportStorageItemsDefault - errors', () => {
      it('should have a configurationServiceGetAutoExportStorageItemsDefault function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetAutoExportStorageItemsDefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetAutoExportStorageItems - errors', () => {
      it('should have a configurationServiceGetAutoExportStorageItems function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetAutoExportStorageItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.configurationServiceGetAutoExportStorageItems(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-configurationServiceGetAutoExportStorageItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetAutoExportStorageItem - errors', () => {
      it('should have a configurationServiceGetAutoExportStorageItem function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetAutoExportStorageItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.configurationServiceGetAutoExportStorageItem(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-configurationServiceGetAutoExportStorageItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetDatabaseBackupConfiguration - errors', () => {
      it('should have a configurationServiceGetDatabaseBackupConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetDatabaseBackupConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchDatabaseBackupConfiguration - errors', () => {
      it('should have a configurationServicePatchDatabaseBackupConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchDatabaseBackupConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetBackupLogsV2 - errors', () => {
      it('should have a configurationServiceGetBackupLogsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetBackupLogsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetSystemLogConfiguration - errors', () => {
      it('should have a configurationServiceGetSystemLogConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetSystemLogConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchSystemLogConfiguration - errors', () => {
      it('should have a configurationServicePatchSystemLogConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchSystemLogConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceClearSystemLog - errors', () => {
      it('should have a configurationServiceClearSystemLog function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceClearSystemLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetPublicSshKeyExpiration - errors', () => {
      it('should have a configurationServiceGetPublicSshKeyExpiration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetPublicSshKeyExpiration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetTicketSystemConfiguration - errors', () => {
      it('should have a configurationServiceGetTicketSystemConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetTicketSystemConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchTicketSystemConfiguration - errors', () => {
      it('should have a configurationServicePatchTicketSystemConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchTicketSystemConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetDatabaseConfiguration - errors', () => {
      it('should have a configurationServiceGetDatabaseConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetDatabaseConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchDatabaseConfiguration - errors', () => {
      it('should have a configurationServicePatchDatabaseConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchDatabaseConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetConfigurationReadOnlyMode - errors', () => {
      it('should have a configurationServiceGetConfigurationReadOnlyMode function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetConfigurationReadOnlyMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchConfigurationReadOnlyMode - errors', () => {
      it('should have a configurationServicePatchConfigurationReadOnlyMode function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchConfigurationReadOnlyMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceGetRotateSecretKeysStatus - errors', () => {
      it('should have a configurationServiceGetRotateSecretKeysStatus function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceGetRotateSecretKeysStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchGeneralConfiguration - errors', () => {
      it('should have a configurationServicePatchGeneralConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchGeneralConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePostSamlConfiguration - errors', () => {
      it('should have a configurationServicePostSamlConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePostSamlConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchSamlIdentityProviderConfiguration - errors', () => {
      it('should have a configurationServicePatchSamlIdentityProviderConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchSamlIdentityProviderConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchSessionRecordingConfiguration - errors', () => {
      it('should have a configurationServicePatchSessionRecordingConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchSessionRecordingConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchFolderConfiguration - errors', () => {
      it('should have a configurationServicePatchFolderConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchFolderConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchApplicationSettingsConfiguration - errors', () => {
      it('should have a configurationServicePatchApplicationSettingsConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchApplicationSettingsConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchPermissionOptionsConfiguration - errors', () => {
      it('should have a configurationServicePatchPermissionOptionsConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchPermissionOptionsConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchLauncherSettingsConfiguration - errors', () => {
      it('should have a configurationServicePatchLauncherSettingsConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchLauncherSettingsConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchProtocolHandlerSettingsConfiguration - errors', () => {
      it('should have a configurationServicePatchProtocolHandlerSettingsConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchProtocolHandlerSettingsConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchUserExperienceConfiguration - errors', () => {
      it('should have a configurationServicePatchUserExperienceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchUserExperienceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchUserInterfaceConfiguration - errors', () => {
      it('should have a configurationServicePatchUserInterfaceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchUserInterfaceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServicePatchEmailConfiguration - errors', () => {
      it('should have a configurationServicePatchEmailConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServicePatchEmailConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceRunRpcNow - errors', () => {
      it('should have a configurationServiceRunRpcNow function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceRunRpcNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceRunHeartbeatNow - errors', () => {
      it('should have a configurationServiceRunHeartbeatNow function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceRunHeartbeatNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceRebuildSecretSearchIndexerConfiguration - errors', () => {
      it('should have a configurationServiceRebuildSecretSearchIndexerConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceRebuildSecretSearchIndexerConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceRunAutoExportNow - errors', () => {
      it('should have a configurationServiceRunAutoExportNow function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceRunAutoExportNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceRunBackupNow - errors', () => {
      it('should have a configurationServiceRunBackupNow function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceRunBackupNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceRotateSecretKeys - errors', () => {
      it('should have a configurationServiceRotateSecretKeys function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceRotateSecretKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceCancelRotateSecretKeys - errors', () => {
      it('should have a configurationServiceCancelRotateSecretKeys function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceCancelRotateSecretKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configurationServiceTestEmail - errors', () => {
      it('should have a configurationServiceTestEmail function', (done) => {
        try {
          assert.equal(true, typeof a.configurationServiceTestEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectionManagerSettingsServiceGet - errors', () => {
      it('should have a connectionManagerSettingsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.connectionManagerSettingsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultSyncServiceGetSyncStatuses - errors', () => {
      it('should have a devOpsSecretsVaultSyncServiceGetSyncStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultSyncServiceGetSyncStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultSyncServiceGetSyncStatus - errors', () => {
      it('should have a devOpsSecretsVaultSyncServiceGetSyncStatus function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultSyncServiceGetSyncStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing syncMapId', (done) => {
        try {
          a.devOpsSecretsVaultSyncServiceGetSyncStatus(null, (data, error) => {
            try {
              const displayE = 'syncMapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-devOpsSecretsVaultSyncServiceGetSyncStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultSyncServiceCreateSync - errors', () => {
      it('should have a devOpsSecretsVaultSyncServiceCreateSync function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultSyncServiceCreateSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultSyncServiceSyncSecret - errors', () => {
      it('should have a devOpsSecretsVaultSyncServiceSyncSecret function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultSyncServiceSyncSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultSyncServiceUpdateSync - errors', () => {
      it('should have a devOpsSecretsVaultSyncServiceUpdateSync function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultSyncServiceUpdateSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing syncSecretMapId', (done) => {
        try {
          a.devOpsSecretsVaultSyncServiceUpdateSync(null, null, (data, error) => {
            try {
              const displayE = 'syncSecretMapId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-devOpsSecretsVaultSyncServiceUpdateSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultTenantServiceGetList - errors', () => {
      it('should have a devOpsSecretsVaultTenantServiceGetList function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultTenantServiceGetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultTenantServiceCreate - errors', () => {
      it('should have a devOpsSecretsVaultTenantServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultTenantServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultTenantServiceGetTenant - errors', () => {
      it('should have a devOpsSecretsVaultTenantServiceGetTenant function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultTenantServiceGetTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.devOpsSecretsVaultTenantServiceGetTenant(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-devOpsSecretsVaultTenantServiceGetTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultTenantServiceUpdate - errors', () => {
      it('should have a devOpsSecretsVaultTenantServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultTenantServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.devOpsSecretsVaultTenantServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-devOpsSecretsVaultTenantServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultTenantServiceGetTenantStub - errors', () => {
      it('should have a devOpsSecretsVaultTenantServiceGetTenantStub function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultTenantServiceGetTenantStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#devOpsSecretsVaultTenantServiceGetTenantAudits - errors', () => {
      it('should have a devOpsSecretsVaultTenantServiceGetTenantAudits function', (done) => {
        try {
          assert.equal(true, typeof a.devOpsSecretsVaultTenantServiceGetTenantAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetDiagnosticInformation - errors', () => {
      it('should have a diagnosticsServiceGetDiagnosticInformation function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetDiagnosticInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetBackgroundProcesses - errors', () => {
      it('should have a diagnosticsServiceGetBackgroundProcesses function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetBackgroundProcesses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetConnectivityReport - errors', () => {
      it('should have a diagnosticsServiceGetConnectivityReport function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetConnectivityReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetAppSettings - errors', () => {
      it('should have a diagnosticsServiceGetAppSettings function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetAppSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetQuartzJobs - errors', () => {
      it('should have a diagnosticsServiceGetQuartzJobs function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetQuartzJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetGeneralLogs - errors', () => {
      it('should have a diagnosticsServiceGetGeneralLogs function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetGeneralLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetSystemLogs - errors', () => {
      it('should have a diagnosticsServiceGetSystemLogs function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetSystemLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetDiscoveryLogs - errors', () => {
      it('should have a diagnosticsServiceGetDiscoveryLogs function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetDiscoveryLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetComputerScanLogs - errors', () => {
      it('should have a diagnosticsServiceGetComputerScanLogs function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetComputerScanLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetRPCLogs - errors', () => {
      it('should have a diagnosticsServiceGetRPCLogs function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetRPCLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceGetHeartbeatLogs - errors', () => {
      it('should have a diagnosticsServiceGetHeartbeatLogs function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceGetHeartbeatLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceClearQuartzJobErrors - errors', () => {
      it('should have a diagnosticsServiceClearQuartzJobErrors function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceClearQuartzJobErrors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceClearUpgradeInProgress - errors', () => {
      it('should have a diagnosticsServiceClearUpgradeInProgress function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceClearUpgradeInProgress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsServiceTestEventLog - errors', () => {
      it('should have a diagnosticsServiceTestEventLog function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsServiceTestEventLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosticsV2ServiceSearchSystemLog - errors', () => {
      it('should have a diagnosticsV2ServiceSearchSystemLog function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosticsV2ServiceSearchSystemLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceUnlinkDomainGroup - errors', () => {
      it('should have a directoryServicesServiceUnlinkDomainGroup function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceUnlinkDomainGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainId', (done) => {
        try {
          a.directoryServicesServiceUnlinkDomainGroup(null, null, (data, error) => {
            try {
              const displayE = 'domainId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServiceUnlinkDomainGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.directoryServicesServiceUnlinkDomainGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServiceUnlinkDomainGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceGetSynchronizationLog - errors', () => {
      it('should have a directoryServicesServiceGetSynchronizationLog function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceGetSynchronizationLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceGetSynchronizationStatus - errors', () => {
      it('should have a directoryServicesServiceGetSynchronizationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceGetSynchronizationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceSearchDomains - errors', () => {
      it('should have a directoryServicesServiceSearchDomains function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceSearchDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceCreateDomain - errors', () => {
      it('should have a directoryServicesServiceCreateDomain function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceCreateDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceGetDomain - errors', () => {
      it('should have a directoryServicesServiceGetDomain function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceGetDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.directoryServicesServiceGetDomain(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServiceGetDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceGetLdapSyncSettings - errors', () => {
      it('should have a directoryServicesServiceGetLdapSyncSettings function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceGetLdapSyncSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.directoryServicesServiceGetLdapSyncSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServiceGetLdapSyncSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceGetDomainStub - errors', () => {
      it('should have a directoryServicesServiceGetDomainStub function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceGetDomainStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceSearchDirectoryForGroups - errors', () => {
      it('should have a directoryServicesServiceSearchDirectoryForGroups function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceSearchDirectoryForGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainId', (done) => {
        try {
          a.directoryServicesServiceSearchDirectoryForGroups(null, null, (data, error) => {
            try {
              const displayE = 'domainId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServiceSearchDirectoryForGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceGetDirectoryGroupMembers - errors', () => {
      it('should have a directoryServicesServiceGetDirectoryGroupMembers function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceGetDirectoryGroupMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainId', (done) => {
        try {
          a.directoryServicesServiceGetDirectoryGroupMembers(null, null, null, (data, error) => {
            try {
              const displayE = 'domainId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServiceGetDirectoryGroupMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceGetDirectoryServicesConfiguration - errors', () => {
      it('should have a directoryServicesServiceGetDirectoryServicesConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceGetDirectoryServicesConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServicePatchDirectoryServicesConfiguration - errors', () => {
      it('should have a directoryServicesServicePatchDirectoryServicesConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServicePatchDirectoryServicesConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServicePatchLdapSyncSettings - errors', () => {
      it('should have a directoryServicesServicePatchLdapSyncSettings function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServicePatchLdapSyncSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainId', (done) => {
        try {
          a.directoryServicesServicePatchLdapSyncSettings(null, null, (data, error) => {
            try {
              const displayE = 'domainId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServicePatchLdapSyncSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServicePatchDomain - errors', () => {
      it('should have a directoryServicesServicePatchDomain function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServicePatchDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainId', (done) => {
        try {
          a.directoryServicesServicePatchDomain(null, null, (data, error) => {
            try {
              const displayE = 'domainId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServicePatchDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceSynchronizeNow - errors', () => {
      it('should have a directoryServicesServiceSynchronizeNow function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceSynchronizeNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#directoryServicesServiceLinkDomainGroup - errors', () => {
      it('should have a directoryServicesServiceLinkDomainGroup function', (done) => {
        try {
          assert.equal(true, typeof a.directoryServicesServiceLinkDomainGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainId', (done) => {
        try {
          a.directoryServicesServiceLinkDomainGroup(null, null, (data, error) => {
            try {
              const displayE = 'domainId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-directoryServicesServiceLinkDomainGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceGetDisasterRecoveryDataReplica - errors', () => {
      it('should have a disasterRecoveryServiceGetDisasterRecoveryDataReplica function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceGetDisasterRecoveryDataReplica === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing replicaId', (done) => {
        try {
          a.disasterRecoveryServiceGetDisasterRecoveryDataReplica(null, (data, error) => {
            try {
              const displayE = 'replicaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-disasterRecoveryServiceGetDisasterRecoveryDataReplica', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceDeleteDisasterRecoveryDataReplica - errors', () => {
      it('should have a disasterRecoveryServiceDeleteDisasterRecoveryDataReplica function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceDeleteDisasterRecoveryDataReplica === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing replicaId', (done) => {
        try {
          a.disasterRecoveryServiceDeleteDisasterRecoveryDataReplica(null, (data, error) => {
            try {
              const displayE = 'replicaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-disasterRecoveryServiceDeleteDisasterRecoveryDataReplica', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServicePatchDisasterRecoveryDataReplica - errors', () => {
      it('should have a disasterRecoveryServicePatchDisasterRecoveryDataReplica function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServicePatchDisasterRecoveryDataReplica === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing replicaId', (done) => {
        try {
          a.disasterRecoveryServicePatchDisasterRecoveryDataReplica(null, null, (data, error) => {
            try {
              const displayE = 'replicaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-disasterRecoveryServicePatchDisasterRecoveryDataReplica', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceGetDisasterRecoveryIncomingConfiguration - errors', () => {
      it('should have a disasterRecoveryServiceGetDisasterRecoveryIncomingConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceGetDisasterRecoveryIncomingConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServicePatchDisasterRecoveryIncomingConfiguration - errors', () => {
      it('should have a disasterRecoveryServicePatchDisasterRecoveryIncomingConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServicePatchDisasterRecoveryIncomingConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceGetDisasterRecoveryConfigurationAudits - errors', () => {
      it('should have a disasterRecoveryServiceGetDisasterRecoveryConfigurationAudits function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceGetDisasterRecoveryConfigurationAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceGetDisasterRecoveryOutgoingConfiguration - errors', () => {
      it('should have a disasterRecoveryServiceGetDisasterRecoveryOutgoingConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceGetDisasterRecoveryOutgoingConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceGetDisasterRecoveryDataReplicationLogs - errors', () => {
      it('should have a disasterRecoveryServiceGetDisasterRecoveryDataReplicationLogs function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceGetDisasterRecoveryDataReplicationLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServicePostDisasterRecoveryHandshake - errors', () => {
      it('should have a disasterRecoveryServicePostDisasterRecoveryHandshake function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServicePostDisasterRecoveryHandshake === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceRunDisasterRecoveryDataReplicationNow - errors', () => {
      it('should have a disasterRecoveryServiceRunDisasterRecoveryDataReplicationNow function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceRunDisasterRecoveryDataReplicationNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceRunDisasterRecoveryDataReplicaTestNow - errors', () => {
      it('should have a disasterRecoveryServiceRunDisasterRecoveryDataReplicaTestNow function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceRunDisasterRecoveryDataReplicaTestNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceRequestDataReplicaReplicationStart - errors', () => {
      it('should have a disasterRecoveryServiceRequestDataReplicaReplicationStart function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceRequestDataReplicaReplicationStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceRequestDataReplicaReplicationStatus - errors', () => {
      it('should have a disasterRecoveryServiceRequestDataReplicaReplicationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceRequestDataReplicaReplicationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disasterRecoveryServiceRequestDataReplicaReplicationPackage - errors', () => {
      it('should have a disasterRecoveryServiceRequestDataReplicaReplicationPackage function', (done) => {
        try {
          assert.equal(true, typeof a.disasterRecoveryServiceRequestDataReplicaReplicationPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceSearchDiscoverySources - errors', () => {
      it('should have a discoveryServiceSearchDiscoverySources function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceSearchDiscoverySources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetDiscoverySource - errors', () => {
      it('should have a discoveryServiceGetDiscoverySource function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetDiscoverySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.discoveryServiceGetDiscoverySource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceGetDiscoverySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceUpdateDiscoverySource - errors', () => {
      it('should have a discoveryServiceUpdateDiscoverySource function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceUpdateDiscoverySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.discoveryServiceUpdateDiscoverySource(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceUpdateDiscoverySource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetDiscoverySourceStub - errors', () => {
      it('should have a discoveryServiceGetDiscoverySourceStub function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetDiscoverySourceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.discoveryServiceGetDiscoverySourceStub(null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceGetDiscoverySourceStub', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetDiscoveryConfiguration - errors', () => {
      it('should have a discoveryServiceGetDiscoveryConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetDiscoveryConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceUpdateDiscoveryConfiguration - errors', () => {
      it('should have a discoveryServiceUpdateDiscoveryConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceUpdateDiscoveryConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetDiscoveryStatus - errors', () => {
      it('should have a discoveryServiceGetDiscoveryStatus function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetDiscoveryStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetAvailableDiscoverySourceScanners - errors', () => {
      it('should have a discoveryServiceGetAvailableDiscoverySourceScanners function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetAvailableDiscoverySourceScanners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoverySourceId', (done) => {
        try {
          a.discoveryServiceGetAvailableDiscoverySourceScanners(null, (data, error) => {
            try {
              const displayE = 'discoverySourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceGetAvailableDiscoverySourceScanners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceSearchDiscoverySourceScannerSettings - errors', () => {
      it('should have a discoveryServiceSearchDiscoverySourceScannerSettings function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceSearchDiscoverySourceScannerSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoverySourceId', (done) => {
        try {
          a.discoveryServiceSearchDiscoverySourceScannerSettings(null, (data, error) => {
            try {
              const displayE = 'discoverySourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceSearchDiscoverySourceScannerSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetScanTypes - errors', () => {
      it('should have a discoveryServiceGetScanTypes function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetScanTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetDiscoverySourceAudits - errors', () => {
      it('should have a discoveryServiceGetDiscoverySourceAudits function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetDiscoverySourceAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceSearchForDomainOu - errors', () => {
      it('should have a discoveryServiceSearchForDomainOu function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceSearchForDomainOu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoverySourceId', (done) => {
        try {
          a.discoveryServiceSearchForDomainOu(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'discoverySourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceSearchForDomainOu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceGetDiscoverySourceFilter - errors', () => {
      it('should have a discoveryServiceGetDiscoverySourceFilter function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceGetDiscoverySourceFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.discoveryServiceGetDiscoverySourceFilter(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceGetDiscoverySourceFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServicePatchDiscoverySourceFilter - errors', () => {
      it('should have a discoveryServicePatchDiscoverySourceFilter function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServicePatchDiscoverySourceFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memberId', (done) => {
        try {
          a.discoveryServicePatchDiscoverySourceFilter(null, null, null, (data, error) => {
            try {
              const displayE = 'memberId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServicePatchDiscoverySourceFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoverySourceId', (done) => {
        try {
          a.discoveryServicePatchDiscoverySourceFilter('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discoverySourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServicePatchDiscoverySourceFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceCreateDiscoverySource - errors', () => {
      it('should have a discoveryServiceCreateDiscoverySource function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceCreateDiscoverySource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceRunDiscoveryNow - errors', () => {
      it('should have a discoveryServiceRunDiscoveryNow function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceRunDiscoveryNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceCreateDiscoverySourceScannerSettings - errors', () => {
      it('should have a discoveryServiceCreateDiscoverySourceScannerSettings function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceCreateDiscoverySourceScannerSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoverySourceId', (done) => {
        try {
          a.discoveryServiceCreateDiscoverySourceScannerSettings(null, null, (data, error) => {
            try {
              const displayE = 'discoverySourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceCreateDiscoverySourceScannerSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryServiceUpdateDiscoverySourceFilters - errors', () => {
      it('should have a discoveryServiceUpdateDiscoverySourceFilters function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryServiceUpdateDiscoverySourceFilters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoverySourceId', (done) => {
        try {
          a.discoveryServiceUpdateDiscoverySourceFilters(null, null, (data, error) => {
            try {
              const displayE = 'discoverySourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-discoveryServiceUpdateDiscoverySourceFilters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetDistributedEngineConfiguration - errors', () => {
      it('should have a distributedEngineServiceGetDistributedEngineConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetDistributedEngineConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServicePatchDistributedEngineConfiguration - errors', () => {
      it('should have a distributedEngineServicePatchDistributedEngineConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServicePatchDistributedEngineConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceSearchSiteConnectors - errors', () => {
      it('should have a distributedEngineServiceSearchSiteConnectors function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceSearchSiteConnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetSiteConnector - errors', () => {
      it('should have a distributedEngineServiceGetSiteConnector function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetSiteConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceGetSiteConnector(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetSiteConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceUpdateSiteConnector - errors', () => {
      it('should have a distributedEngineServiceUpdateSiteConnector function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceUpdateSiteConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceUpdateSiteConnector(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceUpdateSiteConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetSiteConnectorStub - errors', () => {
      it('should have a distributedEngineServiceGetSiteConnectorStub function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetSiteConnectorStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetSiteConnectorCredentials - errors', () => {
      it('should have a distributedEngineServiceGetSiteConnectorCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetSiteConnectorCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteConnectorId', (done) => {
        try {
          a.distributedEngineServiceGetSiteConnectorCredentials(null, (data, error) => {
            try {
              const displayE = 'siteConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetSiteConnectorCredentials', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceSearchEngines - errors', () => {
      it('should have a distributedEngineServiceSearchEngines function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceSearchEngines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetEngineSettingsForSite - errors', () => {
      it('should have a distributedEngineServiceGetEngineSettingsForSite function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetEngineSettingsForSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.distributedEngineServiceGetEngineSettingsForSite(null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetEngineSettingsForSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetEngineAuditsForSite - errors', () => {
      it('should have a distributedEngineServiceGetEngineAuditsForSite function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetEngineAuditsForSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.distributedEngineServiceGetEngineAuditsForSite('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetEngineAuditsForSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetEngineSettings - errors', () => {
      it('should have a distributedEngineServiceGetEngineSettings function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetEngineSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing engineId', (done) => {
        try {
          a.distributedEngineServiceGetEngineSettings(null, (data, error) => {
            try {
              const displayE = 'engineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetEngineSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceSearchSites - errors', () => {
      it('should have a distributedEngineServiceSearchSites function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceSearchSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetSite - errors', () => {
      it('should have a distributedEngineServiceGetSite function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceGetSite(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServicePatchSite - errors', () => {
      it('should have a distributedEngineServicePatchSite function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServicePatchSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServicePatchSite(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServicePatchSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetSiteStub - errors', () => {
      it('should have a distributedEngineServiceGetSiteStub function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetSiteStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetSiteAudits - errors', () => {
      it('should have a distributedEngineServiceGetSiteAudits function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetSiteAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceGetSiteAudits(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetSiteAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceSearchSiteLogs - errors', () => {
      it('should have a distributedEngineServiceSearchSiteLogs function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceSearchSiteLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceSearchSiteLogs(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceSearchSiteLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceDownloadMemoryMq - errors', () => {
      it('should have a distributedEngineServiceDownloadMemoryMq function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceDownloadMemoryMq === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceDownloadMemoryMq(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceDownloadMemoryMq', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceGetServerCapabilities - errors', () => {
      it('should have a distributedEngineServiceGetServerCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceGetServerCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceGetServerCapabilities(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceGetServerCapabilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceDownloadDistributedEngine - errors', () => {
      it('should have a distributedEngineServiceDownloadDistributedEngine function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceDownloadDistributedEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServicePatchEngineSettings - errors', () => {
      it('should have a distributedEngineServicePatchEngineSettings function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServicePatchEngineSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing engineSettingsId', (done) => {
        try {
          a.distributedEngineServicePatchEngineSettings(null, null, (data, error) => {
            try {
              const displayE = 'engineSettingsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServicePatchEngineSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServicePatchEngineSettingsForEngine - errors', () => {
      it('should have a distributedEngineServicePatchEngineSettingsForEngine function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServicePatchEngineSettingsForEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing engineId', (done) => {
        try {
          a.distributedEngineServicePatchEngineSettingsForEngine(null, null, (data, error) => {
            try {
              const displayE = 'engineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServicePatchEngineSettingsForEngine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceCreateSiteConnector - errors', () => {
      it('should have a distributedEngineServiceCreateSiteConnector function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceCreateSiteConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceValidateSiteConnector - errors', () => {
      it('should have a distributedEngineServiceValidateSiteConnector function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceValidateSiteConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteConnectorId', (done) => {
        try {
          a.distributedEngineServiceValidateSiteConnector(null, (data, error) => {
            try {
              const displayE = 'siteConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceValidateSiteConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceUpdateEngineStatus - errors', () => {
      it('should have a distributedEngineServiceUpdateEngineStatus function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceUpdateEngineStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceCreateSite - errors', () => {
      it('should have a distributedEngineServiceCreateSite function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceCreateSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#distributedEngineServiceReassignSecrets - errors', () => {
      it('should have a distributedEngineServiceReassignSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.distributedEngineServiceReassignSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.distributedEngineServiceReassignSecrets(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-distributedEngineServiceReassignSecrets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainNameIndexServiceTruncateDomainNameIndex - errors', () => {
      it('should have a domainNameIndexServiceTruncateDomainNameIndex function', (done) => {
        try {
          assert.equal(true, typeof a.domainNameIndexServiceTruncateDomainNameIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainNameIndexServiceGetDomainNameIndex - errors', () => {
      it('should have a domainNameIndexServiceGetDomainNameIndex function', (done) => {
        try {
          assert.equal(true, typeof a.domainNameIndexServiceGetDomainNameIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainNameIndexServiceUpdateDomainNameIndex - errors', () => {
      it('should have a domainNameIndexServiceUpdateDomainNameIndex function', (done) => {
        try {
          assert.equal(true, typeof a.domainNameIndexServiceUpdateDomainNameIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.domainNameIndexServiceUpdateDomainNameIndex(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-domainNameIndexServiceUpdateDomainNameIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainNameIndexServiceCreateDomainNameIndex - errors', () => {
      it('should have a domainNameIndexServiceCreateDomainNameIndex function', (done) => {
        try {
          assert.equal(true, typeof a.domainNameIndexServiceCreateDomainNameIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainNameIndexServiceExpireSingleDomainNameIndex - errors', () => {
      it('should have a domainNameIndexServiceExpireSingleDomainNameIndex function', (done) => {
        try {
          assert.equal(true, typeof a.domainNameIndexServiceExpireSingleDomainNameIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.domainNameIndexServiceExpireSingleDomainNameIndex(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-domainNameIndexServiceExpireSingleDomainNameIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainNameIndexServiceExpireAllDomainNameIndex - errors', () => {
      it('should have a domainNameIndexServiceExpireAllDomainNameIndex function', (done) => {
        try {
          assert.equal(true, typeof a.domainNameIndexServiceExpireAllDomainNameIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainNameIndexServiceDeleteSingleDomainNameIndex - errors', () => {
      it('should have a domainNameIndexServiceDeleteSingleDomainNameIndex function', (done) => {
        try {
          assert.equal(true, typeof a.domainNameIndexServiceDeleteSingleDomainNameIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.domainNameIndexServiceDeleteSingleDomainNameIndex(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-domainNameIndexServiceDeleteSingleDomainNameIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceGet - errors', () => {
      it('should have a dualControlsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.dualControlsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-dualControlsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceUpdate - errors', () => {
      it('should have a dualControlsServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.dualControlsServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-dualControlsServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceDelete - errors', () => {
      it('should have a dualControlsServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.dualControlsServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-dualControlsServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceGetAllReports - errors', () => {
      it('should have a dualControlsServiceGetAllReports function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceGetAllReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dualControlType', (done) => {
        try {
          a.dualControlsServiceGetAllReports(null, null, (data, error) => {
            try {
              const displayE = 'dualControlType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-dualControlsServiceGetAllReports', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.dualControlsServiceGetAllReports('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-dualControlsServiceGetAllReports', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceSearchDualControls - errors', () => {
      it('should have a dualControlsServiceSearchDualControls function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceSearchDualControls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceCreate - errors', () => {
      it('should have a dualControlsServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceGetTypes - errors', () => {
      it('should have a dualControlsServiceGetTypes function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceGetTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceStub - errors', () => {
      it('should have a dualControlsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dualControlsServiceAuthorizeDualControl - errors', () => {
      it('should have a dualControlsServiceAuthorizeDualControl function', (done) => {
        try {
          assert.equal(true, typeof a.dualControlsServiceAuthorizeDualControl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dualControlType', (done) => {
        try {
          a.dualControlsServiceAuthorizeDualControl(null, null, null, (data, error) => {
            try {
              const displayE = 'dualControlType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-dualControlsServiceAuthorizeDualControl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.dualControlsServiceAuthorizeDualControl('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-dualControlsServiceAuthorizeDualControl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enterpriseServiceGetRequestData - errors', () => {
      it('should have a enterpriseServiceGetRequestData function', (done) => {
        try {
          assert.equal(true, typeof a.enterpriseServiceGetRequestData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestId', (done) => {
        try {
          a.enterpriseServiceGetRequestData(null, (data, error) => {
            try {
              const displayE = 'requestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-enterpriseServiceGetRequestData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enterpriseServiceSearchRequestData - errors', () => {
      it('should have a enterpriseServiceSearchRequestData function', (done) => {
        try {
          assert.equal(true, typeof a.enterpriseServiceSearchRequestData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceGetEventPipeline - errors', () => {
      it('should have a eventPipelineServiceGetEventPipeline function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceGetEventPipeline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelineServiceGetEventPipeline(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelineServiceGetEventPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceUpdateEventPipelines - errors', () => {
      it('should have a eventPipelineServiceUpdateEventPipelines function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceUpdateEventPipelines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelineServiceUpdateEventPipelines(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelineServiceUpdateEventPipelines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceGetEventPipelines - errors', () => {
      it('should have a eventPipelineServiceGetEventPipelines function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceGetEventPipelines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceGetEventPipelineSummaries - errors', () => {
      it('should have a eventPipelineServiceGetEventPipelineSummaries function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceGetEventPipelineSummaries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceGetEventPipelineStub - errors', () => {
      it('should have a eventPipelineServiceGetEventPipelineStub function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceGetEventPipelineStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceGetEventPipelineRuns - errors', () => {
      it('should have a eventPipelineServiceGetEventPipelineRuns function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceGetEventPipelineRuns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceCreateEventPipelines - errors', () => {
      it('should have a eventPipelineServiceCreateEventPipelines function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceCreateEventPipelines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceReorderPipeline - errors', () => {
      it('should have a eventPipelineServiceReorderPipeline function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceReorderPipeline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelineServiceReorderPipeline(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelineServiceReorderPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineServiceTogglePipelineActive - errors', () => {
      it('should have a eventPipelineServiceTogglePipelineActive function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineServiceTogglePipelineActive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelineServiceTogglePipelineActive(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelineServiceTogglePipelineActive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineAuditServiceGetAllPipelineAndPolicyAudits - errors', () => {
      it('should have a eventPipelineAuditServiceGetAllPipelineAndPolicyAudits function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineAuditServiceGetAllPipelineAndPolicyAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceRemoveEventPipelineFromPolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceRemoveEventPipelineFromPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceRemoveEventPipelineFromPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.eventPipelinePolicyServiceRemoveEventPipelineFromPolicy(null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceRemoveEventPipelineFromPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.eventPipelinePolicyServiceRemoveEventPipelineFromPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceRemoveEventPipelineFromPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceGetEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceGetEventPipelinePolicy(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceGetEventPipelinePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceUpdateEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceUpdateEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceUpdateEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceUpdateEventPipelinePolicy(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceUpdateEventPipelinePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceAddPipelineToEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceAddPipelineToEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceAddPipelineToEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceAddPipelineToEventPipelinePolicy(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceAddPipelineToEventPipelinePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetEventPipelinePolicies - errors', () => {
      it('should have a eventPipelinePolicyServiceGetEventPipelinePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetEventPipelinePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetEventPipelinePolicyRuns - errors', () => {
      it('should have a eventPipelinePolicyServiceGetEventPipelinePolicyRuns function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetEventPipelinePolicyRuns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetEventPipelinePolicyRunActivity - errors', () => {
      it('should have a eventPipelinePolicyServiceGetEventPipelinePolicyRunActivity function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetEventPipelinePolicyRunActivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetSecretPoliciesForPipelinePolicies - errors', () => {
      it('should have a eventPipelinePolicyServiceGetSecretPoliciesForPipelinePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetSecretPoliciesForPipelinePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceGetSecretPoliciesForPipelinePolicies(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceGetSecretPoliciesForPipelinePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetFoldersForPipelinePolicies - errors', () => {
      it('should have a eventPipelinePolicyServiceGetFoldersForPipelinePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetFoldersForPipelinePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceGetFoldersForPipelinePolicies(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceGetFoldersForPipelinePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceUpdateEventPipelinePolicyFolderMaps - errors', () => {
      it('should have a eventPipelinePolicyServiceUpdateEventPipelinePolicyFolderMaps function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceUpdateEventPipelinePolicyFolderMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceUpdateEventPipelinePolicyFolderMaps(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceUpdateEventPipelinePolicyFolderMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetGroupsForPipelinePolicies - errors', () => {
      it('should have a eventPipelinePolicyServiceGetGroupsForPipelinePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetGroupsForPipelinePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceGetGroupsForPipelinePolicies(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceGetGroupsForPipelinePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceUpdateEventPipelinePolicyGroupMaps - errors', () => {
      it('should have a eventPipelinePolicyServiceUpdateEventPipelinePolicyGroupMaps function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceUpdateEventPipelinePolicyGroupMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceUpdateEventPipelinePolicyGroupMaps(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceUpdateEventPipelinePolicyGroupMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetGroupCountForPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceGetGroupCountForPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetGroupCountForPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceGetGroupCountForPipelinePolicy(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceGetGroupCountForPipelinePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder - errors', () => {
      it('should have a eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceExportEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceExportEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceExportEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceExportEventPipelinePolicy(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceExportEventPipelinePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceCreateEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceCreateEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceCreateEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceDuplicateEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceDuplicateEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceDuplicateEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceImportEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceImportEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceImportEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceUpdateEventPipelinePolicySortOrder - errors', () => {
      it('should have a eventPipelinePolicyServiceUpdateEventPipelinePolicySortOrder function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceUpdateEventPipelinePolicySortOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceUpdateEventPipelinePolicySortOrder(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceUpdateEventPipelinePolicySortOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelinePolicyServiceActivateEventPipelinePolicy - errors', () => {
      it('should have a eventPipelinePolicyServiceActivateEventPipelinePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelinePolicyServiceActivateEventPipelinePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelinePolicyServiceActivateEventPipelinePolicy(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelinePolicyServiceActivateEventPipelinePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineSettingsServiceGetEventPipelineTasks - errors', () => {
      it('should have a eventPipelineSettingsServiceGetEventPipelineTasks function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineSettingsServiceGetEventPipelineTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineSettingsServiceGetEventPipelineTaskSettings - errors', () => {
      it('should have a eventPipelineSettingsServiceGetEventPipelineTaskSettings function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineSettingsServiceGetEventPipelineTaskSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelineSettingsServiceGetEventPipelineTaskSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelineSettingsServiceGetEventPipelineTaskSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineSettingsServiceGetEventPipelineFilterSettings - errors', () => {
      it('should have a eventPipelineSettingsServiceGetEventPipelineFilterSettings function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineSettingsServiceGetEventPipelineFilterSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelineSettingsServiceGetEventPipelineFilterSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelineSettingsServiceGetEventPipelineFilterSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineSettingsServiceGetPipelineTaskOptions - errors', () => {
      it('should have a eventPipelineSettingsServiceGetPipelineTaskOptions function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineSettingsServiceGetPipelineTaskOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineSettingsServiceGetPipelineFilterOptions - errors', () => {
      it('should have a eventPipelineSettingsServiceGetPipelineFilterOptions function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineSettingsServiceGetPipelineFilterOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineTriggerServiceGetEventPipelineTriggers - errors', () => {
      it('should have a eventPipelineTriggerServiceGetEventPipelineTriggers function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineTriggerServiceGetEventPipelineTriggers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.eventPipelineTriggerServiceGetEventPipelineTriggers(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventPipelineTriggerServiceGetEventPipelineTriggers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineTriggerServiceGetEventPipelineStub - errors', () => {
      it('should have a eventPipelineTriggerServiceGetEventPipelineStub function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineTriggerServiceGetEventPipelineStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventPipelineTriggerServiceGetEventPipelineTriggerOptions - errors', () => {
      it('should have a eventPipelineTriggerServiceGetEventPipelineTriggerOptions function', (done) => {
        try {
          assert.equal(true, typeof a.eventPipelineTriggerServiceGetEventPipelineTriggerOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventSubscriptionsServiceGetSubscriptionStub - errors', () => {
      it('should have a eventSubscriptionsServiceGetSubscriptionStub function', (done) => {
        try {
          assert.equal(true, typeof a.eventSubscriptionsServiceGetSubscriptionStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventSubscriptionsServiceGetSubscriptionEntityTypes - errors', () => {
      it('should have a eventSubscriptionsServiceGetSubscriptionEntityTypes function', (done) => {
        try {
          assert.equal(true, typeof a.eventSubscriptionsServiceGetSubscriptionEntityTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventSubscriptionsServiceSearchEventSubscriptions - errors', () => {
      it('should have a eventSubscriptionsServiceSearchEventSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.eventSubscriptionsServiceSearchEventSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventSubscriptionsServiceCreateEventSubscription - errors', () => {
      it('should have a eventSubscriptionsServiceCreateEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.eventSubscriptionsServiceCreateEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventSubscriptionsServiceGetEventSubscription - errors', () => {
      it('should have a eventSubscriptionsServiceGetEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.eventSubscriptionsServiceGetEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventSubscriptionId', (done) => {
        try {
          a.eventSubscriptionsServiceGetEventSubscription(null, (data, error) => {
            try {
              const displayE = 'eventSubscriptionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventSubscriptionsServiceGetEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eventSubscriptionsServiceUpdateEventSubscription - errors', () => {
      it('should have a eventSubscriptionsServiceUpdateEventSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.eventSubscriptionsServiceUpdateEventSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventSubscriptionId', (done) => {
        try {
          a.eventSubscriptionsServiceUpdateEventSubscription(null, null, (data, error) => {
            try {
              const displayE = 'eventSubscriptionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-eventSubscriptionsServiceUpdateEventSubscription', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#extendedFieldsServiceGetRegexBySecret - errors', () => {
      it('should have a extendedFieldsServiceGetRegexBySecret function', (done) => {
        try {
          assert.equal(true, typeof a.extendedFieldsServiceGetRegexBySecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.extendedFieldsServiceGetRegexBySecret(null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-extendedFieldsServiceGetRegexBySecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#folderPermissionsServiceGet - errors', () => {
      it('should have a folderPermissionsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.folderPermissionsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.folderPermissionsServiceGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-folderPermissionsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#folderPermissionsServiceUpdate - errors', () => {
      it('should have a folderPermissionsServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.folderPermissionsServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.folderPermissionsServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-folderPermissionsServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#folderPermissionsServiceDelete - errors', () => {
      it('should have a folderPermissionsServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.folderPermissionsServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.folderPermissionsServiceDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-folderPermissionsServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#folderPermissionsServiceSearch - errors', () => {
      it('should have a folderPermissionsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.folderPermissionsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#folderPermissionsServiceCreate - errors', () => {
      it('should have a folderPermissionsServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.folderPermissionsServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#folderPermissionsServiceStub - errors', () => {
      it('should have a folderPermissionsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.folderPermissionsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.folderPermissionsServiceStub(null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-folderPermissionsServiceStub', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceGet - errors', () => {
      it('should have a foldersServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceGet('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceUpdate - errors', () => {
      it('should have a foldersServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceDelete - errors', () => {
      it('should have a foldersServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceDissassociateTemplateToFolder - errors', () => {
      it('should have a foldersServiceDissassociateTemplateToFolder function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceDissassociateTemplateToFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceDissassociateTemplateToFolder(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceDissassociateTemplateToFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.foldersServiceDissassociateTemplateToFolder('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceDissassociateTemplateToFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceUnpinFolder - errors', () => {
      it('should have a foldersServiceUnpinFolder function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceUnpinFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceUnpinFolder(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceUnpinFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServicePinFolder - errors', () => {
      it('should have a foldersServicePinFolder function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServicePinFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServicePinFolder(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServicePinFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceSearch - errors', () => {
      it('should have a foldersServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceCreate - errors', () => {
      it('should have a foldersServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceLookup - errors', () => {
      it('should have a foldersServiceLookup function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceStub - errors', () => {
      it('should have a foldersServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceGetAudits - errors', () => {
      it('should have a foldersServiceGetAudits function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceGetAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceGetAudits(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceGetAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceGetFolderDetail - errors', () => {
      it('should have a foldersServiceGetFolderDetail function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceGetFolderDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceGetFolderDetail(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceGetFolderDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceGetPinnedFolders - errors', () => {
      it('should have a foldersServiceGetPinnedFolders function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceGetPinnedFolders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServicePatchFolder - errors', () => {
      it('should have a foldersServicePatchFolder function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServicePatchFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServicePatchFolder(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServicePatchFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServicePatchFolderPermissions - errors', () => {
      it('should have a foldersServicePatchFolderPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServicePatchFolderPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.foldersServicePatchFolderPermissions(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServicePatchFolderPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#foldersServiceAssociateTemplateToFolder - errors', () => {
      it('should have a foldersServiceAssociateTemplateToFolder function', (done) => {
        try {
          assert.equal(true, typeof a.foldersServiceAssociateTemplateToFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.foldersServiceAssociateTemplateToFolder(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-foldersServiceAssociateTemplateToFolder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceGet - errors', () => {
      it('should have a groupsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceUpdate - errors', () => {
      it('should have a groupsServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceDelete - errors', () => {
      it('should have a groupsServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceGetGroupUser - errors', () => {
      it('should have a groupsServiceGetGroupUser function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceGetGroupUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceGetGroupUser(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceGetGroupUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.groupsServiceGetGroupUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceGetGroupUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceRemoveUserFromGroup - errors', () => {
      it('should have a groupsServiceRemoveUserFromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceRemoveUserFromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceRemoveUserFromGroup(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceRemoveUserFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.groupsServiceRemoveUserFromGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceRemoveUserFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceGetRoles - errors', () => {
      it('should have a groupsServiceGetRoles function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceGetRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceGetRoles('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceGetRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceUpdateUserRoles - errors', () => {
      it('should have a groupsServiceUpdateUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceUpdateUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceUpdateUserRoles(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceUpdateUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceCreateUserRoles - errors', () => {
      it('should have a groupsServiceCreateUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceCreateUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceCreateUserRoles(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceCreateUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceDeleteUserRoles - errors', () => {
      it('should have a groupsServiceDeleteUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceDeleteUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceDeleteUserRoles(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceDeleteUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceSearch - errors', () => {
      it('should have a groupsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceCreate - errors', () => {
      it('should have a groupsServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceLookup - errors', () => {
      it('should have a groupsServiceLookup function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceStub - errors', () => {
      it('should have a groupsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceGetGroupMembership - errors', () => {
      it('should have a groupsServiceGetGroupMembership function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceGetGroupMembership === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceGetGroupMembership(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceGetGroupMembership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceAddUserToGroup - errors', () => {
      it('should have a groupsServiceAddUserToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceAddUserToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServiceAddUserToGroup(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceAddUserToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServicePatchGroupMembership - errors', () => {
      it('should have a groupsServicePatchGroupMembership function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServicePatchGroupMembership === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupsServicePatchGroupMembership(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServicePatchGroupMembership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceGetAuditGroupAssignments - errors', () => {
      it('should have a groupsServiceGetAuditGroupAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceGetAuditGroupAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServicePatchGroup - errors', () => {
      it('should have a groupsServicePatchGroup function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServicePatchGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.groupsServicePatchGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServicePatchGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsServiceUpdateGroupMembers - errors', () => {
      it('should have a groupsServiceUpdateGroupMembers function', (done) => {
        try {
          assert.equal(true, typeof a.groupsServiceUpdateGroupMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.groupsServiceUpdateGroupMembers(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-groupsServiceUpdateGroupMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthCheckServiceGet - errors', () => {
      it('should have a healthCheckServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheckServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hsmConfigurationServiceGetHsmConfigurationAudit - errors', () => {
      it('should have a hsmConfigurationServiceGetHsmConfigurationAudit function', (done) => {
        try {
          assert.equal(true, typeof a.hsmConfigurationServiceGetHsmConfigurationAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetResource - errors', () => {
      it('should have a inboxServiceGetResource function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inboxServiceGetResource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServiceGetResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceDeleteResource - errors', () => {
      it('should have a inboxServiceDeleteResource function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceDeleteResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inboxServiceDeleteResource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServiceDeleteResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceSearchInboxMessages - errors', () => {
      it('should have a inboxServiceSearchInboxMessages function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceSearchInboxMessages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetInboxMessage - errors', () => {
      it('should have a inboxServiceGetInboxMessage function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetInboxMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing messageId', (done) => {
        try {
          a.inboxServiceGetInboxMessage(null, (data, error) => {
            try {
              const displayE = 'messageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServiceGetInboxMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetInboxMessageDataNames - errors', () => {
      it('should have a inboxServiceGetInboxMessageDataNames function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetInboxMessageDataNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetInboxTemplates - errors', () => {
      it('should have a inboxServiceGetInboxTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetInboxTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceCreateInboxTemplate - errors', () => {
      it('should have a inboxServiceCreateInboxTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceCreateInboxTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetInboxTemplate - errors', () => {
      it('should have a inboxServiceGetInboxTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetInboxTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.inboxServiceGetInboxTemplate(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServiceGetInboxTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServicePatchInboxTemplate - errors', () => {
      it('should have a inboxServicePatchInboxTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServicePatchInboxTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.inboxServicePatchInboxTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServicePatchInboxTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetInboxTemplateLocale - errors', () => {
      it('should have a inboxServiceGetInboxTemplateLocale function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetInboxTemplateLocale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localeId', (done) => {
        try {
          a.inboxServiceGetInboxTemplateLocale(null, null, (data, error) => {
            try {
              const displayE = 'localeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServiceGetInboxTemplateLocale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.inboxServiceGetInboxTemplateLocale('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServiceGetInboxTemplateLocale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServicePatchInboxTemplateLocale - errors', () => {
      it('should have a inboxServicePatchInboxTemplateLocale function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServicePatchInboxTemplateLocale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localeId', (done) => {
        try {
          a.inboxServicePatchInboxTemplateLocale(null, null, null, (data, error) => {
            try {
              const displayE = 'localeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServicePatchInboxTemplateLocale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.inboxServicePatchInboxTemplateLocale('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxServicePatchInboxTemplateLocale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetInboxMessageTypes - errors', () => {
      it('should have a inboxServiceGetInboxMessageTypes function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetInboxMessageTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceSearchResources - errors', () => {
      it('should have a inboxServiceSearchResources function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceSearchResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceUploadResource - errors', () => {
      it('should have a inboxServiceUploadResource function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceUploadResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetNotifications - errors', () => {
      it('should have a inboxServiceGetNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceGetNotificationsStatus - errors', () => {
      it('should have a inboxServiceGetNotificationsStatus function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceGetNotificationsStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceSendTestMessage - errors', () => {
      it('should have a inboxServiceSendTestMessage function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceSendTestMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceCopyInboxTemplate - errors', () => {
      it('should have a inboxServiceCopyInboxTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceCopyInboxTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceCreateInboxTemplateLocale - errors', () => {
      it('should have a inboxServiceCreateInboxTemplateLocale function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceCreateInboxTemplateLocale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceUpdateMessageReadStatus - errors', () => {
      it('should have a inboxServiceUpdateMessageReadStatus function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceUpdateMessageReadStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceMarkAlertNotificationRead - errors', () => {
      it('should have a inboxServiceMarkAlertNotificationRead function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceMarkAlertNotificationRead === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxServiceMarkAlertNotificationUnread - errors', () => {
      it('should have a inboxServiceMarkAlertNotificationUnread function', (done) => {
        try {
          assert.equal(true, typeof a.inboxServiceMarkAlertNotificationUnread === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceSubscribeCurrentUserToRule - errors', () => {
      it('should have a inboxRulesServiceSubscribeCurrentUserToRule function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceSubscribeCurrentUserToRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.inboxRulesServiceSubscribeCurrentUserToRule(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceSubscribeCurrentUserToRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceUnsubscribeCurrentUserFromRule - errors', () => {
      it('should have a inboxRulesServiceUnsubscribeCurrentUserFromRule function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceUnsubscribeCurrentUserFromRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.inboxRulesServiceUnsubscribeCurrentUserFromRule(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceUnsubscribeCurrentUserFromRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceSearchInboxRules - errors', () => {
      it('should have a inboxRulesServiceSearchInboxRules function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceSearchInboxRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceCreateInboxRule - errors', () => {
      it('should have a inboxRulesServiceCreateInboxRule function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceCreateInboxRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceGetInboxRule - errors', () => {
      it('should have a inboxRulesServiceGetInboxRule function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceGetInboxRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inboxRulesServiceGetInboxRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceGetInboxRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServicePatchInboxRule - errors', () => {
      it('should have a inboxRulesServicePatchInboxRule function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServicePatchInboxRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inboxRulesServicePatchInboxRule(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServicePatchInboxRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceGetInboxRuleStub - errors', () => {
      it('should have a inboxRulesServiceGetInboxRuleStub function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceGetInboxRuleStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceGetInboxRuleCondition - errors', () => {
      it('should have a inboxRulesServiceGetInboxRuleCondition function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceGetInboxRuleCondition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inboxRulesServiceGetInboxRuleCondition(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceGetInboxRuleCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceSearchInboxRuleConditions - errors', () => {
      it('should have a inboxRulesServiceSearchInboxRuleConditions function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceSearchInboxRuleConditions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inboxRulesServiceSearchInboxRuleConditions(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceSearchInboxRuleConditions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceSearchSubscribers - errors', () => {
      it('should have a inboxRulesServiceSearchSubscribers function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceSearchSubscribers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboxRuleId', (done) => {
        try {
          a.inboxRulesServiceSearchSubscribers(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'inboxRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceSearchSubscribers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceSearchLog - errors', () => {
      it('should have a inboxRulesServiceSearchLog function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceSearchLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboxRuleId', (done) => {
        try {
          a.inboxRulesServiceSearchLog(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'inboxRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceSearchLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServicePatchInboxRuleActions - errors', () => {
      it('should have a inboxRulesServicePatchInboxRuleActions function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServicePatchInboxRuleActions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.inboxRulesServicePatchInboxRuleActions(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServicePatchInboxRuleActions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServicePatchInboxRuleSubscribers - errors', () => {
      it('should have a inboxRulesServicePatchInboxRuleSubscribers function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServicePatchInboxRuleSubscribers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.inboxRulesServicePatchInboxRuleSubscribers(null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServicePatchInboxRuleSubscribers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceCopyInboxRule - errors', () => {
      it('should have a inboxRulesServiceCopyInboxRule function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceCopyInboxRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceUpdateInboxRuleCondition - errors', () => {
      it('should have a inboxRulesServiceUpdateInboxRuleCondition function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceUpdateInboxRuleCondition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboxRuleId', (done) => {
        try {
          a.inboxRulesServiceUpdateInboxRuleCondition(null, null, (data, error) => {
            try {
              const displayE = 'inboxRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceUpdateInboxRuleCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inboxRulesServiceCreateInboxRuleCondition - errors', () => {
      it('should have a inboxRulesServiceCreateInboxRuleCondition function', (done) => {
        try {
          assert.equal(true, typeof a.inboxRulesServiceCreateInboxRuleCondition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inboxRuleId', (done) => {
        try {
          a.inboxRulesServiceCreateInboxRuleCondition(null, null, (data, error) => {
            try {
              const displayE = 'inboxRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-inboxRulesServiceCreateInboxRuleCondition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceGet - errors', () => {
      it('should have a ipAddressRestrictionsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceUpdate - errors', () => {
      it('should have a ipAddressRestrictionsServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceDelete - errors', () => {
      it('should have a ipAddressRestrictionsServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceGetGroup - errors', () => {
      it('should have a ipAddressRestrictionsServiceGetGroup function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceGetGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.ipAddressRestrictionsServiceGetGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceGetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceGetGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceGetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceDeleteGroupIpRestriction - errors', () => {
      it('should have a ipAddressRestrictionsServiceDeleteGroupIpRestriction function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceDeleteGroupIpRestriction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.ipAddressRestrictionsServiceDeleteGroupIpRestriction(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceDeleteGroupIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceDeleteGroupIpRestriction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceDeleteGroupIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceGetUserIpRestriction - errors', () => {
      it('should have a ipAddressRestrictionsServiceGetUserIpRestriction function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceGetUserIpRestriction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceGetUserIpRestriction(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceGetUserIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.ipAddressRestrictionsServiceGetUserIpRestriction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceGetUserIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceDeleteUserIpRestriction - errors', () => {
      it('should have a ipAddressRestrictionsServiceDeleteUserIpRestriction function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceDeleteUserIpRestriction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceDeleteUserIpRestriction(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceDeleteUserIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.ipAddressRestrictionsServiceDeleteUserIpRestriction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceDeleteUserIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceSearch - errors', () => {
      it('should have a ipAddressRestrictionsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceCreate - errors', () => {
      it('should have a ipAddressRestrictionsServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceSearchGroups - errors', () => {
      it('should have a ipAddressRestrictionsServiceSearchGroups function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceSearchGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceSearchGroups(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceSearchGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceCreateGroupIpRestriction - errors', () => {
      it('should have a ipAddressRestrictionsServiceCreateGroupIpRestriction function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceCreateGroupIpRestriction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceCreateGroupIpRestriction(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceCreateGroupIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceGetAllByGroup - errors', () => {
      it('should have a ipAddressRestrictionsServiceGetAllByGroup function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceGetAllByGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceGetAllByGroup(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceGetAllByGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceSearchUsers - errors', () => {
      it('should have a ipAddressRestrictionsServiceSearchUsers function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceSearchUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceSearchUsers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceSearchUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceCreateUserIpRestriction - errors', () => {
      it('should have a ipAddressRestrictionsServiceCreateUserIpRestriction function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceCreateUserIpRestriction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceCreateUserIpRestriction(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceCreateUserIpRestriction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipAddressRestrictionsServiceGetAllByUser - errors', () => {
      it('should have a ipAddressRestrictionsServiceGetAllByUser function', (done) => {
        try {
          assert.equal(true, typeof a.ipAddressRestrictionsServiceGetAllByUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ipAddressRestrictionsServiceGetAllByUser(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ipAddressRestrictionsServiceGetAllByUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceDeleteJumpboxRouteLevel - errors', () => {
      it('should have a jumpboxRouteServiceDeleteJumpboxRouteLevel function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceDeleteJumpboxRouteLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jumpboxRouteServiceDeleteJumpboxRouteLevel(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServiceDeleteJumpboxRouteLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing order', (done) => {
        try {
          a.jumpboxRouteServiceDeleteJumpboxRouteLevel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'order is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServiceDeleteJumpboxRouteLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServicePatchJumpboxRouteLevel - errors', () => {
      it('should have a jumpboxRouteServicePatchJumpboxRouteLevel function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServicePatchJumpboxRouteLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jumpboxRouteServicePatchJumpboxRouteLevel(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServicePatchJumpboxRouteLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing order', (done) => {
        try {
          a.jumpboxRouteServicePatchJumpboxRouteLevel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'order is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServicePatchJumpboxRouteLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceSearch - errors', () => {
      it('should have a jumpboxRouteServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceGetAllRoutesForUser - errors', () => {
      it('should have a jumpboxRouteServiceGetAllRoutesForUser function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceGetAllRoutesForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceGet - errors', () => {
      it('should have a jumpboxRouteServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jumpboxRouteServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceUpdateJumpboxRoute - errors', () => {
      it('should have a jumpboxRouteServiceUpdateJumpboxRoute function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceUpdateJumpboxRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jumpboxRouteServiceUpdateJumpboxRoute(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServiceUpdateJumpboxRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceGetJumpboxAudits - errors', () => {
      it('should have a jumpboxRouteServiceGetJumpboxAudits function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceGetJumpboxAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jumpboxRouteServiceGetJumpboxAudits(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServiceGetJumpboxAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceGetAllLevels - errors', () => {
      it('should have a jumpboxRouteServiceGetAllLevels function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceGetAllLevels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jumpboxRouteServiceGetAllLevels(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServiceGetAllLevels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceAddJumpboxRouteLevel - errors', () => {
      it('should have a jumpboxRouteServiceAddJumpboxRouteLevel function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceAddJumpboxRouteLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.jumpboxRouteServiceAddJumpboxRouteLevel(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-jumpboxRouteServiceAddJumpboxRouteLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jumpboxRouteServiceCreatejumpboxRoute - errors', () => {
      it('should have a jumpboxRouteServiceCreatejumpboxRoute function', (done) => {
        try {
          assert.equal(true, typeof a.jumpboxRouteServiceCreatejumpboxRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceGet - errors', () => {
      it('should have a keyManagementServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceUpdate - errors', () => {
      it('should have a keyManagementServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceStub - errors', () => {
      it('should have a keyManagementServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceGetMasterEncryptionKeyRotationInfo - errors', () => {
      it('should have a keyManagementServiceGetMasterEncryptionKeyRotationInfo function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceGetMasterEncryptionKeyRotationInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceSearchKeyRotationAudit - errors', () => {
      it('should have a keyManagementServiceSearchKeyRotationAudit function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceSearchKeyRotationAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceGetKeyRotationAuditUsers - errors', () => {
      it('should have a keyManagementServiceGetKeyRotationAuditUsers function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceGetKeyRotationAuditUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceStartMasterEncryptionKeyRotation - errors', () => {
      it('should have a keyManagementServiceStartMasterEncryptionKeyRotation function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceStartMasterEncryptionKeyRotation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keyManagementServiceRetryMasterEncryptionKeyStatus - errors', () => {
      it('should have a keyManagementServiceRetryMasterEncryptionKeyStatus function', (done) => {
        try {
          assert.equal(true, typeof a.keyManagementServiceRetryMasterEncryptionKeyStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceSearchCollections - errors', () => {
      it('should have a launcherAgentsServiceSearchCollections function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceSearchCollections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceCreateCollection - errors', () => {
      it('should have a launcherAgentsServiceCreateCollection function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceCreateCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceStub - errors', () => {
      it('should have a launcherAgentsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceGetByCollectionId - errors', () => {
      it('should have a launcherAgentsServiceGetByCollectionId function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceGetByCollectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.launcherAgentsServiceGetByCollectionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-launcherAgentsServiceGetByCollectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceUpdateCollection - errors', () => {
      it('should have a launcherAgentsServiceUpdateCollection function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceUpdateCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.launcherAgentsServiceUpdateCollection(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-launcherAgentsServiceUpdateCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceSearchAgentsWithIssues - errors', () => {
      it('should have a launcherAgentsServiceSearchAgentsWithIssues function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceSearchAgentsWithIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceSearch - errors', () => {
      it('should have a launcherAgentsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceGet - errors', () => {
      it('should have a launcherAgentsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.launcherAgentsServiceGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-launcherAgentsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launcherAgentsServiceUpdate - errors', () => {
      it('should have a launcherAgentsServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.launcherAgentsServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.launcherAgentsServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-launcherAgentsServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchersServiceSearchLaunchers - errors', () => {
      it('should have a launchersServiceSearchLaunchers function', (done) => {
        try {
          assert.equal(true, typeof a.launchersServiceSearchLaunchers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchersServiceLookup - errors', () => {
      it('should have a launchersServiceLookup function', (done) => {
        try {
          assert.equal(true, typeof a.launchersServiceLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchersServiceGet - errors', () => {
      it('should have a launchersServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.launchersServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.launchersServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-launchersServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchersServiceSearchLauncherDetailsV2 - errors', () => {
      it('should have a launchersServiceSearchLauncherDetailsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.launchersServiceSearchLauncherDetailsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchersServiceTriggerDownload - errors', () => {
      it('should have a launchersServiceTriggerDownload function', (done) => {
        try {
          assert.equal(true, typeof a.launchersServiceTriggerDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchersServiceCreate - errors', () => {
      it('should have a launchersServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.launchersServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#licenseServiceGet - errors', () => {
      it('should have a licenseServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.licenseServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceDeleteMetadata - errors', () => {
      it('should have a metadataServiceDeleteMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceDeleteMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.metadataServiceDeleteMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceDeleteMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemType', (done) => {
        try {
          a.metadataServiceDeleteMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceDeleteMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metadataItemDataId', (done) => {
        try {
          a.metadataServiceDeleteMetadata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'metadataItemDataId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceDeleteMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceSearchMetadata - errors', () => {
      it('should have a metadataServiceSearchMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceSearchMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceSearchMetadataHistory - errors', () => {
      it('should have a metadataServiceSearchMetadataHistory function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceSearchMetadataHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceGetFieldSections - errors', () => {
      it('should have a metadataServiceGetFieldSections function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceGetFieldSections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceGetFields - errors', () => {
      it('should have a metadataServiceGetFields function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceGetFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceCreateMetadata - errors', () => {
      it('should have a metadataServiceCreateMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceCreateMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.metadataServiceCreateMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceCreateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemType', (done) => {
        try {
          a.metadataServiceCreateMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceCreateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceUpdateMetadata - errors', () => {
      it('should have a metadataServiceUpdateMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceUpdateMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.metadataServiceUpdateMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceUpdateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemType', (done) => {
        try {
          a.metadataServiceUpdateMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceUpdateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metadataServiceUpdateMetadataFieldSection - errors', () => {
      it('should have a metadataServiceUpdateMetadataFieldSection function', (done) => {
        try {
          assert.equal(true, typeof a.metadataServiceUpdateMetadataFieldSection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldSectionId', (done) => {
        try {
          a.metadataServiceUpdateMetadataFieldSection(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldSectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-metadataServiceUpdateMetadataFieldSection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mobileServiceGetMobileConfiguration - errors', () => {
      it('should have a mobileServiceGetMobileConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.mobileServiceGetMobileConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oAuthExpirationServiceExpireSession - errors', () => {
      it('should have a oAuthExpirationServiceExpireSession function', (done) => {
        try {
          assert.equal(true, typeof a.oAuthExpirationServiceExpireSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oneTimePasswordCodeServiceGet - errors', () => {
      it('should have a oneTimePasswordCodeServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.oneTimePasswordCodeServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.oneTimePasswordCodeServiceGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-oneTimePasswordCodeServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#passwordRequirementsServiceSearchPasswordRequirements - errors', () => {
      it('should have a passwordRequirementsServiceSearchPasswordRequirements function', (done) => {
        try {
          assert.equal(true, typeof a.passwordRequirementsServiceSearchPasswordRequirements === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#passwordRequirementsServiceCreate - errors', () => {
      it('should have a passwordRequirementsServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.passwordRequirementsServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#passwordRequirementsServiceGet - errors', () => {
      it('should have a passwordRequirementsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.passwordRequirementsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.passwordRequirementsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-passwordRequirementsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#passwordRequirementsServicePatch - errors', () => {
      it('should have a passwordRequirementsServicePatch function', (done) => {
        try {
          assert.equal(true, typeof a.passwordRequirementsServicePatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.passwordRequirementsServicePatch(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-passwordRequirementsServicePatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#passwordRequirementsServiceUpdateRules - errors', () => {
      it('should have a passwordRequirementsServiceUpdateRules function', (done) => {
        try {
          assert.equal(true, typeof a.passwordRequirementsServiceUpdateRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.passwordRequirementsServiceUpdateRules(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-passwordRequirementsServiceUpdateRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pbaConfigurationServiceGetPbaConfiguration - errors', () => {
      it('should have a pbaConfigurationServiceGetPbaConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.pbaConfigurationServiceGetPbaConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pbaConfigurationServiceUpdatePbaConfiguration - errors', () => {
      it('should have a pbaConfigurationServiceUpdatePbaConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.pbaConfigurationServiceUpdatePbaConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pbaConfigurationServiceProcessPbaHistoryImport - errors', () => {
      it('should have a pbaConfigurationServiceProcessPbaHistoryImport function', (done) => {
        try {
          assert.equal(true, typeof a.pbaConfigurationServiceProcessPbaHistoryImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pbaConfigurationServiceTestPbaConnection - errors', () => {
      it('should have a pbaConfigurationServiceTestPbaConnection function', (done) => {
        try {
          assert.equal(true, typeof a.pbaConfigurationServiceTestPbaConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pbaConfigurationServiceConfirmPbaPair - errors', () => {
      it('should have a pbaConfigurationServiceConfirmPbaPair function', (done) => {
        try {
          assert.equal(true, typeof a.pbaConfigurationServiceConfirmPbaPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pbaConfigurationServiceSyncPbaMetadata - errors', () => {
      it('should have a pbaConfigurationServiceSyncPbaMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.pbaConfigurationServiceSyncPbaMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceDeleteTerminalClientOverrides - errors', () => {
      it('should have a proxyServiceDeleteTerminalClientOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceDeleteTerminalClientOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientOverrideId', (done) => {
        try {
          a.proxyServiceDeleteTerminalClientOverrides(null, (data, error) => {
            try {
              const displayE = 'clientOverrideId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-proxyServiceDeleteTerminalClientOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceUpdateTerminalClientOverrides - errors', () => {
      it('should have a proxyServiceUpdateTerminalClientOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceUpdateTerminalClientOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientOverrideId', (done) => {
        try {
          a.proxyServiceUpdateTerminalClientOverrides(null, null, (data, error) => {
            try {
              const displayE = 'clientOverrideId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-proxyServiceUpdateTerminalClientOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetSshProxyConfiguration - errors', () => {
      it('should have a proxyServiceGetSshProxyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetSshProxyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServicePatchSshProxyConfiguration - errors', () => {
      it('should have a proxyServicePatchSshProxyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServicePatchSshProxyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetRdpProxyConfiguration - errors', () => {
      it('should have a proxyServiceGetRdpProxyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetRdpProxyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServicePatchRdpProxyConfiguration - errors', () => {
      it('should have a proxyServicePatchRdpProxyConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServicePatchRdpProxyConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetExplanations - errors', () => {
      it('should have a proxyServiceGetExplanations function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetExplanations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetAudits - errors', () => {
      it('should have a proxyServiceGetAudits function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetEndpoints - errors', () => {
      it('should have a proxyServiceGetEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetSshEndpointNotification - errors', () => {
      it('should have a proxyServiceGetSshEndpointNotification function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetSshEndpointNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetRdpEndpointNotification - errors', () => {
      it('should have a proxyServiceGetRdpEndpointNotification function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetRdpEndpointNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetEndpointNotification - errors', () => {
      it('should have a proxyServiceGetEndpointNotification function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetEndpointNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetTerminalClients - errors', () => {
      it('should have a proxyServiceGetTerminalClients function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetTerminalClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetTerminalClientHistory - errors', () => {
      it('should have a proxyServiceGetTerminalClientHistory function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetTerminalClientHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetTerminalClientOverrides - errors', () => {
      it('should have a proxyServiceGetTerminalClientOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetTerminalClientOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceCreateTerminalClientOverrides - errors', () => {
      it('should have a proxyServiceCreateTerminalClientOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceCreateTerminalClientOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGetProxyingState - errors', () => {
      it('should have a proxyServiceGetProxyingState function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGetProxyingState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServicePatchNode - errors', () => {
      it('should have a proxyServicePatchNode function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServicePatchNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.proxyServicePatchNode(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-proxyServicePatchNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServicePatchSite - errors', () => {
      it('should have a proxyServicePatchSite function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServicePatchSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.proxyServicePatchSite(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-proxyServicePatchSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServicePatchEngine - errors', () => {
      it('should have a proxyServicePatchEngine function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServicePatchEngine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.proxyServicePatchEngine(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-proxyServicePatchEngine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceUpdateTerminalClientType - errors', () => {
      it('should have a proxyServiceUpdateTerminalClientType function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceUpdateTerminalClientType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.proxyServiceUpdateTerminalClientType(null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-proxyServiceUpdateTerminalClientType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGenerateSshHostKey - errors', () => {
      it('should have a proxyServiceGenerateSshHostKey function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGenerateSshHostKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#proxyServiceGenerateRdpProxyCertificate - errors', () => {
      it('should have a proxyServiceGenerateRdpProxyCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.proxyServiceGenerateRdpProxyCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceGetPasswordType - errors', () => {
      it('should have a remotePasswordChangingServiceGetPasswordType function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceGetPasswordType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.remotePasswordChangingServiceGetPasswordType(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-remotePasswordChangingServiceGetPasswordType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceUpdatePasswordType - errors', () => {
      it('should have a remotePasswordChangingServiceUpdatePasswordType function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceUpdatePasswordType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.remotePasswordChangingServiceUpdatePasswordType(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-remotePasswordChangingServiceUpdatePasswordType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceDeletePasswordType - errors', () => {
      it('should have a remotePasswordChangingServiceDeletePasswordType function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceDeletePasswordType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.remotePasswordChangingServiceDeletePasswordType(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-remotePasswordChangingServiceDeletePasswordType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceGetCustomCommands - errors', () => {
      it('should have a remotePasswordChangingServiceGetCustomCommands function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceGetCustomCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.remotePasswordChangingServiceGetCustomCommands(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-remotePasswordChangingServiceGetCustomCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceUpdateCustomCommand - errors', () => {
      it('should have a remotePasswordChangingServiceUpdateCustomCommand function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceUpdateCustomCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.remotePasswordChangingServiceUpdateCustomCommand(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-remotePasswordChangingServiceUpdateCustomCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceDeleteCustomCommand - errors', () => {
      it('should have a remotePasswordChangingServiceDeleteCustomCommand function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceDeleteCustomCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.remotePasswordChangingServiceDeleteCustomCommand(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-remotePasswordChangingServiceDeleteCustomCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceGetPasswordTypeList - errors', () => {
      it('should have a remotePasswordChangingServiceGetPasswordTypeList function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceGetPasswordTypeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceCreatePasswordType - errors', () => {
      it('should have a remotePasswordChangingServiceCreatePasswordType function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceCreatePasswordType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remotePasswordChangingServiceCreateCustomCommand - errors', () => {
      it('should have a remotePasswordChangingServiceCreateCustomCommand function', (done) => {
        try {
          assert.equal(true, typeof a.remotePasswordChangingServiceCreateCustomCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportDetail - errors', () => {
      it('should have a reportsServiceGetReportDetail function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reportsServiceGetReportDetail(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceUpdateReport - errors', () => {
      it('should have a reportsServiceUpdateReport function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceUpdateReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reportsServiceUpdateReport(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceUpdateReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceDelete - errors', () => {
      it('should have a reportsServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reportsServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceDeleteV2 - errors', () => {
      it('should have a reportsServiceDeleteV2 function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceDeleteV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reportsServiceDeleteV2(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceDeleteV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportCategory - errors', () => {
      it('should have a reportsServiceGetReportCategory function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportCategoryId', (done) => {
        try {
          a.reportsServiceGetReportCategory(null, (data, error) => {
            try {
              const displayE = 'reportCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceDeleteReportCategory - errors', () => {
      it('should have a reportsServiceDeleteReportCategory function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceDeleteReportCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportCategoryId', (done) => {
        try {
          a.reportsServiceDeleteReportCategory(null, (data, error) => {
            try {
              const displayE = 'reportCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceDeleteReportCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceUpdateReportCategory - errors', () => {
      it('should have a reportsServiceUpdateReportCategory function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceUpdateReportCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportCategoryId', (done) => {
        try {
          a.reportsServiceUpdateReportCategory(null, null, (data, error) => {
            try {
              const displayE = 'reportCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceUpdateReportCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceDeleteReportCategoryV2 - errors', () => {
      it('should have a reportsServiceDeleteReportCategoryV2 function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceDeleteReportCategoryV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportCategoryId', (done) => {
        try {
          a.reportsServiceDeleteReportCategoryV2(null, (data, error) => {
            try {
              const displayE = 'reportCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceDeleteReportCategoryV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportSchedule - errors', () => {
      it('should have a reportsServiceGetReportSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleId', (done) => {
        try {
          a.reportsServiceGetReportSchedule(null, (data, error) => {
            try {
              const displayE = 'reportScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceDeleteReportSchedule - errors', () => {
      it('should have a reportsServiceDeleteReportSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceDeleteReportSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleId', (done) => {
        try {
          a.reportsServiceDeleteReportSchedule(null, (data, error) => {
            try {
              const displayE = 'reportScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceDeleteReportSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceUpdateReportSchedule - errors', () => {
      it('should have a reportsServiceUpdateReportSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceUpdateReportSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleId', (done) => {
        try {
          a.reportsServiceUpdateReportSchedule(null, null, (data, error) => {
            try {
              const displayE = 'reportScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceUpdateReportSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceDeleteReportScheduleV2 - errors', () => {
      it('should have a reportsServiceDeleteReportScheduleV2 function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceDeleteReportScheduleV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleId', (done) => {
        try {
          a.reportsServiceDeleteReportScheduleV2(null, (data, error) => {
            try {
              const displayE = 'reportScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceDeleteReportScheduleV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportAuditsById - errors', () => {
      it('should have a reportsServiceGetReportAuditsById function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportAuditsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reportsServiceGetReportAuditsById(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportAuditsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportAudits - errors', () => {
      it('should have a reportsServiceGetReportAudits function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceSearchReportSummary - errors', () => {
      it('should have a reportsServiceSearchReportSummary function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceSearchReportSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceCreateReport - errors', () => {
      it('should have a reportsServiceCreateReport function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceCreateReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceLookup - errors', () => {
      it('should have a reportsServiceLookup function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetCategories - errors', () => {
      it('should have a reportsServiceGetCategories function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceCreateReportCategory - errors', () => {
      it('should have a reportsServiceCreateReportCategory function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceCreateReportCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetChartTypes - errors', () => {
      it('should have a reportsServiceGetChartTypes function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetChartTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetDefaultParameters - errors', () => {
      it('should have a reportsServiceGetDefaultParameters function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetDefaultParameters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reportsServiceGetDefaultParameters(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetDefaultParameters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceSearchReportSchedules - errors', () => {
      it('should have a reportsServiceSearchReportSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceSearchReportSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceCreateReportSchedule - errors', () => {
      it('should have a reportsServiceCreateReportSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceCreateReportSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceStubReportSchedule - errors', () => {
      it('should have a reportsServiceStubReportSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceStubReportSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.reportsServiceStubReportSchedule(null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceStubReportSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceSearchReportScheduleHistory - errors', () => {
      it('should have a reportsServiceSearchReportScheduleHistory function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceSearchReportScheduleHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleId', (done) => {
        try {
          a.reportsServiceSearchReportScheduleHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceSearchReportScheduleHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportScheduleHistory - errors', () => {
      it('should have a reportsServiceGetReportScheduleHistory function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportScheduleHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleHistoryId', (done) => {
        try {
          a.reportsServiceGetReportScheduleHistory(null, (data, error) => {
            try {
              const displayE = 'reportScheduleHistoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportScheduleHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportPermissions - errors', () => {
      it('should have a reportsServiceGetReportPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.reportsServiceGetReportPermissions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceUpdateReportPermissions - errors', () => {
      it('should have a reportsServiceUpdateReportPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceUpdateReportPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.reportsServiceUpdateReportPermissions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceUpdateReportPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportPermissionsFromCategory - errors', () => {
      it('should have a reportsServiceGetReportPermissionsFromCategory function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportPermissionsFromCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportCategoryId', (done) => {
        try {
          a.reportsServiceGetReportPermissionsFromCategory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportPermissionsFromCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportCategoryPermissions - errors', () => {
      it('should have a reportsServiceGetReportCategoryPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportCategoryPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportCategoryId', (done) => {
        try {
          a.reportsServiceGetReportCategoryPermissions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceGetReportCategoryPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceUpdateReportCategoryPermissions - errors', () => {
      it('should have a reportsServiceUpdateReportCategoryPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceUpdateReportCategoryPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportCategoryId', (done) => {
        try {
          a.reportsServiceUpdateReportCategoryPermissions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'reportCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceUpdateReportCategoryPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportPermissionOptions - errors', () => {
      it('should have a reportsServiceGetReportPermissionOptions function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportPermissionOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceGetReportCategoryPermissionOptions - errors', () => {
      it('should have a reportsServiceGetReportCategoryPermissionOptions function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceGetReportCategoryPermissionOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceExport - errors', () => {
      it('should have a reportsServiceExport function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceExecute - errors', () => {
      it('should have a reportsServiceExecute function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceExecute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceEmail - errors', () => {
      it('should have a reportsServiceEmail function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reportsServiceEmail(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceDownloadHistoricalReport - errors', () => {
      it('should have a reportsServiceDownloadHistoricalReport function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceDownloadHistoricalReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleHistoryId', (done) => {
        try {
          a.reportsServiceDownloadHistoricalReport(null, (data, error) => {
            try {
              const displayE = 'reportScheduleHistoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceDownloadHistoricalReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceUndeleteReportSchedule - errors', () => {
      it('should have a reportsServiceUndeleteReportSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceUndeleteReportSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportScheduleId', (done) => {
        try {
          a.reportsServiceUndeleteReportSchedule(null, (data, error) => {
            try {
              const displayE = 'reportScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceUndeleteReportSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportsServiceUndeleteSystemReport - errors', () => {
      it('should have a reportsServiceUndeleteSystemReport function', (done) => {
        try {
          assert.equal(true, typeof a.reportsServiceUndeleteSystemReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportId', (done) => {
        try {
          a.reportsServiceUndeleteSystemReport(null, (data, error) => {
            try {
              const displayE = 'reportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-reportsServiceUndeleteSystemReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#roleAuditServiceGetRoleAudits - errors', () => {
      it('should have a roleAuditServiceGetRoleAudits function', (done) => {
        try {
          assert.equal(true, typeof a.roleAuditServiceGetRoleAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing isExporting', (done) => {
        try {
          a.roleAuditServiceGetRoleAudits(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'isExporting is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-roleAuditServiceGetRoleAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolePermissionsServiceGetUnassigned - errors', () => {
      it('should have a rolePermissionsServiceGetUnassigned function', (done) => {
        try {
          assert.equal(true, typeof a.rolePermissionsServiceGetUnassigned === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceGet - errors', () => {
      it('should have a rolesServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-rolesServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceUpdate - errors', () => {
      it('should have a rolesServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesServiceUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-rolesServiceUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceGetAll - errors', () => {
      it('should have a rolesServiceGetAll function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceGetAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceCreate - errors', () => {
      it('should have a rolesServiceCreate function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceGetRoleGroups - errors', () => {
      it('should have a rolesServiceGetRoleGroups function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceGetRoleGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesServiceGetRoleGroups(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-rolesServiceGetRoleGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceGetRolePermissions - errors', () => {
      it('should have a rolesServiceGetRolePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceGetRolePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesServiceGetRolePermissions(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-rolesServiceGetRolePermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceUpdatePermissions - errors', () => {
      it('should have a rolesServiceUpdatePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceUpdatePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesServiceUpdatePermissions(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-rolesServiceUpdatePermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceGetAllRolePermissionsByType - errors', () => {
      it('should have a rolesServiceGetAllRolePermissionsByType function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceGetAllRolePermissionsByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rolesServiceGetAllRolePermissionsByType(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-rolesServiceGetAllRolePermissionsByType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServiceStub - errors', () => {
      it('should have a rolesServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesServicePatchGroups - errors', () => {
      it('should have a rolesServicePatchGroups function', (done) => {
        try {
          assert.equal(true, typeof a.rolesServicePatchGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.rolesServicePatchGroups(null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-rolesServicePatchGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleServiceGetSchedule - errors', () => {
      it('should have a scheduleServiceGetSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.scheduleServiceGetSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleId', (done) => {
        try {
          a.scheduleServiceGetSchedule(null, (data, error) => {
            try {
              const displayE = 'scheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-scheduleServiceGetSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleServiceUpdateSchedule - errors', () => {
      it('should have a scheduleServiceUpdateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.scheduleServiceUpdateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleId', (done) => {
        try {
          a.scheduleServiceUpdateSchedule(null, null, (data, error) => {
            try {
              const displayE = 'scheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-scheduleServiceUpdateSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleServiceCreateSchedule - errors', () => {
      it('should have a scheduleServiceCreateSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.scheduleServiceCreateSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scriptServiceSearch - errors', () => {
      it('should have a scriptServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.scriptServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scriptServicePost - errors', () => {
      it('should have a scriptServicePost function', (done) => {
        try {
          assert.equal(true, typeof a.scriptServicePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scriptServiceLoad - errors', () => {
      it('should have a scriptServiceLoad function', (done) => {
        try {
          assert.equal(true, typeof a.scriptServiceLoad === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.scriptServiceLoad(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-scriptServiceLoad', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAccountsServiceSearchClientAccounts - errors', () => {
      it('should have a sdkClientAccountsServiceSearchClientAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAccountsServiceSearchClientAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAccountsServiceCreateClientAccount - errors', () => {
      it('should have a sdkClientAccountsServiceCreateClientAccount function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAccountsServiceCreateClientAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAccountsServiceGet - errors', () => {
      it('should have a sdkClientAccountsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAccountsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sdkClientAccountsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sdkClientAccountsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAccountsServiceUpdateClientAccount - errors', () => {
      it('should have a sdkClientAccountsServiceUpdateClientAccount function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAccountsServiceUpdateClientAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sdkClientAccountsServiceUpdateClientAccount(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sdkClientAccountsServiceUpdateClientAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAccountsServiceGetEnabled - errors', () => {
      it('should have a sdkClientAccountsServiceGetEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAccountsServiceGetEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAccountsServiceToggleEnabled - errors', () => {
      it('should have a sdkClientAccountsServiceToggleEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAccountsServiceToggleEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAccountsServiceRevoke - errors', () => {
      it('should have a sdkClientAccountsServiceRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAccountsServiceRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sdkClientAccountsServiceRevoke(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sdkClientAccountsServiceRevoke', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientAuditsServiceSearchClientAudit - errors', () => {
      it('should have a sdkClientAuditsServiceSearchClientAudit function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientAuditsServiceSearchClientAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientRulesServiceGet - errors', () => {
      it('should have a sdkClientRulesServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientRulesServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sdkClientRulesServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sdkClientRulesServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientRulesServiceUpdateClientRule - errors', () => {
      it('should have a sdkClientRulesServiceUpdateClientRule function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientRulesServiceUpdateClientRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sdkClientRulesServiceUpdateClientRule(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sdkClientRulesServiceUpdateClientRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientRulesServiceDelete - errors', () => {
      it('should have a sdkClientRulesServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientRulesServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sdkClientRulesServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sdkClientRulesServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientRulesServiceSearchClientRules - errors', () => {
      it('should have a sdkClientRulesServiceSearchClientRules function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientRulesServiceSearchClientRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientRulesServiceCreateClientRule - errors', () => {
      it('should have a sdkClientRulesServiceCreateClientRule function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientRulesServiceCreateClientRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientRulesServiceGetOnboardingKey - errors', () => {
      it('should have a sdkClientRulesServiceGetOnboardingKey function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientRulesServiceGetOnboardingKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sdkClientRulesServiceGetOnboardingKey(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sdkClientRulesServiceGetOnboardingKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sdkClientRulesServiceStub - errors', () => {
      it('should have a sdkClientRulesServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.sdkClientRulesServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceSearchRequests - errors', () => {
      it('should have a secretAccessRequestsServiceSearchRequests function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceSearchRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceUpdateRequest - errors', () => {
      it('should have a secretAccessRequestsServiceUpdateRequest function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceUpdateRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceCreateRequest - errors', () => {
      it('should have a secretAccessRequestsServiceCreateRequest function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceCreateRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceGetHistory - errors', () => {
      it('should have a secretAccessRequestsServiceGetHistory function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceGetHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretAccessRequestsServiceGetHistory(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretAccessRequestsServiceGetHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceGetRequestsForSecret - errors', () => {
      it('should have a secretAccessRequestsServiceGetRequestsForSecret function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceGetRequestsForSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretAccessRequestsServiceGetRequestsForSecret(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretAccessRequestsServiceGetRequestsForSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceGetRequest - errors', () => {
      it('should have a secretAccessRequestsServiceGetRequest function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceGetRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretAccessRequestsServiceGetRequest(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretAccessRequestsServiceGetRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceGetPendingRequest - errors', () => {
      it('should have a secretAccessRequestsServiceGetPendingRequest function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceGetPendingRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretAccessRequestsServiceGetPendingRequest(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretAccessRequestsServiceGetPendingRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceGetOptionsBySecret - errors', () => {
      it('should have a secretAccessRequestsServiceGetOptionsBySecret function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceGetOptionsBySecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretAccessRequestsServiceGetOptionsBySecret(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretAccessRequestsServiceGetOptionsBySecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretAccessRequestsServiceCreateViewComment - errors', () => {
      it('should have a secretAccessRequestsServiceCreateViewComment function', (done) => {
        try {
          assert.equal(true, typeof a.secretAccessRequestsServiceCreateViewComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretAccessRequestsServiceCreateViewComment(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretAccessRequestsServiceCreateViewComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceGetDependency - errors', () => {
      it('should have a secretDependenciesServiceGetDependency function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceGetDependency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretDependenciesServiceGetDependency(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretDependenciesServiceGetDependency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceUpdateDependency - errors', () => {
      it('should have a secretDependenciesServiceUpdateDependency function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceUpdateDependency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretDependenciesServiceUpdateDependency(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretDependenciesServiceUpdateDependency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceDelete - errors', () => {
      it('should have a secretDependenciesServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretDependenciesServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretDependenciesServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceGetGroups - errors', () => {
      it('should have a secretDependenciesServiceGetGroups function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceGetGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretDependenciesServiceGetGroups(null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretDependenciesServiceGetGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceCreateDependencyGroup - errors', () => {
      it('should have a secretDependenciesServiceCreateDependencyGroup function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceCreateDependencyGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretDependenciesServiceCreateDependencyGroup(null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretDependenciesServiceCreateDependencyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceGetScripts - errors', () => {
      it('should have a secretDependenciesServiceGetScripts function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceGetScripts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceGetTemplates - errors', () => {
      it('should have a secretDependenciesServiceGetTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceGetTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceSearchDependencySummary - errors', () => {
      it('should have a secretDependenciesServiceSearchDependencySummary function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceSearchDependencySummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filterSecretId', (done) => {
        try {
          a.secretDependenciesServiceSearchDependencySummary('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filterSecretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretDependenciesServiceSearchDependencySummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceCreateDependency - errors', () => {
      it('should have a secretDependenciesServiceCreateDependency function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceCreateDependency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceGetDependencyRunTaskStatus - errors', () => {
      it('should have a secretDependenciesServiceGetDependencyRunTaskStatus function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceGetDependencyRunTaskStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identifier', (done) => {
        try {
          a.secretDependenciesServiceGetDependencyRunTaskStatus(null, (data, error) => {
            try {
              const displayE = 'identifier is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretDependenciesServiceGetDependencyRunTaskStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceStub - errors', () => {
      it('should have a secretDependenciesServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretDependenciesServiceExecute - errors', () => {
      it('should have a secretDependenciesServiceExecute function', (done) => {
        try {
          assert.equal(true, typeof a.secretDependenciesServiceExecute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceGetHistory - errors', () => {
      it('should have a secretEraseRequestsServiceGetHistory function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceGetHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretEraseRequestsServiceGetHistory(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretEraseRequestsServiceGetHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceGetRequest - errors', () => {
      it('should have a secretEraseRequestsServiceGetRequest function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceGetRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretEraseRequestsServiceGetRequest(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretEraseRequestsServiceGetRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceGetRequestSecrets - errors', () => {
      it('should have a secretEraseRequestsServiceGetRequestSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceGetRequestSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceInboxSearch - errors', () => {
      it('should have a secretEraseRequestsServiceInboxSearch function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceInboxSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceSearch - errors', () => {
      it('should have a secretEraseRequestsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceProcessNow - errors', () => {
      it('should have a secretEraseRequestsServiceProcessNow function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceProcessNow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceUpdateRequest - errors', () => {
      it('should have a secretEraseRequestsServiceUpdateRequest function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceUpdateRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretEraseRequestsServiceCreateRequest - errors', () => {
      it('should have a secretEraseRequestsServiceCreateRequest function', (done) => {
        try {
          assert.equal(true, typeof a.secretEraseRequestsServiceCreateRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretExtensionsServiceGetWebSecretTemplates - errors', () => {
      it('should have a secretExtensionsServiceGetWebSecretTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.secretExtensionsServiceGetWebSecretTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretExtensionsServiceSearch - errors', () => {
      it('should have a secretExtensionsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.secretExtensionsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretExtensionsServiceSearchActiveDirectorySecrets - errors', () => {
      it('should have a secretExtensionsServiceSearchActiveDirectorySecrets function', (done) => {
        try {
          assert.equal(true, typeof a.secretExtensionsServiceSearchActiveDirectorySecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretExtensionsServiceSearchWindowsAccountSecrets - errors', () => {
      it('should have a secretExtensionsServiceSearchWindowsAccountSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.secretExtensionsServiceSearchWindowsAccountSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretExtensionsServiceGetAutoFillValues - errors', () => {
      it('should have a secretExtensionsServiceGetAutoFillValues function', (done) => {
        try {
          assert.equal(true, typeof a.secretExtensionsServiceGetAutoFillValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHealthServiceGetSecretExposure - errors', () => {
      it('should have a secretHealthServiceGetSecretExposure function', (done) => {
        try {
          assert.equal(true, typeof a.secretHealthServiceGetSecretExposure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceUpdateHook - errors', () => {
      it('should have a secretHooksServiceUpdateHook function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceUpdateHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretHookId', (done) => {
        try {
          a.secretHooksServiceUpdateHook(null, null, null, (data, error) => {
            try {
              const displayE = 'secretHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceUpdateHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceUpdateHook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceUpdateHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceDeleteHook - errors', () => {
      it('should have a secretHooksServiceDeleteHook function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceDeleteHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretHookId', (done) => {
        try {
          a.secretHooksServiceDeleteHook(null, null, (data, error) => {
            try {
              const displayE = 'secretHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceDeleteHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceDeleteHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceDeleteHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceGetHooks - errors', () => {
      it('should have a secretHooksServiceGetHooks function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceGetHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceGetHooks(null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceGetHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceStubHook - errors', () => {
      it('should have a secretHooksServiceStubHook function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceStubHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scriptId', (done) => {
        try {
          a.secretHooksServiceStubHook(null, null, (data, error) => {
            try {
              const displayE = 'scriptId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceStubHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceStubHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceStubHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceGetHook - errors', () => {
      it('should have a secretHooksServiceGetHook function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceGetHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretHookId', (done) => {
        try {
          a.secretHooksServiceGetHook(null, null, (data, error) => {
            try {
              const displayE = 'secretHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceGetHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceGetHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceGetHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceGetHooksV2 - errors', () => {
      it('should have a secretHooksServiceGetHooksV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceGetHooksV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceGetHooksV2(null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceGetHooksV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceStubHookV2 - errors', () => {
      it('should have a secretHooksServiceStubHookV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceStubHookV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scriptId', (done) => {
        try {
          a.secretHooksServiceStubHookV2(null, null, (data, error) => {
            try {
              const displayE = 'scriptId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceStubHookV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceStubHookV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceStubHookV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceGetHookV2 - errors', () => {
      it('should have a secretHooksServiceGetHookV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceGetHookV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretHookId', (done) => {
        try {
          a.secretHooksServiceGetHookV2(null, null, (data, error) => {
            try {
              const displayE = 'secretHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceGetHookV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceGetHookV2('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceGetHookV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceCreateHook - errors', () => {
      it('should have a secretHooksServiceCreateHook function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceCreateHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceCreateHook(null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceCreateHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceCreateHookV2 - errors', () => {
      it('should have a secretHooksServiceCreateHookV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceCreateHookV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceCreateHookV2(null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceCreateHookV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretHooksServiceUpdateHookV2 - errors', () => {
      it('should have a secretHooksServiceUpdateHookV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretHooksServiceUpdateHookV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretHookId', (done) => {
        try {
          a.secretHooksServiceUpdateHookV2(null, null, null, (data, error) => {
            try {
              const displayE = 'secretHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceUpdateHookV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretHooksServiceUpdateHookV2('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretHooksServiceUpdateHookV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPermissionsServiceGet - errors', () => {
      it('should have a secretPermissionsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.secretPermissionsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretPermissionsServiceGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPermissionsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPermissionsServiceUpdateSecretPermission - errors', () => {
      it('should have a secretPermissionsServiceUpdateSecretPermission function', (done) => {
        try {
          assert.equal(true, typeof a.secretPermissionsServiceUpdateSecretPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretPermissionsServiceUpdateSecretPermission(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPermissionsServiceUpdateSecretPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPermissionsServiceDelete - errors', () => {
      it('should have a secretPermissionsServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.secretPermissionsServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretPermissionsServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPermissionsServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPermissionsServiceSearchSecretPermissions - errors', () => {
      it('should have a secretPermissionsServiceSearchSecretPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.secretPermissionsServiceSearchSecretPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPermissionsServiceAddSecretPermission - errors', () => {
      it('should have a secretPermissionsServiceAddSecretPermission function', (done) => {
        try {
          assert.equal(true, typeof a.secretPermissionsServiceAddSecretPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPermissionsServiceStub - errors', () => {
      it('should have a secretPermissionsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.secretPermissionsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretPermissionsServiceStub(null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPermissionsServiceStub', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPermissionsServiceUpdateSecretShare - errors', () => {
      it('should have a secretPermissionsServiceUpdateSecretShare function', (done) => {
        try {
          assert.equal(true, typeof a.secretPermissionsServiceUpdateSecretShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretPermissionsServiceUpdateSecretShare(null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPermissionsServiceUpdateSecretShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPolicyServiceSearchSecretPolicies - errors', () => {
      it('should have a secretPolicyServiceSearchSecretPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.secretPolicyServiceSearchSecretPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPolicyServiceGetSecretPolicyStubV2 - errors', () => {
      it('should have a secretPolicyServiceGetSecretPolicyStubV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretPolicyServiceGetSecretPolicyStubV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPolicyServiceGetSecretPolicyV2 - errors', () => {
      it('should have a secretPolicyServiceGetSecretPolicyV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretPolicyServiceGetSecretPolicyV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretPolicyServiceGetSecretPolicyV2(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPolicyServiceGetSecretPolicyV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPolicyServiceUpdateSecretPolicyV2 - errors', () => {
      it('should have a secretPolicyServiceUpdateSecretPolicyV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretPolicyServiceUpdateSecretPolicyV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretPolicyServiceUpdateSecretPolicyV2(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPolicyServiceUpdateSecretPolicyV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPolicyServiceGetSecretPolicyAudit - errors', () => {
      it('should have a secretPolicyServiceGetSecretPolicyAudit function', (done) => {
        try {
          assert.equal(true, typeof a.secretPolicyServiceGetSecretPolicyAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretPolicyServiceGetSecretPolicyAudit(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPolicyServiceGetSecretPolicyAudit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPolicyServiceGetSecretPolicyStatus - errors', () => {
      it('should have a secretPolicyServiceGetSecretPolicyStatus function', (done) => {
        try {
          assert.equal(true, typeof a.secretPolicyServiceGetSecretPolicyStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretPolicyServiceGetSecretPolicyStatus(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretPolicyServiceGetSecretPolicyStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretPolicyServiceCreateSecretPolicyV2 - errors', () => {
      it('should have a secretPolicyServiceCreateSecretPolicyV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretPolicyServiceCreateSecretPolicyV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateSecret - errors', () => {
      it('should have a secretsServiceUpdateSecret function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateSecret('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceDelete - errors', () => {
      it('should have a secretsServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceDelete('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetListFieldListDefinitions - errors', () => {
      it('should have a secretsServiceGetListFieldListDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetListFieldListDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetListFieldListDefinitions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetListFieldListDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.secretsServiceGetListFieldListDefinitions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetListFieldListDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateListFieldListDefinitions - errors', () => {
      it('should have a secretsServiceUpdateListFieldListDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateListFieldListDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateListFieldListDefinitions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateListFieldListDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.secretsServiceUpdateListFieldListDefinitions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateListFieldListDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceDeleteListFieldListDefinitions - errors', () => {
      it('should have a secretsServiceDeleteListFieldListDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceDeleteListFieldListDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceDeleteListFieldListDefinitions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceDeleteListFieldListDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.secretsServiceDeleteListFieldListDefinitions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceDeleteListFieldListDefinitions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceSearchV2 - errors', () => {
      it('should have a secretsServiceSearchV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceSearchV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceSearchTotalV2 - errors', () => {
      it('should have a secretsServiceSearchTotalV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceSearchTotalV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceSearchSecretLookup - errors', () => {
      it('should have a secretsServiceSearchSecretLookup function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceSearchSecretLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetLookup - errors', () => {
      it('should have a secretsServiceGetLookup function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetLookup(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceStub - errors', () => {
      it('should have a secretsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretsServiceStub('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceStub', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretV2 - errors', () => {
      it('should have a secretsServiceGetSecretV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetSecretV2('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetSecretV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSummary - errors', () => {
      it('should have a secretsServiceGetSummary function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetSummary('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetField - errors', () => {
      it('should have a secretsServiceGetField function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.secretsServiceGetField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServicePutField - errors', () => {
      it('should have a secretsServicePutField function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServicePutField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServicePutField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServicePutField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.secretsServicePutField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServicePutField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetListField - errors', () => {
      it('should have a secretsServiceGetListField function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetListField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetListField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetListField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.secretsServiceGetListField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetListField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetFavorites - errors', () => {
      it('should have a secretsServiceGetFavorites function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetFavorites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretAudits - errors', () => {
      it('should have a secretsServiceGetSecretAudits function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetSecretAudits(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetSecretAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretState - errors', () => {
      it('should have a secretsServiceGetSecretState function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetSecretState('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetSecretState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetGeneral - errors', () => {
      it('should have a secretsServiceGetGeneral function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetGeneral === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetGeneral(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetGeneral', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretSettings - errors', () => {
      it('should have a secretsServiceGetSecretSettings function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetSecretSettings('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetSecretSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetActiveSecretSessions - errors', () => {
      it('should have a secretsServiceGetActiveSecretSessions function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetActiveSecretSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateSecretSession - errors', () => {
      it('should have a secretsServiceUpdateSecretSession function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateSecretSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSshRestrictedCommands - errors', () => {
      it('should have a secretsServiceGetSshRestrictedCommands function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSshRestrictedCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetSshRestrictedCommands('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetSshRestrictedCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateGeneral - errors', () => {
      it('should have a secretsServiceUpdateGeneral function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateGeneral === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateGeneral('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateGeneral', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateEmail - errors', () => {
      it('should have a secretsServiceUpdateEmail function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateEmail('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateSecurity - errors', () => {
      it('should have a secretsServiceUpdateSecurity function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateSecurity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateSecurity('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateSecurity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateGeneralV2 - errors', () => {
      it('should have a secretsServiceUpdateGeneralV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateGeneralV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateGeneralV2('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateGeneralV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateEmailV2 - errors', () => {
      it('should have a secretsServiceUpdateEmailV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateEmailV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateEmailV2('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateEmailV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateSecurityCheckoutV3 - errors', () => {
      it('should have a secretsServiceUpdateSecurityCheckoutV3 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateSecurityCheckoutV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateSecurityCheckoutV3('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateSecurityCheckoutV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateSecurityV2 - errors', () => {
      it('should have a secretsServiceUpdateSecurityV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateSecurityV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateSecurityV2('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateSecurityV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretExtendedSearchDetails - errors', () => {
      it('should have a secretsServiceGetSecretExtendedSearchDetails function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretExtendedSearchDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetRestricted - errors', () => {
      it('should have a secretsServiceGetRestricted function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetRestricted === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceGetRestricted('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceGetRestricted', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceCreateSecret - errors', () => {
      it('should have a secretsServiceCreateSecret function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceCreateSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceRunHeartBeat - errors', () => {
      it('should have a secretsServiceRunHeartBeat function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceRunHeartBeat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceRunHeartBeat('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceRunHeartBeat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceExpire - errors', () => {
      it('should have a secretsServiceExpire function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceExpire === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceExpire('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceExpire', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceChangePassword - errors', () => {
      it('should have a secretsServiceChangePassword function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceChangePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceChangePassword('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceChangePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceCheckIn - errors', () => {
      it('should have a secretsServiceCheckIn function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceCheckIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceCheckIn(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceCheckIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceCheckOut - errors', () => {
      it('should have a secretsServiceCheckOut function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceCheckOut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceCheckOut(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceCheckOut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceExtendCheckOut - errors', () => {
      it('should have a secretsServiceExtendCheckOut function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceExtendCheckOut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceExtendCheckOut(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceExtendCheckOut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceRestrictedField - errors', () => {
      it('should have a secretsServiceRestrictedField function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceRestrictedField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceRestrictedField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceRestrictedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.secretsServiceRestrictedField('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceRestrictedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretRdpProxyInfo - errors', () => {
      it('should have a secretsServiceGetSecretRdpProxyInfo function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretRdpProxyInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretSshProxyInfo - errors', () => {
      it('should have a secretsServiceGetSecretSshProxyInfo function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretSshProxyInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceGetSecretSshTerminalDetails - errors', () => {
      it('should have a secretsServiceGetSecretSshTerminalDetails function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceGetSecretSshTerminalDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceFavorite - errors', () => {
      it('should have a secretsServiceFavorite function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceFavorite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretsServiceFavorite(null, null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceFavorite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceStopPasswordChange - errors', () => {
      it('should have a secretsServiceStopPasswordChange function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceStopPasswordChange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceStopPasswordChange(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceStopPasswordChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceExportSecrets - errors', () => {
      it('should have a secretsServiceExportSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceExportSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateRpcScriptSecrets - errors', () => {
      it('should have a secretsServiceUpdateRpcScriptSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateRpcScriptSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateRpcScriptSecrets('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateRpcScriptSecrets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUndeleteSecret - errors', () => {
      it('should have a secretsServiceUndeleteSecret function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUndeleteSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUndeleteSecret(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUndeleteSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateRpcScriptSecretsV2 - errors', () => {
      it('should have a secretsServiceUpdateRpcScriptSecretsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateRpcScriptSecretsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateRpcScriptSecretsV2('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateRpcScriptSecretsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateSecurityApprovalV3 - errors', () => {
      it('should have a secretsServiceUpdateSecurityApprovalV3 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateSecurityApprovalV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateSecurityApprovalV3('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateSecurityApprovalV3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUndeleteSecretV2 - errors', () => {
      it('should have a secretsServiceUndeleteSecretV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUndeleteSecretV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUndeleteSecretV2(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUndeleteSecretV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateExpiration - errors', () => {
      it('should have a secretsServiceUpdateExpiration function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateExpiration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretsServiceUpdateExpiration('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateExpiration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateSshRestrictedCommands - errors', () => {
      it('should have a secretsServiceUpdateSshRestrictedCommands function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateSshRestrictedCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretsServiceUpdateSshRestrictedCommands('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateSshRestrictedCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretsServiceUpdateJumpboxRouteSelection - errors', () => {
      it('should have a secretsServiceUpdateJumpboxRouteSelection function', (done) => {
        try {
          assert.equal(true, typeof a.secretsServiceUpdateJumpboxRouteSelection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretId', (done) => {
        try {
          a.secretsServiceUpdateJumpboxRouteSelection(null, null, (data, error) => {
            try {
              const displayE = 'secretId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretsServiceUpdateJumpboxRouteSelection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretServerSettingsServiceGetExport - errors', () => {
      it('should have a secretServerSettingsServiceGetExport function', (done) => {
        try {
          assert.equal(true, typeof a.secretServerSettingsServiceGetExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretServerSettingsServiceGetExportStub - errors', () => {
      it('should have a secretServerSettingsServiceGetExportStub function', (done) => {
        try {
          assert.equal(true, typeof a.secretServerSettingsServiceGetExportStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretServerSettingsServiceGetExportImportCapabilities - errors', () => {
      it('should have a secretServerSettingsServiceGetExportImportCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.secretServerSettingsServiceGetExportImportCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretServerSettingsServiceImportSetting - errors', () => {
      it('should have a secretServerSettingsServiceImportSetting function', (done) => {
        try {
          assert.equal(true, typeof a.secretServerSettingsServiceImportSetting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretSessionsServiceSearchSessions - errors', () => {
      it('should have a secretSessionsServiceSearchSessions function', (done) => {
        try {
          assert.equal(true, typeof a.secretSessionsServiceSearchSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretSessionsServiceGet - errors', () => {
      it('should have a secretSessionsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.secretSessionsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretSessionsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretSessionsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretSessionsServiceGetSummary - errors', () => {
      it('should have a secretSessionsServiceGetSummary function', (done) => {
        try {
          assert.equal(true, typeof a.secretSessionsServiceGetSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretSessionsServiceGetSummary(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretSessionsServiceGetSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretSessionsServiceSearchPointsOfInterest - errors', () => {
      it('should have a secretSessionsServiceSearchPointsOfInterest function', (done) => {
        try {
          assert.equal(true, typeof a.secretSessionsServiceSearchPointsOfInterest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretSessionsServiceSearchPointsOfInterest(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretSessionsServiceSearchPointsOfInterest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretSessionsServiceSearchPointsOfInterestSummary - errors', () => {
      it('should have a secretSessionsServiceSearchPointsOfInterestSummary function', (done) => {
        try {
          assert.equal(true, typeof a.secretSessionsServiceSearchPointsOfInterestSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretSessionsServiceSearchPointsOfInterestSummary(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretSessionsServiceSearchPointsOfInterestSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretSessionsServiceGetSessionRecording - errors', () => {
      it('should have a secretSessionsServiceGetSessionRecording function', (done) => {
        try {
          assert.equal(true, typeof a.secretSessionsServiceGetSessionRecording === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretSessionsServiceGetSessionRecording(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretSessionsServiceGetSessionRecording', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretSessionsServiceProcessSession - errors', () => {
      it('should have a secretSessionsServiceProcessSession function', (done) => {
        try {
          assert.equal(true, typeof a.secretSessionsServiceProcessSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretSessionsServiceProcessSession(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretSessionsServiceProcessSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatePermissionsServiceSearchTemplatePermissions - errors', () => {
      it('should have a secretTemplatePermissionsServiceSearchTemplatePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatePermissionsServiceSearchTemplatePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatePermissionsServiceSearch - errors', () => {
      it('should have a secretTemplatePermissionsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatePermissionsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatePermissionsServiceUpdate - errors', () => {
      it('should have a secretTemplatePermissionsServiceUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatePermissionsServiceUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatePermissionsServiceGetTemplatePermissionRoles - errors', () => {
      it('should have a secretTemplatePermissionsServiceGetTemplatePermissionRoles function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatePermissionsServiceGetTemplatePermissionRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatePermissionsServiceUpdateTemplatePermissions - errors', () => {
      it('should have a secretTemplatePermissionsServiceUpdateTemplatePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatePermissionsServiceUpdateTemplatePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTypeId', (done) => {
        try {
          a.secretTemplatePermissionsServiceUpdateTemplatePermissions(null, null, (data, error) => {
            try {
              const displayE = 'secretTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatePermissionsServiceUpdateTemplatePermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceDisableField - errors', () => {
      it('should have a secretTemplatesServiceDisableField function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceDisableField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateFieldId', (done) => {
        try {
          a.secretTemplatesServiceDisableField(null, (data, error) => {
            try {
              const displayE = 'templateFieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceDisableField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetPasswordDictionary - errors', () => {
      it('should have a secretTemplatesServiceGetPasswordDictionary function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetPasswordDictionary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretTemplatesServiceGetPasswordDictionary(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetPasswordDictionary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceDeletePasswordDictionary - errors', () => {
      it('should have a secretTemplatesServiceDeletePasswordDictionary function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceDeletePasswordDictionary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretTemplatesServiceDeletePasswordDictionary(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceDeletePasswordDictionary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetSecretTemplateExtendedMapping - errors', () => {
      it('should have a secretTemplatesServiceGetSecretTemplateExtendedMapping function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetSecretTemplateExtendedMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedTypeId', (done) => {
        try {
          a.secretTemplatesServiceGetSecretTemplateExtendedMapping(null, null, (data, error) => {
            try {
              const displayE = 'extendedTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceGetSecretTemplateExtendedMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceCreateSecretTemplateExtendedMapping - errors', () => {
      it('should have a secretTemplatesServiceCreateSecretTemplateExtendedMapping function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceCreateSecretTemplateExtendedMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedTypeId', (done) => {
        try {
          a.secretTemplatesServiceCreateSecretTemplateExtendedMapping(null, null, null, (data, error) => {
            try {
              const displayE = 'extendedTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceCreateSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceCreateSecretTemplateExtendedMapping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceCreateSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceDeleteExtendedMapping - errors', () => {
      it('should have a secretTemplatesServiceDeleteExtendedMapping function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceDeleteExtendedMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedTypeId', (done) => {
        try {
          a.secretTemplatesServiceDeleteExtendedMapping(null, null, (data, error) => {
            try {
              const displayE = 'extendedTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceDeleteExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceDeleteExtendedMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceDeleteExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceUpdateSecretTemplateExtendedMapping - errors', () => {
      it('should have a secretTemplatesServiceUpdateSecretTemplateExtendedMapping function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceUpdateSecretTemplateExtendedMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedTypeId', (done) => {
        try {
          a.secretTemplatesServiceUpdateSecretTemplateExtendedMapping(null, null, null, (data, error) => {
            try {
              const displayE = 'extendedTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceUpdateSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceUpdateSecretTemplateExtendedMapping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceUpdateSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetSecretTemplateLauncher - errors', () => {
      it('should have a secretTemplatesServiceGetSecretTemplateLauncher function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetSecretTemplateLauncher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launcherTypeId', (done) => {
        try {
          a.secretTemplatesServiceGetSecretTemplateLauncher(null, null, (data, error) => {
            try {
              const displayE = 'launcherTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceGetSecretTemplateLauncher('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceCreateSecretTemplateLauncher - errors', () => {
      it('should have a secretTemplatesServiceCreateSecretTemplateLauncher function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceCreateSecretTemplateLauncher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launcherTypeId', (done) => {
        try {
          a.secretTemplatesServiceCreateSecretTemplateLauncher(null, null, null, (data, error) => {
            try {
              const displayE = 'launcherTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceCreateSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceCreateSecretTemplateLauncher('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceCreateSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceDeleteSecretTemplateLauncher - errors', () => {
      it('should have a secretTemplatesServiceDeleteSecretTemplateLauncher function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceDeleteSecretTemplateLauncher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launcherTypeId', (done) => {
        try {
          a.secretTemplatesServiceDeleteSecretTemplateLauncher(null, null, (data, error) => {
            try {
              const displayE = 'launcherTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceDeleteSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceDeleteSecretTemplateLauncher('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceDeleteSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceUpdateSecretTemplateLauncher - errors', () => {
      it('should have a secretTemplatesServiceUpdateSecretTemplateLauncher function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceUpdateSecretTemplateLauncher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launcherTypeId', (done) => {
        try {
          a.secretTemplatesServiceUpdateSecretTemplateLauncher(null, null, null, (data, error) => {
            try {
              const displayE = 'launcherTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceUpdateSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceUpdateSecretTemplateLauncher('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceUpdateSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceSearch - errors', () => {
      it('should have a secretTemplatesServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceCreateTemplate - errors', () => {
      it('should have a secretTemplatesServiceCreateTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceCreateTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceExport - errors', () => {
      it('should have a secretTemplatesServiceExport function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretTemplatesServiceExport(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetPasswordDictionaries - errors', () => {
      it('should have a secretTemplatesServiceGetPasswordDictionaries function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetPasswordDictionaries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceUpdatePasswordDictionary - errors', () => {
      it('should have a secretTemplatesServiceUpdatePasswordDictionary function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceUpdatePasswordDictionary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceAddPasswordDictionary - errors', () => {
      it('should have a secretTemplatesServiceAddPasswordDictionary function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceAddPasswordDictionary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceSearchSecretTemplateExtendedTypes - errors', () => {
      it('should have a secretTemplatesServiceSearchSecretTemplateExtendedTypes function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceSearchSecretTemplateExtendedTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceSearchSecretTemplateExtendedMappings - errors', () => {
      it('should have a secretTemplatesServiceSearchSecretTemplateExtendedMappings function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceSearchSecretTemplateExtendedMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceStubSecretTemplateExtendedMapping - errors', () => {
      it('should have a secretTemplatesServiceStubSecretTemplateExtendedMapping function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceStubSecretTemplateExtendedMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedTypeId', (done) => {
        try {
          a.secretTemplatesServiceStubSecretTemplateExtendedMapping(null, null, (data, error) => {
            try {
              const displayE = 'extendedTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceStubSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceStubSecretTemplateExtendedMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceStubSecretTemplateExtendedMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceSearchLauncherTypes - errors', () => {
      it('should have a secretTemplatesServiceSearchLauncherTypes function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceSearchLauncherTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceSearchSecretTemplateLaunchers - errors', () => {
      it('should have a secretTemplatesServiceSearchSecretTemplateLaunchers function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceSearchSecretTemplateLaunchers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceStubSecretTemplateLauncher - errors', () => {
      it('should have a secretTemplatesServiceStubSecretTemplateLauncher function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceStubSecretTemplateLauncher === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launcherTypeId', (done) => {
        try {
          a.secretTemplatesServiceStubSecretTemplateLauncher(null, null, (data, error) => {
            try {
              const displayE = 'launcherTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceStubSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceStubSecretTemplateLauncher('fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceStubSecretTemplateLauncher', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetSecretTemplatePasswordType - errors', () => {
      it('should have a secretTemplatesServiceGetSecretTemplatePasswordType function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetSecretTemplatePasswordType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceGetSecretTemplatePasswordType(null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetSecretTemplatePasswordType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServicePatchSecretTemplatePasswordChanger - errors', () => {
      it('should have a secretTemplatesServicePatchSecretTemplatePasswordChanger function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServicePatchSecretTemplatePasswordChanger === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServicePatchSecretTemplatePasswordChanger(null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServicePatchSecretTemplatePasswordChanger', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetTemplates - errors', () => {
      it('should have a secretTemplatesServiceGetTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetV2 - errors', () => {
      it('should have a secretTemplatesServiceGetV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceGetV2(null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServicePatchTemplateV2 - errors', () => {
      it('should have a secretTemplatesServicePatchTemplateV2 function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServicePatchTemplateV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServicePatchTemplateV2(null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServicePatchTemplateV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceSearchTemplateFields - errors', () => {
      it('should have a secretTemplatesServiceSearchTemplateFields function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceSearchTemplateFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceStubTemplateField - errors', () => {
      it('should have a secretTemplatesServiceStubTemplateField function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceStubTemplateField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGetTemplateField - errors', () => {
      it('should have a secretTemplatesServiceGetTemplateField function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGetTemplateField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretFieldId', (done) => {
        try {
          a.secretTemplatesServiceGetTemplateField(null, (data, error) => {
            try {
              const displayE = 'secretFieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGetTemplateField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServicePatchTemplateField - errors', () => {
      it('should have a secretTemplatesServicePatchTemplateField function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServicePatchTemplateField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateFieldId', (done) => {
        try {
          a.secretTemplatesServicePatchTemplateField(null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateFieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServicePatchTemplateField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceCopy - errors', () => {
      it('should have a secretTemplatesServiceCopy function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceCopy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.secretTemplatesServiceCopy(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServicePut - errors', () => {
      it('should have a secretTemplatesServicePut function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServicePut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.secretTemplatesServicePut(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServicePut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceCreateField - errors', () => {
      it('should have a secretTemplatesServiceCreateField function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceCreateField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.secretTemplatesServiceCreateField(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceCreateField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceGeneratePassword - errors', () => {
      it('should have a secretTemplatesServiceGeneratePassword function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceGeneratePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretfieldId', (done) => {
        try {
          a.secretTemplatesServiceGeneratePassword(null, (data, error) => {
            try {
              const displayE = 'secretfieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceGeneratePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceImportSecretTemplate - errors', () => {
      it('should have a secretTemplatesServiceImportSecretTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceImportSecretTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceCreateTemplateField - errors', () => {
      it('should have a secretTemplatesServiceCreateTemplateField function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceCreateTemplateField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceCreateTemplateField(null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceCreateTemplateField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceSortTemplateFields - errors', () => {
      it('should have a secretTemplatesServiceSortTemplateFields function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceSortTemplateFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretTemplateId', (done) => {
        try {
          a.secretTemplatesServiceSortTemplateFields(null, null, (data, error) => {
            try {
              const displayE = 'secretTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceSortTemplateFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#secretTemplatesServiceUpdateSecretTemplatePasswordType - errors', () => {
      it('should have a secretTemplatesServiceUpdateSecretTemplatePasswordType function', (done) => {
        try {
          assert.equal(true, typeof a.secretTemplatesServiceUpdateSecretTemplatePasswordType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.secretTemplatesServiceUpdateSecretTemplatePasswordType(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-secretTemplatesServiceUpdateSecretTemplatePasswordType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityAuditLogsServiceSearchLogs - errors', () => {
      it('should have a securityAuditLogsServiceSearchLogs function', (done) => {
        try {
          assert.equal(true, typeof a.securityAuditLogsServiceSearchLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serverNodesServiceGetList - errors', () => {
      it('should have a serverNodesServiceGetList function', (done) => {
        try {
          assert.equal(true, typeof a.serverNodesServiceGetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serverNodesServiceGet - errors', () => {
      it('should have a serverNodesServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.serverNodesServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.serverNodesServiceGet(null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-serverNodesServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serverNodesServiceUpdateNodeConfiguration - errors', () => {
      it('should have a serverNodesServiceUpdateNodeConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.serverNodesServiceUpdateNodeConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.serverNodesServiceUpdateNodeConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-serverNodesServiceUpdateNodeConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sitesServiceGet - errors', () => {
      it('should have a sitesServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.sitesServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#slackServiceGetConfigurationV3 - errors', () => {
      it('should have a slackServiceGetConfigurationV3 function', (done) => {
        try {
          assert.equal(true, typeof a.slackServiceGetConfigurationV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#slackServiceUpdateConfigurationV3 - errors', () => {
      it('should have a slackServiceUpdateConfigurationV3 function', (done) => {
        try {
          assert.equal(true, typeof a.slackServiceUpdateConfigurationV3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#slackServiceSendTestSlackMessage - errors', () => {
      it('should have a slackServiceSendTestSlackMessage function', (done) => {
        try {
          assert.equal(true, typeof a.slackServiceSendTestSlackMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#slackServiceHandleEvent - errors', () => {
      it('should have a slackServiceHandleEvent function', (done) => {
        try {
          assert.equal(true, typeof a.slackServiceHandleEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#slackServiceHandleInteraction - errors', () => {
      it('should have a slackServiceHandleInteraction function', (done) => {
        try {
          assert.equal(true, typeof a.slackServiceHandleInteraction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandServiceGetSshCommand - errors', () => {
      it('should have a sshCommandServiceGetSshCommand function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandServiceGetSshCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sshCommandServiceGetSshCommand(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sshCommandServiceGetSshCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandServiceGetSshCommands - errors', () => {
      it('should have a sshCommandServiceGetSshCommands function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandServiceGetSshCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandServiceGetSshCommandStub - errors', () => {
      it('should have a sshCommandServiceGetSshCommandStub function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandServiceGetSshCommandStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandServiceUpdateSshCommand - errors', () => {
      it('should have a sshCommandServiceUpdateSshCommand function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandServiceUpdateSshCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sshCommandId', (done) => {
        try {
          a.sshCommandServiceUpdateSshCommand(null, null, (data, error) => {
            try {
              const displayE = 'sshCommandId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sshCommandServiceUpdateSshCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandServiceCreateSshCommand - errors', () => {
      it('should have a sshCommandServiceCreateSshCommand function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandServiceCreateSshCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandBlocklistServiceGetSshCommandBlocklist - errors', () => {
      it('should have a sshCommandBlocklistServiceGetSshCommandBlocklist function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandBlocklistServiceGetSshCommandBlocklist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sshCommandBlocklistServiceGetSshCommandBlocklist(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sshCommandBlocklistServiceGetSshCommandBlocklist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandBlocklistServiceGetSshCommandBlocklists - errors', () => {
      it('should have a sshCommandBlocklistServiceGetSshCommandBlocklists function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandBlocklistServiceGetSshCommandBlocklists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandBlocklistServiceGetSshCommandBlocklistStub - errors', () => {
      it('should have a sshCommandBlocklistServiceGetSshCommandBlocklistStub function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandBlocklistServiceGetSshCommandBlocklistStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandBlocklistServiceGetSshCommandBlocklistPolicies - errors', () => {
      it('should have a sshCommandBlocklistServiceGetSshCommandBlocklistPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandBlocklistServiceGetSshCommandBlocklistPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandBlocklistServicePatchSshCommandBlocklist - errors', () => {
      it('should have a sshCommandBlocklistServicePatchSshCommandBlocklist function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandBlocklistServicePatchSshCommandBlocklist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sshCommandBlocklistId', (done) => {
        try {
          a.sshCommandBlocklistServicePatchSshCommandBlocklist(null, null, (data, error) => {
            try {
              const displayE = 'sshCommandBlocklistId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sshCommandBlocklistServicePatchSshCommandBlocklist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandBlocklistServiceCreateSshCommandBlocklist - errors', () => {
      it('should have a sshCommandBlocklistServiceCreateSshCommandBlocklist function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandBlocklistServiceCreateSshCommandBlocklist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandMenuServiceSearchSshCommandMenu - errors', () => {
      it('should have a sshCommandMenuServiceSearchSshCommandMenu function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandMenuServiceSearchSshCommandMenu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandMenuServiceSearchAudits - errors', () => {
      it('should have a sshCommandMenuServiceSearchAudits function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandMenuServiceSearchAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandMenuServiceSearchItemAudits - errors', () => {
      it('should have a sshCommandMenuServiceSearchItemAudits function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandMenuServiceSearchItemAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandMenuServiceGetSshCommandMenu - errors', () => {
      it('should have a sshCommandMenuServiceGetSshCommandMenu function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandMenuServiceGetSshCommandMenu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sshCommandMenuId', (done) => {
        try {
          a.sshCommandMenuServiceGetSshCommandMenu(null, (data, error) => {
            try {
              const displayE = 'sshCommandMenuId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sshCommandMenuServiceGetSshCommandMenu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandMenuServicePatchSshCommandMenu - errors', () => {
      it('should have a sshCommandMenuServicePatchSshCommandMenu function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandMenuServicePatchSshCommandMenu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sshCommandMenuId', (done) => {
        try {
          a.sshCommandMenuServicePatchSshCommandMenu(null, null, (data, error) => {
            try {
              const displayE = 'sshCommandMenuId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-sshCommandMenuServicePatchSshCommandMenu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandMenuServiceGetSshCommandState - errors', () => {
      it('should have a sshCommandMenuServiceGetSshCommandState function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandMenuServiceGetSshCommandState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sshCommandMenuServiceAddSshCommandMenu - errors', () => {
      it('should have a sshCommandMenuServiceAddSshCommandMenu function', (done) => {
        try {
          assert.equal(true, typeof a.sshCommandMenuServiceAddSshCommandMenu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceSearch - errors', () => {
      it('should have a teamsServiceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceCreateTeam - errors', () => {
      it('should have a teamsServiceCreateTeam function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceCreateTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceStub - errors', () => {
      it('should have a teamsServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceGet - errors', () => {
      it('should have a teamsServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceUpdateTeam - errors', () => {
      it('should have a teamsServiceUpdateTeam function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceUpdateTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceUpdateTeam(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceUpdateTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceGetTeamAudits - errors', () => {
      it('should have a teamsServiceGetTeamAudits function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceGetTeamAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceGetTeamAudits(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceGetTeamAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceGetTeamMembers - errors', () => {
      it('should have a teamsServiceGetTeamMembers function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceGetTeamMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceGetTeamMembers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceGetTeamMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceAddTeamMember - errors', () => {
      it('should have a teamsServiceAddTeamMember function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceAddTeamMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceAddTeamMember(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceAddTeamMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceGetTeamSites - errors', () => {
      it('should have a teamsServiceGetTeamSites function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceGetTeamSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceGetTeamSites(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceGetTeamSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing includeInactive', (done) => {
        try {
          a.teamsServiceGetTeamSites('fakeparam', null, (data, error) => {
            try {
              const displayE = 'includeInactive is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceGetTeamSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceAddTeamSite - errors', () => {
      it('should have a teamsServiceAddTeamSite function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceAddTeamSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceAddTeamSite(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceAddTeamSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceGetTeamLists - errors', () => {
      it('should have a teamsServiceGetTeamLists function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceGetTeamLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceGetTeamLists(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceGetTeamLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsServiceAddListsToTeam - errors', () => {
      it('should have a teamsServiceAddListsToTeam function', (done) => {
        try {
          assert.equal(true, typeof a.teamsServiceAddListsToTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.teamsServiceAddListsToTeam(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-teamsServiceAddListsToTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketSystemsServiceGetTicketSystems - errors', () => {
      it('should have a ticketSystemsServiceGetTicketSystems function', (done) => {
        try {
          assert.equal(true, typeof a.ticketSystemsServiceGetTicketSystems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketSystemsServiceCreateTicketSystem - errors', () => {
      it('should have a ticketSystemsServiceCreateTicketSystem function', (done) => {
        try {
          assert.equal(true, typeof a.ticketSystemsServiceCreateTicketSystem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketSystemsServiceGetTicketSystem - errors', () => {
      it('should have a ticketSystemsServiceGetTicketSystem function', (done) => {
        try {
          assert.equal(true, typeof a.ticketSystemsServiceGetTicketSystem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ticketSystemsServiceGetTicketSystem(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ticketSystemsServiceGetTicketSystem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketSystemsServiceUpdateTicketSystem - errors', () => {
      it('should have a ticketSystemsServiceUpdateTicketSystem function', (done) => {
        try {
          assert.equal(true, typeof a.ticketSystemsServiceUpdateTicketSystem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ticketSystemsServiceUpdateTicketSystem(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ticketSystemsServiceUpdateTicketSystem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketSystemsServiceGetTicketSystemsV2 - errors', () => {
      it('should have a ticketSystemsServiceGetTicketSystemsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.ticketSystemsServiceGetTicketSystemsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketSystemsServiceGetTicketSystemV2 - errors', () => {
      it('should have a ticketSystemsServiceGetTicketSystemV2 function', (done) => {
        try {
          assert.equal(true, typeof a.ticketSystemsServiceGetTicketSystemV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.ticketSystemsServiceGetTicketSystemV2(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-ticketSystemsServiceGetTicketSystemV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGet - errors', () => {
      it('should have a usersServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceUpdateUser - errors', () => {
      it('should have a usersServiceUpdateUser function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceUpdateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceUpdateUser(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceUpdateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceDelete - errors', () => {
      it('should have a usersServiceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServicePatchUser - errors', () => {
      it('should have a usersServicePatchUser function', (done) => {
        try {
          assert.equal(true, typeof a.usersServicePatchUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServicePatchUser(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServicePatchUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetUserOwner - errors', () => {
      it('should have a usersServiceGetUserOwner function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetUserOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceGetUserOwner(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGetUserOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ownerId', (done) => {
        try {
          a.usersServiceGetUserOwner('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ownerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGetUserOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceDeleteUserOwner - errors', () => {
      it('should have a usersServiceDeleteUserOwner function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceDeleteUserOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceDeleteUserOwner(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceDeleteUserOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ownerId', (done) => {
        try {
          a.usersServiceDeleteUserOwner('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ownerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceDeleteUserOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetRoles - errors', () => {
      it('should have a usersServiceGetRoles function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceGetRoles('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGetRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceUpdateUserRoles - errors', () => {
      it('should have a usersServiceUpdateUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceUpdateUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceUpdateUserRoles(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceUpdateUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceCreateUserRoles - errors', () => {
      it('should have a usersServiceCreateUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceCreateUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceCreateUserRoles(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceCreateUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceDeleteUserRoles - errors', () => {
      it('should have a usersServiceDeleteUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceDeleteUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceDeleteUserRoles(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceDeleteUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetUserGroups - errors', () => {
      it('should have a usersServiceGetUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceGetUserGroups(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGetUserGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceUpdateUserGroups - errors', () => {
      it('should have a usersServiceUpdateUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceUpdateUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceUpdateUserGroups(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceUpdateUserGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceAddUserToGroups - errors', () => {
      it('should have a usersServiceAddUserToGroups function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceAddUserToGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceAddUserToGroups(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceAddUserToGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceRemoveUserGroups - errors', () => {
      it('should have a usersServiceRemoveUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceRemoveUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceRemoveUserGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceRemoveUserGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceSearchUsers - errors', () => {
      it('should have a usersServiceSearchUsers function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceSearchUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceCreateUser - errors', () => {
      it('should have a usersServiceCreateUser function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceCreateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetCurrentUser - errors', () => {
      it('should have a usersServiceGetCurrentUser function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetCurrentUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceLookup - errors', () => {
      it('should have a usersServiceLookup function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceStub - errors', () => {
      it('should have a usersServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetUserPublicSshKeys - errors', () => {
      it('should have a usersServiceGetUserPublicSshKeys function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetUserPublicSshKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceCreateUserPublicSshKey - errors', () => {
      it('should have a usersServiceCreateUserPublicSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceCreateUserPublicSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceSearchUserOwners - errors', () => {
      it('should have a usersServiceSearchUserOwners function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceSearchUserOwners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceSearchUserOwners(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceSearchUserOwners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceAddUserOwner - errors', () => {
      it('should have a usersServiceAddUserOwner function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceAddUserOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceAddUserOwner(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceAddUserOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServicePatchUserOwners - errors', () => {
      it('should have a usersServicePatchUserOwners function', (done) => {
        try {
          assert.equal(true, typeof a.usersServicePatchUserOwners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServicePatchUserOwners(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServicePatchUserOwners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetDomains - errors', () => {
      it('should have a usersServiceGetDomains function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetUserRoles - errors', () => {
      it('should have a usersServiceGetUserRoles function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetUserRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.usersServiceGetUserRoles('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGetUserRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetSiteAudits - errors', () => {
      it('should have a usersServiceGetSiteAudits function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetSiteAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.usersServiceGetSiteAudits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGetSiteAudits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetUserActionAudits - errors', () => {
      it('should have a usersServiceGetUserActionAudits function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetUserActionAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetUserTeams - errors', () => {
      it('should have a usersServiceGetUserTeams function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetUserTeams === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.usersServiceGetUserTeams('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceGetUserTeams', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetPreference - errors', () => {
      it('should have a usersServiceGetPreference function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetPreference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceUpdatePreference - errors', () => {
      it('should have a usersServiceUpdatePreference function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceUpdatePreference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceGetCurrentUserSessions - errors', () => {
      it('should have a usersServiceGetCurrentUserSessions function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceGetCurrentUserSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceDeactivateUserPublicSshKey - errors', () => {
      it('should have a usersServiceDeactivateUserPublicSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceDeactivateUserPublicSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceDeactivateUserPublicSshKey(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceDeactivateUserPublicSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceChangePassword - errors', () => {
      it('should have a usersServiceChangePassword function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceChangePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceVerifyPassword - errors', () => {
      it('should have a usersServiceVerifyPassword function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceVerifyPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceResetTwoFactor - errors', () => {
      it('should have a usersServiceResetTwoFactor function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceResetTwoFactor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.usersServiceResetTwoFactor(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceResetTwoFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceResetUserPassword - errors', () => {
      it('should have a usersServiceResetUserPassword function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceResetUserPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.usersServiceResetUserPassword(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceResetUserPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceUserPersonalInfoDeleteCommand - errors', () => {
      it('should have a usersServiceUserPersonalInfoDeleteCommand function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceUserPersonalInfoDeleteCommand === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.usersServiceUserPersonalInfoDeleteCommand(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceUserPersonalInfoDeleteCommand', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceLockOut - errors', () => {
      it('should have a usersServiceLockOut function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceLockOut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.usersServiceLockOut(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-usersServiceLockOut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceTerminateCurrentUserSessions - errors', () => {
      it('should have a usersServiceTerminateCurrentUserSessions function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceTerminateCurrentUserSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersServiceSetUserDoubleLockPassword - errors', () => {
      it('should have a usersServiceSetUserDoubleLockPassword function', (done) => {
        try {
          assert.equal(true, typeof a.usersServiceSetUserDoubleLockPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#versionServiceGet - errors', () => {
      it('should have a versionServiceGet function', (done) => {
        try {
          assert.equal(true, typeof a.versionServiceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowInstancesServiceGetByTemplateId - errors', () => {
      it('should have a workflowInstancesServiceGetByTemplateId function', (done) => {
        try {
          assert.equal(true, typeof a.workflowInstancesServiceGetByTemplateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowInstancesServiceGetByTemplateId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowInstancesServiceGetByTemplateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowStepTemplatesServiceGetTemplateSteps - errors', () => {
      it('should have a workflowStepTemplatesServiceGetTemplateSteps function', (done) => {
        try {
          assert.equal(true, typeof a.workflowStepTemplatesServiceGetTemplateSteps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowStepTemplatesServiceGetTemplateSteps(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowStepTemplatesServiceGetTemplateSteps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowStepTemplatesServiceUpdateStep - errors', () => {
      it('should have a workflowStepTemplatesServiceUpdateStep function', (done) => {
        try {
          assert.equal(true, typeof a.workflowStepTemplatesServiceUpdateStep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowStepTemplatesServiceUpdateStep(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowStepTemplatesServiceUpdateStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowStepTemplatesServiceCreateStep - errors', () => {
      it('should have a workflowStepTemplatesServiceCreateStep function', (done) => {
        try {
          assert.equal(true, typeof a.workflowStepTemplatesServiceCreateStep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowStepTemplatesServiceCreateStep(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowStepTemplatesServiceCreateStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowStepTemplatesServiceStub - errors', () => {
      it('should have a workflowStepTemplatesServiceStub function', (done) => {
        try {
          assert.equal(true, typeof a.workflowStepTemplatesServiceStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowStepTemplatesServiceGetTemplateStep - errors', () => {
      it('should have a workflowStepTemplatesServiceGetTemplateStep function', (done) => {
        try {
          assert.equal(true, typeof a.workflowStepTemplatesServiceGetTemplateStep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowStepTemplatesServiceGetTemplateStep(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowStepTemplatesServiceGetTemplateStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepNum', (done) => {
        try {
          a.workflowStepTemplatesServiceGetTemplateStep('fakeparam', null, (data, error) => {
            try {
              const displayE = 'stepNum is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowStepTemplatesServiceGetTemplateStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowStepTemplatesServiceUpdateStepModel - errors', () => {
      it('should have a workflowStepTemplatesServiceUpdateStepModel function', (done) => {
        try {
          assert.equal(true, typeof a.workflowStepTemplatesServiceUpdateStepModel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowStepTemplatesServiceUpdateStepModel(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowStepTemplatesServiceUpdateStepModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepNum', (done) => {
        try {
          a.workflowStepTemplatesServiceUpdateStepModel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepNum is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowStepTemplatesServiceUpdateStepModel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceSearchWorkflowTemplates - errors', () => {
      it('should have a workflowTemplatesServiceSearchWorkflowTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceSearchWorkflowTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceCreateWorkflowTemplate - errors', () => {
      it('should have a workflowTemplatesServiceCreateWorkflowTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceCreateWorkflowTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceStubWorkflowTemplate - errors', () => {
      it('should have a workflowTemplatesServiceStubWorkflowTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceStubWorkflowTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceGetTemplate - errors', () => {
      it('should have a workflowTemplatesServiceGetTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceGetTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowTemplatesServiceGetTemplate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowTemplatesServiceGetTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceUpdateWorkflowTemplate - errors', () => {
      it('should have a workflowTemplatesServiceUpdateWorkflowTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceUpdateWorkflowTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowTemplatesServiceUpdateWorkflowTemplate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowTemplatesServiceUpdateWorkflowTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceSearchTemplateAudit - errors', () => {
      it('should have a workflowTemplatesServiceSearchTemplateAudit function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceSearchTemplateAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowTemplatesServiceSearchTemplateAudit(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowTemplatesServiceSearchTemplateAudit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceGetWorkflowEntities - errors', () => {
      it('should have a workflowTemplatesServiceGetWorkflowEntities function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceGetWorkflowEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowTemplatesServiceGetWorkflowEntities(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowTemplatesServiceGetWorkflowEntities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing includeAll', (done) => {
        try {
          a.workflowTemplatesServiceGetWorkflowEntities('fakeparam', null, (data, error) => {
            try {
              const displayE = 'includeAll is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowTemplatesServiceGetWorkflowEntities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowTemplatesServiceCancelRequest - errors', () => {
      it('should have a workflowTemplatesServiceCancelRequest function', (done) => {
        try {
          assert.equal(true, typeof a.workflowTemplatesServiceCancelRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.workflowTemplatesServiceCancelRequest(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-delinea-adapter-workflowTemplatesServiceCancelRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
