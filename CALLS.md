## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Delinea Secret Server. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Delinea Secret Server.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>

  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Delinea. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">activationsServiceActivate(body, callback)</td>
    <td style="padding:15px">ActivationsService_Activate</td>
    <td style="padding:15px">{base_path}/{version}/v1/activations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activeDirectoryServiceSynchronize(callback)</td>
    <td style="padding:15px">ActiveDirectoryService_Synchronize</td>
    <td style="padding:15px">{base_path}/{version}/v1/active-directory/synchronize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">apiTokenServiceGetToken(callback)</td>
    <td style="padding:15px">ApiTokenService_GetToken</td>
    <td style="padding:15px">{base_path}/{version}/v1/-token/generate-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appClientsServiceGetClient(includeInactive, id, callback)</td>
    <td style="padding:15px">AppClientsService_GetClient</td>
    <td style="padding:15px">{base_path}/{version}/v1/app-clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appClientsServiceUpdateClient(id, body, callback)</td>
    <td style="padding:15px">AppClientsService_UpdateClient</td>
    <td style="padding:15px">{base_path}/{version}/v1/app-clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appClientsServiceDeleteClient(id, callback)</td>
    <td style="padding:15px">AppClientsService_DeleteClient</td>
    <td style="padding:15px">{base_path}/{version}/v1/app-clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appClientsServiceStub(callback)</td>
    <td style="padding:15px">AppClientsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/app-clients/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appClientsServiceCreateClient(body, callback)</td>
    <td style="padding:15px">AppClientsService_CreateClient</td>
    <td style="padding:15px">{base_path}/{version}/v1/app-clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationAccountsServiceLookupAccount(filterIncludeAll, filterIncludeInactive, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ApplicationAccountsService_LookupAccount</td>
    <td style="padding:15px">{base_path}/{version}/v1/application-accounts/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationRequestServiceSearchRequestsByStatus(filterStatus, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ApplicationRequestService_SearchRequestsByStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/application-access-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationRequestServiceGetRequestByDeviceId(deviceId, callback)</td>
    <td style="padding:15px">ApplicationRequestService_GetRequestByDeviceId</td>
    <td style="padding:15px">{base_path}/{version}/v1/application-access-request/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationRequestServiceUpdateRequest(deviceId, body, callback)</td>
    <td style="padding:15px">ApplicationRequestService_UpdateRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/application-access-request/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationRequestServiceGetAudits(isExporting, filterDeviceId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ApplicationRequestService_GetAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/application-access-request-audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceGetList(categorizedListId, callback)</td>
    <td style="padding:15px">CategorizedListsService_GetList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceUpdateList(categorizedListId, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_UpdateList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceDeleteList(categorizedListId, callback)</td>
    <td style="padding:15px">CategorizedListsService_DeleteList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceGetListItems(categorizedListId, filterCategory, filterNullCategoryIsUncategorized, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">CategorizedListsService_GetListItems</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceUpdateItemsInList(categorizedListId, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_UpdateItemsInList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceAddItemsToList(categorizedListId, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_AddItemsToList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceRemoveItemsFromList(categorizedListId, callback)</td>
    <td style="padding:15px">CategorizedListsService_RemoveItemsFromList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceRemoveItemFromList(listId, optionId, callback)</td>
    <td style="padding:15px">CategorizedListsService_RemoveItemFromList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceSearch(filterIncludeActive, filterIncludeInactive, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">CategorizedListsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceCreateList(body, callback)</td>
    <td style="padding:15px">CategorizedListsService_CreateList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceGetAllListsUserMaySee(callback)</td>
    <td style="padding:15px">CategorizedListsService_GetAllListsUserMaySee</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceGetCategoriesForList(categorizedListId, callback)</td>
    <td style="padding:15px">CategorizedListsService_GetCategoriesForList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceSearchListAudit(categorizedListId, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">CategorizedListsService_SearchListAudit</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceUpdateItemInList(categorizedListId, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_UpdateItemInList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options/single?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceAddItemToList(categorizedListId, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_AddItemToList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options/single?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceAddItemsToListWithCategory(categorizedListId, category, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_AddItemsToListWithCategory</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceAddItemsToListFromFile(categorizedListId, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_AddItemsToListFromFile</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">categorizedListsServiceReplaceItemsInList(categorizedListId, body, callback)</td>
    <td style="padding:15px">CategorizedListsService_ReplaceItemsInList</td>
    <td style="padding:15px">{base_path}/{version}/v1/lists/{pathv1}/options/replace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">characterSetsServiceSearchCharacterSets(filterStatus, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">CharacterSetsService_SearchCharacterSets</td>
    <td style="padding:15px">{base_path}/{version}/v1/character-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">characterSetsServiceCreate(body, callback)</td>
    <td style="padding:15px">CharacterSetsService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/character-sets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">characterSetsServiceGet(id, callback)</td>
    <td style="padding:15px">CharacterSetsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/character-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">characterSetsServicePatch(id, body, callback)</td>
    <td style="padding:15px">CharacterSetsService_Patch</td>
    <td style="padding:15px">{base_path}/{version}/v1/character-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">characterSetsServiceGetSiteAudits(id, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">CharacterSetsService_GetSiteAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/character-sets/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetConfigurationState(callback)</td>
    <td style="padding:15px">ConfigurationService_GetConfigurationState</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetConfigurationAudit(isExporting, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ConfigurationService_GetConfigurationAudit</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetGeneralConfigurationV2(loadAll, loadApplicationSettings, loadEmail, loadFolders, loadLauncherSettings, loadLocalUserPasswords, loadLogin, loadPermissionOptions, loadProtocolHandlerSettings, loadSecurity, loadSessionRecording, loadUnlimitedAdmin, loadUserExperience, loadUserInterface, callback)</td>
    <td style="padding:15px">ConfigurationService_GetGeneralConfigurationV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/configuration/general?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetLocalPasswordConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetLocalPasswordConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/local-user-passwords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchLocalPasswordConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchLocalPasswordConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/local-user-passwords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetSecurityConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetSecurityConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/security?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchSecurityConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchSecurityConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/security?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetLoginConfigurationV2(callback)</td>
    <td style="padding:15px">ConfigurationService_GetLoginConfigurationV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/configuration/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchLoginConfigurationV2(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchLoginConfigurationV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/configuration/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetLoginPolicyConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetLoginPolicyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/login-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchLoginPolicyConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchLoginPolicyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/login-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetSamlConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetSamlConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/saml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchSamlConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchSamlConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/saml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetSamlIdentityProviderConfiguration(id, callback)</td>
    <td style="padding:15px">ConfigurationService_GetSamlIdentityProviderConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/saml/identity-provider/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetRpcConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetRpcConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/rpc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchRpcConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchRpcConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/rpc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetSessionRecordingAdvancedConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetSessionRecordingAdvancedConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/sessionrecording-advanced?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchSessionRecordingAdvancedConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchSessionRecordingAdvancedConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/sessionrecording-advanced?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetSiteConnectors(includeInactive, callback)</td>
    <td style="padding:15px">ConfigurationService_GetSiteConnectors</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/site-connector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetInternalSiteConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetInternalSiteConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/internal-site-connector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchInternalSiteConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchInternalSiteConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/internal-site-connector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetUnlimitedAdmin(callback)</td>
    <td style="padding:15px">ConfigurationService_GetUnlimitedAdmin</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/unlimited-admin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceUpdateUnlimitedAdmin(body, callback)</td>
    <td style="padding:15px">ConfigurationService_UpdateUnlimitedAdmin</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/unlimited-admin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetSecretSearchIndexerConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetSecretSearchIndexerConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/secret-search-indexer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchSecretSearchIndexerConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchSecretSearchIndexerConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/secret-search-indexer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetPlatformConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetPlatformConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/platform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchPlatformConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchPlatformConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/platform?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetPlatformConfigurationAudits(isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ConfigurationService_GetPlatformConfigurationAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/platform-audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetAutoExportConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetAutoExportConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchAutomaticExportConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchAutomaticExportConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetAutoExportConfigurationAudits(isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ConfigurationService_GetAutoExportConfigurationAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export-audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetAutoExportLogs(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ConfigurationService_GetAutoExportLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetAutoExportStorageItemsDefault(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ConfigurationService_GetAutoExportStorageItemsDefault</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export-storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetAutoExportStorageItems(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ConfigurationService_GetAutoExportStorageItems</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export-storage/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetAutoExportStorageItem(id, callback)</td>
    <td style="padding:15px">ConfigurationService_GetAutoExportStorageItem</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export-storage/item/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetDatabaseBackupConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetDatabaseBackupConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchDatabaseBackupConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchDatabaseBackupConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetBackupLogsV2(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ConfigurationService_GetBackupLogsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/configuration/backup-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetSystemLogConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetSystemLogConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/system-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchSystemLogConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchSystemLogConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/system-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceClearSystemLog(callback)</td>
    <td style="padding:15px">ConfigurationService_ClearSystemLog</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/system-log/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetPublicSshKeyExpiration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetPublicSshKeyExpiration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/public-ssh-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetTicketSystemConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetTicketSystemConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/ticket-system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchTicketSystemConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchTicketSystemConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/ticket-system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetDatabaseConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_GetDatabaseConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchDatabaseConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchDatabaseConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetConfigurationReadOnlyMode(callback)</td>
    <td style="padding:15px">ConfigurationService_GetConfigurationReadOnlyMode</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/read-only-mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchConfigurationReadOnlyMode(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchConfigurationReadOnlyMode</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/read-only-mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceGetRotateSecretKeysStatus(callback)</td>
    <td style="padding:15px">ConfigurationService_GetRotateSecretKeysStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/security/rotate-secret-keys-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchGeneralConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchGeneralConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/general?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePostSamlConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PostSamlConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/saml/identity-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchSamlIdentityProviderConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchSamlIdentityProviderConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/saml/identity-provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchSessionRecordingConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchSessionRecordingConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/sessionrecording?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchFolderConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchFolderConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/folder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchApplicationSettingsConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchApplicationSettingsConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/application-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchPermissionOptionsConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchPermissionOptionsConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/permission-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchLauncherSettingsConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchLauncherSettingsConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/launcher-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchProtocolHandlerSettingsConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchProtocolHandlerSettingsConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/protocol-handler-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchUserExperienceConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchUserExperienceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/user-experience?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchUserInterfaceConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchUserInterfaceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/user-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServicePatchEmailConfiguration(body, callback)</td>
    <td style="padding:15px">ConfigurationService_PatchEmailConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceRunRpcNow(callback)</td>
    <td style="padding:15px">ConfigurationService_RunRpcNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/rpc/run-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceRunHeartbeatNow(callback)</td>
    <td style="padding:15px">ConfigurationService_RunHeartbeatNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/heartbeat/run-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceRebuildSecretSearchIndexerConfiguration(callback)</td>
    <td style="padding:15px">ConfigurationService_RebuildSecretSearchIndexerConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/secret-search-indexer/rebuild-index?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceRunAutoExportNow(callback)</td>
    <td style="padding:15px">ConfigurationService_RunAutoExportNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/auto-export/run-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceRunBackupNow(callback)</td>
    <td style="padding:15px">ConfigurationService_RunBackupNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/backup/run-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceRotateSecretKeys(callback)</td>
    <td style="padding:15px">ConfigurationService_RotateSecretKeys</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/security/rotate-secret-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceCancelRotateSecretKeys(callback)</td>
    <td style="padding:15px">ConfigurationService_CancelRotateSecretKeys</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/security/cancel-rotate-secret-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configurationServiceTestEmail(callback)</td>
    <td style="padding:15px">ConfigurationService_TestEmail</td>
    <td style="padding:15px">{base_path}/{version}/v1/configuration/email/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectionManagerSettingsServiceGet(callback)</td>
    <td style="padding:15px">ConnectionManagerSettingsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/connection-manager-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultSyncServiceGetSyncStatuses(filterIncludeInactive, filterSecretId, filterTenantId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultSyncService_GetSyncStatuses</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/sync/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultSyncServiceGetSyncStatus(syncMapId, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultSyncService_GetSyncStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/sync/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultSyncServiceCreateSync(body, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultSyncService_CreateSync</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/add-sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultSyncServiceSyncSecret(body, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultSyncService_SyncSecret</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultSyncServiceUpdateSync(syncSecretMapId, body, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultSyncService_UpdateSync</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/sync/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultTenantServiceGetList(filterIncludeInactive, filterNameSearch, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultTenantService_GetList</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultTenantServiceCreate(body, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultTenantService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultTenantServiceGetTenant(id, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultTenantService_GetTenant</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultTenantServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultTenantService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultTenantServiceGetTenantStub(callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultTenantService_GetTenantStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/tenant/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devOpsSecretsVaultTenantServiceGetTenantAudits(isExporting, filterSearchText, filterTenantId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DevOpsSecretsVaultTenantService_GetTenantAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/devops-secrets-vault/tenant/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetDiagnosticInformation(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetDiagnosticInformation</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetBackgroundProcesses(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetBackgroundProcesses</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/background-processes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetConnectivityReport(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetConnectivityReport</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/connectivity-report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetAppSettings(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetAppSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/app-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetQuartzJobs(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetQuartzJobs</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/quartz-jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetGeneralLogs(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetGeneralLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/general-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetSystemLogs(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetSystemLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/system-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetDiscoveryLogs(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetDiscoveryLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/discovery-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetComputerScanLogs(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetComputerScanLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/computer-scan-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetRPCLogs(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetRPCLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/rpc-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceGetHeartbeatLogs(callback)</td>
    <td style="padding:15px">DiagnosticsService_GetHeartbeatLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/heartbeat-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceClearQuartzJobErrors(callback)</td>
    <td style="padding:15px">DiagnosticsService_ClearQuartzJobErrors</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/clear-quartz-job-errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceClearUpgradeInProgress(callback)</td>
    <td style="padding:15px">DiagnosticsService_ClearUpgradeInProgress</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/clear-upgrade-in-progress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsServiceTestEventLog(callback)</td>
    <td style="padding:15px">DiagnosticsService_TestEventLog</td>
    <td style="padding:15px">{base_path}/{version}/v1/diagnostics/test-event-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosticsV2ServiceSearchSystemLog(filterLogLevel, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DiagnosticsV2Service_SearchSystemLog</td>
    <td style="padding:15px">{base_path}/{version}/v2/diagnostics/system-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceUnlinkDomainGroup(domainId, groupId, callback)</td>
    <td style="padding:15px">DirectoryServicesService_UnlinkDomainGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/{pathv1}/group/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceGetSynchronizationLog(filterEndDate, filterSearchText, filterStartDate, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DirectoryServicesService_GetSynchronizationLog</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/synchronization/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceGetSynchronizationStatus(callback)</td>
    <td style="padding:15px">DirectoryServicesService_GetSynchronizationStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/synchronization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceSearchDomains(filterDomainName, filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DirectoryServicesService_SearchDomains</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceCreateDomain(body, callback)</td>
    <td style="padding:15px">DirectoryServicesService_CreateDomain</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceGetDomain(id, callback)</td>
    <td style="padding:15px">DirectoryServicesService_GetDomain</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceGetLdapSyncSettings(id, callback)</td>
    <td style="padding:15px">DirectoryServicesService_GetLdapSyncSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/ldap-settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceGetDomainStub(domainType, callback)</td>
    <td style="padding:15px">DirectoryServicesService_GetDomainStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceSearchDirectoryForGroups(domainId, searchText, callback)</td>
    <td style="padding:15px">DirectoryServicesService_SearchDirectoryForGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/{pathv1}/groups/search-directory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceGetDirectoryGroupMembers(domainId, domainIdentifier, groupName, callback)</td>
    <td style="padding:15px">DirectoryServicesService_GetDirectoryGroupMembers</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceGetDirectoryServicesConfiguration(callback)</td>
    <td style="padding:15px">DirectoryServicesService_GetDirectoryServicesConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServicePatchDirectoryServicesConfiguration(body, callback)</td>
    <td style="padding:15px">DirectoryServicesService_PatchDirectoryServicesConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServicePatchLdapSyncSettings(domainId, body, callback)</td>
    <td style="padding:15px">DirectoryServicesService_PatchLdapSyncSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/ldap-settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServicePatchDomain(domainId, body, callback)</td>
    <td style="padding:15px">DirectoryServicesService_PatchDomain</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceSynchronizeNow(callback)</td>
    <td style="padding:15px">DirectoryServicesService_SynchronizeNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/synchronization-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directoryServicesServiceLinkDomainGroup(domainId, body, callback)</td>
    <td style="padding:15px">DirectoryServicesService_LinkDomainGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/directory-services/domains/{pathv1}/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceGetDisasterRecoveryDataReplica(replicaId, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_GetDisasterRecoveryDataReplica</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceDeleteDisasterRecoveryDataReplica(replicaId, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_DeleteDisasterRecoveryDataReplica</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServicePatchDisasterRecoveryDataReplica(replicaId, body, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_PatchDisasterRecoveryDataReplica</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceGetDisasterRecoveryIncomingConfiguration(callback)</td>
    <td style="padding:15px">DisasterRecoveryService_GetDisasterRecoveryIncomingConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/incoming-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServicePatchDisasterRecoveryIncomingConfiguration(body, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_PatchDisasterRecoveryIncomingConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/incoming-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceGetDisasterRecoveryConfigurationAudits(auditType, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_GetDisasterRecoveryConfigurationAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceGetDisasterRecoveryOutgoingConfiguration(filterLocation, filterName, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_GetDisasterRecoveryOutgoingConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/outgoing-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceGetDisasterRecoveryDataReplicationLogs(logType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_GetDisasterRecoveryDataReplicationLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServicePostDisasterRecoveryHandshake(body, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_PostDisasterRecoveryHandshake</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/handshake?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceRunDisasterRecoveryDataReplicationNow(minutes, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_RunDisasterRecoveryDataReplicationNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/run-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceRunDisasterRecoveryDataReplicaTestNow(callback)</td>
    <td style="padding:15px">DisasterRecoveryService_RunDisasterRecoveryDataReplicaTestNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceRequestDataReplicaReplicationStart(body, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_RequestDataReplicaReplicationStart</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceRequestDataReplicaReplicationStatus(body, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_RequestDataReplicaReplicationStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryServiceRequestDataReplicaReplicationPackage(body, callback)</td>
    <td style="padding:15px">DisasterRecoveryService_RequestDataReplicaReplicationPackage</td>
    <td style="padding:15px">{base_path}/{version}/v1/disaster-recovery/data-replication/package?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceSearchDiscoverySources(filterDiscoverySourceName, filterDiscoverySourceType, filterIncludeActive, filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DiscoveryService_SearchDiscoverySources</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetDiscoverySource(id, callback)</td>
    <td style="padding:15px">DiscoveryService_GetDiscoverySource</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceUpdateDiscoverySource(id, body, callback)</td>
    <td style="padding:15px">DiscoveryService_UpdateDiscoverySource</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetDiscoverySourceStub(typeId, callback)</td>
    <td style="padding:15px">DiscoveryService_GetDiscoverySourceStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/stub/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetDiscoveryConfiguration(callback)</td>
    <td style="padding:15px">DiscoveryService_GetDiscoveryConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceUpdateDiscoveryConfiguration(body, callback)</td>
    <td style="padding:15px">DiscoveryService_UpdateDiscoveryConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetDiscoveryStatus(callback)</td>
    <td style="padding:15px">DiscoveryService_GetDiscoveryStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetAvailableDiscoverySourceScanners(discoverySourceId, callback)</td>
    <td style="padding:15px">DiscoveryService_GetAvailableDiscoverySourceScanners</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}/available-scanners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceSearchDiscoverySourceScannerSettings(discoverySourceId, callback)</td>
    <td style="padding:15px">DiscoveryService_SearchDiscoverySourceScannerSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}/scanner-settings/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetScanTypes(callback)</td>
    <td style="padding:15px">DiscoveryService_GetScanTypes</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/scan-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetDiscoverySourceAudits(isExporting, filterDiscoverySourceId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DiscoveryService_GetDiscoverySourceAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery-source/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceSearchForDomainOu(discoverySourceId, include, searchText, selectedIds, take, callback)</td>
    <td style="padding:15px">DiscoveryService_SearchForDomainOu</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}/ou?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceGetDiscoverySourceFilter(id, callback)</td>
    <td style="padding:15px">DiscoveryService_GetDiscoverySourceFilter</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServicePatchDiscoverySourceFilter(memberId, discoverySourceId, body, callback)</td>
    <td style="padding:15px">DiscoveryService_PatchDiscoverySourceFilter</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}/filter/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceCreateDiscoverySource(body, callback)</td>
    <td style="padding:15px">DiscoveryService_CreateDiscoverySource</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceRunDiscoveryNow(body, callback)</td>
    <td style="padding:15px">DiscoveryService_RunDiscoveryNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceCreateDiscoverySourceScannerSettings(discoverySourceId, body, callback)</td>
    <td style="padding:15px">DiscoveryService_CreateDiscoverySourceScannerSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}/scanner-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryServiceUpdateDiscoverySourceFilters(discoverySourceId, body, callback)</td>
    <td style="padding:15px">DiscoveryService_UpdateDiscoverySourceFilters</td>
    <td style="padding:15px">{base_path}/{version}/v1/discovery/source/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetDistributedEngineConfiguration(callback)</td>
    <td style="padding:15px">DistributedEngineService_GetDistributedEngineConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServicePatchDistributedEngineConfiguration(body, callback)</td>
    <td style="padding:15px">DistributedEngineService_PatchDistributedEngineConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceSearchSiteConnectors(filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DistributedEngineService_SearchSiteConnectors</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetSiteConnector(id, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetSiteConnector</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceUpdateSiteConnector(id, body, callback)</td>
    <td style="padding:15px">DistributedEngineService_UpdateSiteConnector</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connector/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetSiteConnectorStub(queueType, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetSiteConnectorStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connector/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetSiteConnectorCredentials(siteConnectorId, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetSiteConnectorCredentials</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connector/{pathv1}/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceSearchEngines(filterActivationStatus, filterConnectionStatus, filterFriendlyName, filterOnlyIncludeRequiringAction, filterSiteId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DistributedEngineService_SearchEngines</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/engines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetEngineSettingsForSite(siteId, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetEngineSettingsForSite</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/engine-settings/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetEngineAuditsForSite(isExporting, filterEngineId, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, siteId, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetEngineAuditsForSite</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site/{pathv1}/engine-audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetEngineSettings(engineId, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetEngineSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/engine-settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceSearchSites(filterIncludeInactive, filterIncludeSiteMetrics, filterOnlyIncludeSitesThatCanAddNewEngines, filterSiteId, filterSiteName, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DistributedEngineService_SearchSites</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetSite(id, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetSite</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServicePatchSite(id, body, callback)</td>
    <td style="padding:15px">DistributedEngineService_PatchSite</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetSiteStub(callback)</td>
    <td style="padding:15px">DistributedEngineService_GetSiteStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetSiteAudits(id, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetSiteAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceSearchSiteLogs(id, filterEngineId, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DistributedEngineService_SearchSiteLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceDownloadMemoryMq(id, callback)</td>
    <td style="padding:15px">DistributedEngineService_DownloadMemoryMq</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connector/{pathv1}/download-memorymq?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceGetServerCapabilities(id, callback)</td>
    <td style="padding:15px">DistributedEngineService_GetServerCapabilities</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/{pathv1}/server-capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceDownloadDistributedEngine(is64Bit, siteId, callback)</td>
    <td style="padding:15px">DistributedEngineService_DownloadDistributedEngine</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/download-distributed-engine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServicePatchEngineSettings(engineSettingsId, body, callback)</td>
    <td style="padding:15px">DistributedEngineService_PatchEngineSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/engine-settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServicePatchEngineSettingsForEngine(engineId, body, callback)</td>
    <td style="padding:15px">DistributedEngineService_PatchEngineSettingsForEngine</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/engine-settings/engine/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceCreateSiteConnector(body, callback)</td>
    <td style="padding:15px">DistributedEngineService_CreateSiteConnector</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connector?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceValidateSiteConnector(siteConnectorId, callback)</td>
    <td style="padding:15px">DistributedEngineService_ValidateSiteConnector</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site-connector/{pathv1}/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceUpdateEngineStatus(body, callback)</td>
    <td style="padding:15px">DistributedEngineService_UpdateEngineStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/update-engine-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceCreateSite(body, callback)</td>
    <td style="padding:15px">DistributedEngineService_CreateSite</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">distributedEngineServiceReassignSecrets(id, body, callback)</td>
    <td style="padding:15px">DistributedEngineService_ReassignSecrets</td>
    <td style="padding:15px">{base_path}/{version}/v1/distributed-engine/site/{pathv1}/reassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainNameIndexServiceTruncateDomainNameIndex(callback)</td>
    <td style="padding:15px">DomainNameIndexService_TruncateDomainNameIndex</td>
    <td style="padding:15px">{base_path}/{version}/v1/domain-index/delete-all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainNameIndexServiceGetDomainNameIndex(filterDiscoverySourceIdScanned, filterDomainName, filterDomainResolveType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DomainNameIndexService_GetDomainNameIndex</td>
    <td style="padding:15px">{base_path}/{version}/v1/domain-index?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainNameIndexServiceUpdateDomainNameIndex(id, body, callback)</td>
    <td style="padding:15px">DomainNameIndexService_UpdateDomainNameIndex</td>
    <td style="padding:15px">{base_path}/{version}/v1/domain-index/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainNameIndexServiceCreateDomainNameIndex(body, callback)</td>
    <td style="padding:15px">DomainNameIndexService_CreateDomainNameIndex</td>
    <td style="padding:15px">{base_path}/{version}/v1/domain-index/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainNameIndexServiceExpireSingleDomainNameIndex(id, callback)</td>
    <td style="padding:15px">DomainNameIndexService_ExpireSingleDomainNameIndex</td>
    <td style="padding:15px">{base_path}/{version}/v1/domain-index/expire/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainNameIndexServiceExpireAllDomainNameIndex(body, callback)</td>
    <td style="padding:15px">DomainNameIndexService_ExpireAllDomainNameIndex</td>
    <td style="padding:15px">{base_path}/{version}/v1/domain-index/expire-all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainNameIndexServiceDeleteSingleDomainNameIndex(id, callback)</td>
    <td style="padding:15px">DomainNameIndexService_DeleteSingleDomainNameIndex</td>
    <td style="padding:15px">{base_path}/{version}/v1/domain-index/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceGet(id, callback)</td>
    <td style="padding:15px">DualControlsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">DualControlsService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceDelete(id, callback)</td>
    <td style="padding:15px">DualControlsService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceGetAllReports(dualControlType, id, callback)</td>
    <td style="padding:15px">DualControlsService_GetAllReports</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls/state/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceSearchDualControls(filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">DualControlsService_SearchDualControls</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceCreate(body, callback)</td>
    <td style="padding:15px">DualControlsService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceGetTypes(callback)</td>
    <td style="padding:15px">DualControlsService_GetTypes</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceStub(callback)</td>
    <td style="padding:15px">DualControlsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dualControlsServiceAuthorizeDualControl(dualControlType, id, body, callback)</td>
    <td style="padding:15px">DualControlsService_AuthorizeDualControl</td>
    <td style="padding:15px">{base_path}/{version}/v1/dual-controls/auth/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseServiceGetRequestData(requestId, callback)</td>
    <td style="padding:15px">EnterpriseService_GetRequestData</td>
    <td style="padding:15px">{base_path}/{version}/v1/enterprise/search-request/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseServiceSearchRequestData(body, callback)</td>
    <td style="padding:15px">EnterpriseService_SearchRequestData</td>
    <td style="padding:15px">{base_path}/{version}/v1/enterprise/search-request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceGetEventPipeline(id, callback)</td>
    <td style="padding:15px">EventPipelineService_GetEventPipeline</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceUpdateEventPipelines(id, body, callback)</td>
    <td style="padding:15px">EventPipelineService_UpdateEventPipelines</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceGetEventPipelines(filterEventEntityTypeId, filterEventPipelineName, filterEventPipelinePolicyId, filterIncludeActive, filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelineService_GetEventPipelines</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceGetEventPipelineSummaries(filterEventEntityTypeId, filterEventPipelineName, filterEventPipelinePolicyId, filterIncludeActive, filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelineService_GetEventPipelineSummaries</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/summaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceGetEventPipelineStub(callback)</td>
    <td style="padding:15px">EventPipelineService_GetEventPipelineStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceGetEventPipelineRuns(filterEventPipelineId, filterEventPipelinePolicyRunId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelineService_GetEventPipelineRuns</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceCreateEventPipelines(body, callback)</td>
    <td style="padding:15px">EventPipelineService_CreateEventPipelines</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceReorderPipeline(id, body, callback)</td>
    <td style="padding:15px">EventPipelineService_ReorderPipeline</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/{pathv1}/order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineServiceTogglePipelineActive(id, body, callback)</td>
    <td style="padding:15px">EventPipelineService_TogglePipelineActive</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/{pathv1}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineAuditServiceGetAllPipelineAndPolicyAudits(isExporting, filterEventPipelinePolicyId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelineAuditService_GetAllPipelineAndPolicyAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceRemoveEventPipelineFromPolicy(pipelineId, policyId, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_RemoveEventPipelineFromPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/pipeline/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetEventPipelinePolicy(id, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceUpdateEventPipelinePolicy(id, body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_UpdateEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceAddPipelineToEventPipelinePolicy(id, body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_AddPipelineToEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetEventPipelinePolicies(filterEventPipelineId, filterEventPipelinePolicyName, filterFolderId, filterIncludeActive, filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetEventPipelinePolicies</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetEventPipelinePolicyRuns(filterEventPipelinePolicyId, filterEventPipelinePolicyRunId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetEventPipelinePolicyRuns</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetEventPipelinePolicyRunActivity(eventPipelineId, eventPipelinePolicyRunId, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetEventPipelinePolicyRunActivity</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetSecretPoliciesForPipelinePolicies(id, filterEventPipelinePolicyId, filterEventPipelinePolicyName, filterIncludeActive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetSecretPoliciesForPipelinePolicies</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/secretpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetFoldersForPipelinePolicies(id, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetFoldersForPipelinePolicies</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceUpdateEventPipelinePolicyFolderMaps(id, body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_UpdateEventPipelinePolicyFolderMaps</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetGroupsForPipelinePolicies(id, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetGroupsForPipelinePolicies</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceUpdateEventPipelinePolicyGroupMaps(id, body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_UpdateEventPipelinePolicyGroupMaps</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetGroupCountForPipelinePolicy(id, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetGroupCountForPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/groups/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceGetChildFolderDataForPipelinePolicyFolder(folderId, id, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_GetChildFolderDataForPipelinePolicyFolder</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/folders/{pathv2}/childdata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceExportEventPipelinePolicy(id, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_ExportEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/export/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceCreateEventPipelinePolicy(body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_CreateEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceDuplicateEventPipelinePolicy(body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_DuplicateEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/duplicate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceImportEventPipelinePolicy(body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_ImportEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceUpdateEventPipelinePolicySortOrder(id, body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_UpdateEventPipelinePolicySortOrder</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelinePolicyServiceActivateEventPipelinePolicy(id, body, callback)</td>
    <td style="padding:15px">EventPipelinePolicyService_ActivateEventPipelinePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-policy/{pathv1}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineSettingsServiceGetEventPipelineTasks(filterEventActionId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventPipelineSettingsService_GetEventPipelineTasks</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-settings/task/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineSettingsServiceGetEventPipelineTaskSettings(id, callback)</td>
    <td style="padding:15px">EventPipelineSettingsService_GetEventPipelineTaskSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-settings/task-settings/list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineSettingsServiceGetEventPipelineFilterSettings(id, callback)</td>
    <td style="padding:15px">EventPipelineSettingsService_GetEventPipelineFilterSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-settings/filter-settings/list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineSettingsServiceGetPipelineTaskOptions(eventEntityTypeId, callback)</td>
    <td style="padding:15px">EventPipelineSettingsService_GetPipelineTaskOptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-settings/tasks/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineSettingsServiceGetPipelineFilterOptions(eventEntityTypeId, callback)</td>
    <td style="padding:15px">EventPipelineSettingsService_GetPipelineFilterOptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline-settings/filter/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineTriggerServiceGetEventPipelineTriggers(id, callback)</td>
    <td style="padding:15px">EventPipelineTriggerService_GetEventPipelineTriggers</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/{pathv1}/trigger?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineTriggerServiceGetEventPipelineStub(callback)</td>
    <td style="padding:15px">EventPipelineTriggerService_GetEventPipelineStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/trigger/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventPipelineTriggerServiceGetEventPipelineTriggerOptions(eventEntityTypeId, callback)</td>
    <td style="padding:15px">EventPipelineTriggerService_GetEventPipelineTriggerOptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-pipeline/trigger/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventSubscriptionsServiceGetSubscriptionStub(callback)</td>
    <td style="padding:15px">EventSubscriptionsService_GetSubscriptionStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-subscriptions/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventSubscriptionsServiceGetSubscriptionEntityTypes(callback)</td>
    <td style="padding:15px">EventSubscriptionsService_GetSubscriptionEntityTypes</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-subscriptions/event-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventSubscriptionsServiceSearchEventSubscriptions(filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">EventSubscriptionsService_SearchEventSubscriptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventSubscriptionsServiceCreateEventSubscription(body, callback)</td>
    <td style="padding:15px">EventSubscriptionsService_CreateEventSubscription</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventSubscriptionsServiceGetEventSubscription(eventSubscriptionId, callback)</td>
    <td style="padding:15px">EventSubscriptionsService_GetEventSubscription</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eventSubscriptionsServiceUpdateEventSubscription(eventSubscriptionId, body, callback)</td>
    <td style="padding:15px">EventSubscriptionsService_UpdateEventSubscription</td>
    <td style="padding:15px">{base_path}/{version}/v1/event-subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">extendedFieldsServiceGetRegexBySecret(secretId, callback)</td>
    <td style="padding:15px">ExtendedFieldsService_GetRegexBySecret</td>
    <td style="padding:15px">{base_path}/{version}/v1/extended-fields/regex/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">folderPermissionsServiceGet(includeInactive, id, callback)</td>
    <td style="padding:15px">FolderPermissionsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">folderPermissionsServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">FolderPermissionsService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">folderPermissionsServiceDelete(breakInheritance, id, callback)</td>
    <td style="padding:15px">FolderPermissionsService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">folderPermissionsServiceSearch(filterExcludeEditors, filterExcludeOwners, filterExcludeViewers, filterFolderId, filterGroupId, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">FolderPermissionsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">folderPermissionsServiceCreate(body, callback)</td>
    <td style="padding:15px">FolderPermissionsService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">folderPermissionsServiceStub(folderId, callback)</td>
    <td style="padding:15px">FolderPermissionsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder-permissions/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceGet(getAllChildren, includeAssociatedTemplates, folderPath, id, callback)</td>
    <td style="padding:15px">FoldersService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">FoldersService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceDelete(id, callback)</td>
    <td style="padding:15px">FoldersService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceDissassociateTemplateToFolder(id, templateId, callback)</td>
    <td style="padding:15px">FoldersService_DissassociateTemplateToFolder</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/{pathv1}/templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceUnpinFolder(id, callback)</td>
    <td style="padding:15px">FoldersService_UnpinFolder</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder/{pathv1}/pinned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServicePinFolder(id, body, callback)</td>
    <td style="padding:15px">FoldersService_PinFolder</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder/{pathv1}/pinned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceSearch(filterFolderTypeId, filterOnlyIncludeRootFolders, filterParentFolderId, filterPermissionRequired, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">FoldersService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceCreate(body, callback)</td>
    <td style="padding:15px">FoldersService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceLookup(filterFolderTypeId, filterOnlyIncludeRootFolders, filterParentFolderId, filterPermissionRequired, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">FoldersService_Lookup</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceStub(callback)</td>
    <td style="padding:15px">FoldersService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceGetAudits(id, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">FoldersService_GetAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceGetFolderDetail(id, returnEmptyInsteadOfNoAccessException, callback)</td>
    <td style="padding:15px">FoldersService_GetFolderDetail</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder-details/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceGetPinnedFolders(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">FoldersService_GetPinnedFolders</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/pinned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServicePatchFolder(id, body, callback)</td>
    <td style="padding:15px">FoldersService_PatchFolder</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServicePatchFolderPermissions(folderId, body, callback)</td>
    <td style="padding:15px">FoldersService_PatchFolderPermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/folder/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">foldersServiceAssociateTemplateToFolder(id, body, callback)</td>
    <td style="padding:15px">FoldersService_AssociateTemplateToFolder</td>
    <td style="padding:15px">{base_path}/{version}/v1/folders/{pathv1}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceGet(id, callback)</td>
    <td style="padding:15px">GroupsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">GroupsService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceDelete(id, callback)</td>
    <td style="padding:15px">GroupsService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceGetGroupUser(id, userId, callback)</td>
    <td style="padding:15px">GroupsService_GetGroupUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceRemoveUserFromGroup(id, userId, callback)</td>
    <td style="padding:15px">GroupsService_RemoveUserFromGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceGetRoles(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, id, callback)</td>
    <td style="padding:15px">GroupsService_GetRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceUpdateUserRoles(id, body, callback)</td>
    <td style="padding:15px">GroupsService_UpdateUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceCreateUserRoles(id, body, callback)</td>
    <td style="padding:15px">GroupsService_CreateUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceDeleteUserRoles(id, callback)</td>
    <td style="padding:15px">GroupsService_DeleteUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceSearch(filterDomainId, filterExcludeInboxRuleIdSubscribers, filterIncludeInactive, filterLimitToViewableGroups, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">GroupsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceCreate(body, callback)</td>
    <td style="padding:15px">GroupsService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceLookup(filterDomainId, filterExcludeInboxRuleIdSubscribers, filterIncludeInactive, filterLimitToViewableGroups, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">GroupsService_Lookup</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceStub(callback)</td>
    <td style="padding:15px">GroupsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceGetGroupMembership(id, filterIncludeInactiveUsersForGroup, filterUserDomainId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">GroupsService_GetGroupMembership</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceAddUserToGroup(id, body, callback)</td>
    <td style="padding:15px">GroupsService_AddUserToGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServicePatchGroupMembership(id, body, callback)</td>
    <td style="padding:15px">GroupsService_PatchGroupMembership</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceGetAuditGroupAssignments(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">GroupsService_GetAuditGroupAssignments</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServicePatchGroup(groupId, body, callback)</td>
    <td style="padding:15px">GroupsService_PatchGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupsServiceUpdateGroupMembers(groupId, body, callback)</td>
    <td style="padding:15px">GroupsService_UpdateGroupMembers</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheckServiceGet(callback)</td>
    <td style="padding:15px">HealthCheckService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/healthcheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hsmConfigurationServiceGetHsmConfigurationAudit(isExporting, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">HsmConfigurationService_GetHsmConfigurationAudit</td>
    <td style="padding:15px">{base_path}/{version}/v1/hsm-configuration/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetResource(id, callback)</td>
    <td style="padding:15px">InboxService_GetResource</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceDeleteResource(id, callback)</td>
    <td style="padding:15px">InboxService_DeleteResource</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceSearchInboxMessages(messageTypeId, filterEndDate, messageDataFilters0DisplayValue, messageDataFilters0InboxDataName, messageDataFilters0ValueBool, messageDataFilters0ValueDateTimeEnd, messageDataFilters0ValueDateTimeStart, messageDataFilters0ValueInt, messageDataFilters0ValueString, filterMessageTypeIds, filterReadStatusFilter, filterStartDate, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">InboxService_SearchInboxMessages</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/messages/{messageTypeId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetInboxMessage(messageId, callback)</td>
    <td style="padding:15px">InboxService_GetInboxMessage</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/message/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetInboxMessageDataNames(messageTypeIds, callback)</td>
    <td style="padding:15px">InboxService_GetInboxMessageDataNames</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/data-names?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetInboxTemplates(filterTemplateType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">InboxService_GetInboxTemplates</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceCreateInboxTemplate(body, callback)</td>
    <td style="padding:15px">InboxService_CreateInboxTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetInboxTemplate(templateId, callback)</td>
    <td style="padding:15px">InboxService_GetInboxTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServicePatchInboxTemplate(templateId, body, callback)</td>
    <td style="padding:15px">InboxService_PatchInboxTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetInboxTemplateLocale(localeId, templateId, callback)</td>
    <td style="padding:15px">InboxService_GetInboxTemplateLocale</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/{pathv1}/locales/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServicePatchInboxTemplateLocale(localeId, templateId, body, callback)</td>
    <td style="padding:15px">InboxService_PatchInboxTemplateLocale</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/{pathv1}/locales/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetInboxMessageTypes(includeCurrentUserMessageCounts, callback)</td>
    <td style="padding:15px">InboxService_GetInboxMessageTypes</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/message-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceSearchResources(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">InboxService_SearchResources</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceUploadResource(body, callback)</td>
    <td style="padding:15px">InboxService_UploadResource</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetNotifications(includeArchived, markAlertsAsViewed, callback)</td>
    <td style="padding:15px">InboxService_GetNotifications</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceGetNotificationsStatus(callback)</td>
    <td style="padding:15px">InboxService_GetNotificationsStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceSendTestMessage(body, callback)</td>
    <td style="padding:15px">InboxService_SendTestMessage</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/send-test-message?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceCopyInboxTemplate(body, callback)</td>
    <td style="padding:15px">InboxService_CopyInboxTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/copy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceCreateInboxTemplateLocale(body, callback)</td>
    <td style="padding:15px">InboxService_CreateInboxTemplateLocale</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/templates/template-locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceUpdateMessageReadStatus(body, callback)</td>
    <td style="padding:15px">InboxService_UpdateMessageReadStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox/update-read?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceMarkAlertNotificationRead(body, callback)</td>
    <td style="padding:15px">InboxService_MarkAlertNotificationRead</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications/notification-read?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxServiceMarkAlertNotificationUnread(body, callback)</td>
    <td style="padding:15px">InboxService_MarkAlertNotificationUnread</td>
    <td style="padding:15px">{base_path}/{version}/v1/notifications/notification-unread?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceSubscribeCurrentUserToRule(ruleId, callback)</td>
    <td style="padding:15px">InboxRulesService_SubscribeCurrentUserToRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceUnsubscribeCurrentUserFromRule(ruleId, callback)</td>
    <td style="padding:15px">InboxRulesService_UnsubscribeCurrentUserFromRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceSearchInboxRules(filterIncludeCurrentUserSubscriptionStatus, filterIncludeInactive, filterIsImmediate, filterMessageId, filterMessageTypeId, ruleConditionFilters0DisplayValue, ruleConditionFilters0InboxDataId, ruleConditionFilters0InboxDataName, ruleConditionFilters0ValueBool, ruleConditionFilters0ValueInt, ruleConditionFilters0ValueString, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">InboxRulesService_SearchInboxRules</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceCreateInboxRule(body, callback)</td>
    <td style="padding:15px">InboxRulesService_CreateInboxRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceGetInboxRule(id, callback)</td>
    <td style="padding:15px">InboxRulesService_GetInboxRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServicePatchInboxRule(id, body, callback)</td>
    <td style="padding:15px">InboxRulesService_PatchInboxRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceGetInboxRuleStub(callback)</td>
    <td style="padding:15px">InboxRulesService_GetInboxRuleStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceGetInboxRuleCondition(id, callback)</td>
    <td style="padding:15px">InboxRulesService_GetInboxRuleCondition</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/conditions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceSearchInboxRuleConditions(id, callback)</td>
    <td style="padding:15px">InboxRulesService_SearchInboxRuleConditions</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceSearchSubscribers(inboxRuleId, filterAccountTypes, filterOnlyIncludeUnsubscribableUsers, filterSearchText, filterStatus, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">InboxRulesService_SearchSubscribers</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceSearchLog(inboxRuleId, filterEndDate, filterRuleActionStatus, filterStartDate, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">InboxRulesService_SearchLog</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/action-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServicePatchInboxRuleActions(id, body, callback)</td>
    <td style="padding:15px">InboxRulesService_PatchInboxRuleActions</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServicePatchInboxRuleSubscribers(ruleId, body, callback)</td>
    <td style="padding:15px">InboxRulesService_PatchInboxRuleSubscribers</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceCopyInboxRule(body, callback)</td>
    <td style="padding:15px">InboxRulesService_CopyInboxRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/copy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceUpdateInboxRuleCondition(inboxRuleId, body, callback)</td>
    <td style="padding:15px">InboxRulesService_UpdateInboxRuleCondition</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inboxRulesServiceCreateInboxRuleCondition(inboxRuleId, body, callback)</td>
    <td style="padding:15px">InboxRulesService_CreateInboxRuleCondition</td>
    <td style="padding:15px">{base_path}/{version}/v1/inbox-rules/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceGet(id, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceDelete(id, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceGetGroup(groupId, id, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_GetGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceDeleteGroupIpRestriction(groupId, id, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_DeleteGroupIpRestriction</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceGetUserIpRestriction(id, userId, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_GetUserIpRestriction</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceDeleteUserIpRestriction(id, userId, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_DeleteUserIpRestriction</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceSearch(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceCreate(body, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceSearchGroups(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_SearchGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceCreateGroupIpRestriction(id, body, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_CreateGroupIpRestriction</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceGetAllByGroup(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_GetAllByGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/groups/{pathv1}/ipaddress-restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceSearchUsers(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_SearchUsers</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceCreateUserIpRestriction(id, body, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_CreateUserIpRestriction</td>
    <td style="padding:15px">{base_path}/{version}/v1/ipaddress-restrictions/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipAddressRestrictionsServiceGetAllByUser(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">IpAddressRestrictionsService_GetAllByUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/ipaddress-restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceDeleteJumpboxRouteLevel(id, order, callback)</td>
    <td style="padding:15px">JumpboxRouteService_DeleteJumpboxRouteLevel</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/{pathv1}/level/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServicePatchJumpboxRouteLevel(id, order, body, callback)</td>
    <td style="padding:15px">JumpboxRouteService_PatchJumpboxRouteLevel</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/{pathv1}/level/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceSearch(filterIncludeActive, filterIncludeInactive, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">JumpboxRouteService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceGetAllRoutesForUser(filterIncludeActive, filterIncludeInactive, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">JumpboxRouteService_GetAllRoutesForUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceGet(id, callback)</td>
    <td style="padding:15px">JumpboxRouteService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceUpdateJumpboxRoute(id, body, callback)</td>
    <td style="padding:15px">JumpboxRouteService_UpdateJumpboxRoute</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceGetJumpboxAudits(id, isExporting, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">JumpboxRouteService_GetJumpboxAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceGetAllLevels(id, callback)</td>
    <td style="padding:15px">JumpboxRouteService_GetAllLevels</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/{pathv1}/level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceAddJumpboxRouteLevel(id, body, callback)</td>
    <td style="padding:15px">JumpboxRouteService_AddJumpboxRouteLevel</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route/{pathv1}/level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jumpboxRouteServiceCreatejumpboxRoute(body, callback)</td>
    <td style="padding:15px">JumpboxRouteService_CreatejumpboxRoute</td>
    <td style="padding:15px">{base_path}/{version}/v1/jumpbox-route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceGet(callback)</td>
    <td style="padding:15px">KeyManagementService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceUpdate(body, callback)</td>
    <td style="padding:15px">KeyManagementService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceStub(callback)</td>
    <td style="padding:15px">KeyManagementService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceGetMasterEncryptionKeyRotationInfo(callback)</td>
    <td style="padding:15px">KeyManagementService_GetMasterEncryptionKeyRotationInfo</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management/mekrotationinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceSearchKeyRotationAudit(isExporting, filterAction, filterDate, filterSearchTerm, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">KeyManagementService_SearchKeyRotationAudit</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceGetKeyRotationAuditUsers(callback)</td>
    <td style="padding:15px">KeyManagementService_GetKeyRotationAuditUsers</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management/audit/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceStartMasterEncryptionKeyRotation(callback)</td>
    <td style="padding:15px">KeyManagementService_StartMasterEncryptionKeyRotation</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management/startmekrotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keyManagementServiceRetryMasterEncryptionKeyStatus(callback)</td>
    <td style="padding:15px">KeyManagementService_RetryMasterEncryptionKeyStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/key-management/retrymekstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceSearchCollections(filterIncludeInactive, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">LauncherAgentsService_SearchCollections</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceCreateCollection(body, callback)</td>
    <td style="padding:15px">LauncherAgentsService_CreateCollection</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceStub(includeInactive, searchText, callback)</td>
    <td style="padding:15px">LauncherAgentsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/collections/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceGetByCollectionId(includeInactive, id, callback)</td>
    <td style="padding:15px">LauncherAgentsService_GetByCollectionId</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/collections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceUpdateCollection(id, body, callback)</td>
    <td style="padding:15px">LauncherAgentsService_UpdateCollection</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/collections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceSearchAgentsWithIssues(filterLauncherAgentCollectionId, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">LauncherAgentsService_SearchAgentsWithIssues</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceSearch(filterIncludeInactive, filterLauncherAgentCollectionId, filterOutOfDate, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">LauncherAgentsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceGet(includeInactive, id, callback)</td>
    <td style="padding:15px">LauncherAgentsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launcherAgentsServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">LauncherAgentsService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/launcheragents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchersServiceSearchLaunchers(filterApplication, filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">LaunchersService_SearchLaunchers</td>
    <td style="padding:15px">{base_path}/{version}/v1/launchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchersServiceLookup(filterApplication, filterIncludeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">LaunchersService_Lookup</td>
    <td style="padding:15px">{base_path}/{version}/v1/launchers/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchersServiceGet(id, callback)</td>
    <td style="padding:15px">LaunchersService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/launchers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchersServiceSearchLauncherDetailsV2(filterSecretId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">LaunchersService_SearchLauncherDetailsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/launchers/secret?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchersServiceTriggerDownload(forceMsi, is32Bit, isRDS, callback)</td>
    <td style="padding:15px">LaunchersService_TriggerDownload</td>
    <td style="padding:15px">{base_path}/{version}/v1/launchers/protocol-handler?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchersServiceCreate(body, callback)</td>
    <td style="padding:15px">LaunchersService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/launchers/secret?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licenseServiceGet(callback)</td>
    <td style="padding:15px">LicenseService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/license/validforconnectionmanager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceDeleteMetadata(itemId, itemType, metadataItemDataId, callback)</td>
    <td style="padding:15px">MetadataService_DeleteMetadata</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceSearchMetadata(filterItemId, filterMetaDataFieldId, filterMetadataType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">MetadataService_SearchMetadata</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceSearchMetadataHistory(isExporting, filterEndDate, filterItemId, filterMetaDataFieldId, filterMetadataType, filterStartDate, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">MetadataService_SearchMetadataHistory</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceGetFieldSections(filterItemId, filterMetadataSectionFilterId, filterMetadataType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">MetadataService_GetFieldSections</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata/field-sections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceGetFields(callback)</td>
    <td style="padding:15px">MetadataService_GetFields</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceCreateMetadata(itemId, itemType, body, callback)</td>
    <td style="padding:15px">MetadataService_CreateMetadata</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceUpdateMetadata(itemId, itemType, body, callback)</td>
    <td style="padding:15px">MetadataService_UpdateMetadata</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">metadataServiceUpdateMetadataFieldSection(fieldSectionId, itemId, itemType, body, callback)</td>
    <td style="padding:15px">MetadataService_UpdateMetadataFieldSection</td>
    <td style="padding:15px">{base_path}/{version}/v1/metadata/field-sections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mobileServiceGetMobileConfiguration(callback)</td>
    <td style="padding:15px">MobileService_GetMobileConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/mobile-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oAuthExpirationServiceExpireSession(callback)</td>
    <td style="padding:15px">OAuthExpirationService_ExpireSession</td>
    <td style="padding:15px">{base_path}/{version}/v1/oauth-expiration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oneTimePasswordCodeServiceGet(numberOfCodesToGenerate, id, callback)</td>
    <td style="padding:15px">OneTimePasswordCodeService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/one-time-password-code/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordRequirementsServiceSearchPasswordRequirements(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">PasswordRequirementsService_SearchPasswordRequirements</td>
    <td style="padding:15px">{base_path}/{version}/v1/password-requirements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordRequirementsServiceCreate(body, callback)</td>
    <td style="padding:15px">PasswordRequirementsService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/password-requirements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordRequirementsServiceGet(id, callback)</td>
    <td style="padding:15px">PasswordRequirementsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/password-requirements/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordRequirementsServicePatch(id, body, callback)</td>
    <td style="padding:15px">PasswordRequirementsService_Patch</td>
    <td style="padding:15px">{base_path}/{version}/v1/password-requirements/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passwordRequirementsServiceUpdateRules(id, body, callback)</td>
    <td style="padding:15px">PasswordRequirementsService_UpdateRules</td>
    <td style="padding:15px">{base_path}/{version}/v1/password-requirements/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pbaConfigurationServiceGetPbaConfiguration(callback)</td>
    <td style="padding:15px">PbaConfigurationService_GetPbaConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/pba-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pbaConfigurationServiceUpdatePbaConfiguration(body, callback)</td>
    <td style="padding:15px">PbaConfigurationService_UpdatePbaConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/pba-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pbaConfigurationServiceProcessPbaHistoryImport(body, callback)</td>
    <td style="padding:15px">PbaConfigurationService_ProcessPbaHistoryImport</td>
    <td style="padding:15px">{base_path}/{version}/v1/pba-history-import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pbaConfigurationServiceTestPbaConnection(callback)</td>
    <td style="padding:15px">PbaConfigurationService_TestPbaConnection</td>
    <td style="padding:15px">{base_path}/{version}/v1/pba-test-connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pbaConfigurationServiceConfirmPbaPair(callback)</td>
    <td style="padding:15px">PbaConfigurationService_ConfirmPbaPair</td>
    <td style="padding:15px">{base_path}/{version}/v1/pba-confirm-pair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pbaConfigurationServiceSyncPbaMetadata(callback)</td>
    <td style="padding:15px">PbaConfigurationService_SyncPbaMetadata</td>
    <td style="padding:15px">{base_path}/{version}/v1/pba-sync-metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceDeleteTerminalClientOverrides(clientOverrideId, callback)</td>
    <td style="padding:15px">ProxyService_DeleteTerminalClientOverrides</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/client-overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceUpdateTerminalClientOverrides(clientOverrideId, body, callback)</td>
    <td style="padding:15px">ProxyService_UpdateTerminalClientOverrides</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/client-overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetSshProxyConfiguration(callback)</td>
    <td style="padding:15px">ProxyService_GetSshProxyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServicePatchSshProxyConfiguration(body, callback)</td>
    <td style="padding:15px">ProxyService_PatchSshProxyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetRdpProxyConfiguration(callback)</td>
    <td style="padding:15px">ProxyService_GetRdpProxyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/rdp/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServicePatchRdpProxyConfiguration(body, callback)</td>
    <td style="padding:15px">ProxyService_PatchRdpProxyConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/rdp/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetExplanations(callback)</td>
    <td style="padding:15px">ProxyService_GetExplanations</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/explanation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetAudits(isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ProxyService_GetAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetEndpoints(callback)</td>
    <td style="padding:15px">ProxyService_GetEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetSshEndpointNotification(callback)</td>
    <td style="padding:15px">ProxyService_GetSshEndpointNotification</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetRdpEndpointNotification(callback)</td>
    <td style="padding:15px">ProxyService_GetRdpEndpointNotification</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/rdp/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetEndpointNotification(callback)</td>
    <td style="padding:15px">ProxyService_GetEndpointNotification</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/endpoints/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetTerminalClients(filterIpAddress, filterTerminalClientType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ProxyService_GetTerminalClients</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetTerminalClientHistory(filterAuthenticateResult, filterEndDate, filterEngineIdentityGuid, filterIpAddress, filterStartDate, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ProxyService_GetTerminalClientHistory</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/clienthistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetTerminalClientOverrides(callback)</td>
    <td style="padding:15px">ProxyService_GetTerminalClientOverrides</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/client-overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceCreateTerminalClientOverrides(body, callback)</td>
    <td style="padding:15px">ProxyService_CreateTerminalClientOverrides</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/client-overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGetProxyingState(callback)</td>
    <td style="padding:15px">ProxyService_GetProxyingState</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServicePatchNode(id, body, callback)</td>
    <td style="padding:15px">ProxyService_PatchNode</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/endpoints/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServicePatchSite(id, body, callback)</td>
    <td style="padding:15px">ProxyService_PatchSite</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/endpoints/sites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServicePatchEngine(id, body, callback)</td>
    <td style="padding:15px">ProxyService_PatchEngine</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/endpoints/engines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceUpdateTerminalClientType(clientId, body, callback)</td>
    <td style="padding:15px">ProxyService_UpdateTerminalClientType</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGenerateSshHostKey(callback)</td>
    <td style="padding:15px">ProxyService_GenerateSshHostKey</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/ssh/generate-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">proxyServiceGenerateRdpProxyCertificate(body, callback)</td>
    <td style="padding:15px">ProxyService_GenerateRdpProxyCertificate</td>
    <td style="padding:15px">{base_path}/{version}/v1/proxy/rdp/generate-certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceGetPasswordType(id, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_GetPasswordType</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/password-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceUpdatePasswordType(id, body, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_UpdatePasswordType</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/password-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceDeletePasswordType(id, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_DeletePasswordType</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/password-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceGetCustomCommands(id, filterCommandTypeCode, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_GetCustomCommands</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/custom-commands/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceUpdateCustomCommand(id, body, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_UpdateCustomCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/custom-commands/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceDeleteCustomCommand(id, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_DeleteCustomCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/custom-commands/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceGetPasswordTypeList(filterIncludeInactive, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_GetPasswordTypeList</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/password-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceCreatePasswordType(body, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_CreatePasswordType</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/password-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remotePasswordChangingServiceCreateCustomCommand(body, callback)</td>
    <td style="padding:15px">RemotePasswordChangingService_CreateCustomCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/remote-password-changing/custom-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportDetail(id, callback)</td>
    <td style="padding:15px">ReportsService_GetReportDetail</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceUpdateReport(id, body, callback)</td>
    <td style="padding:15px">ReportsService_UpdateReport</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceDelete(id, callback)</td>
    <td style="padding:15px">ReportsService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceDeleteV2(id, callback)</td>
    <td style="padding:15px">ReportsService_DeleteV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportCategory(reportCategoryId, callback)</td>
    <td style="padding:15px">ReportsService_GetReportCategory</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceDeleteReportCategory(reportCategoryId, callback)</td>
    <td style="padding:15px">ReportsService_DeleteReportCategory</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceUpdateReportCategory(reportCategoryId, body, callback)</td>
    <td style="padding:15px">ReportsService_UpdateReportCategory</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceDeleteReportCategoryV2(reportCategoryId, callback)</td>
    <td style="padding:15px">ReportsService_DeleteReportCategoryV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/reports/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportSchedule(reportScheduleId, callback)</td>
    <td style="padding:15px">ReportsService_GetReportSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceDeleteReportSchedule(reportScheduleId, callback)</td>
    <td style="padding:15px">ReportsService_DeleteReportSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceUpdateReportSchedule(reportScheduleId, body, callback)</td>
    <td style="padding:15px">ReportsService_UpdateReportSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceDeleteReportScheduleV2(reportScheduleId, callback)</td>
    <td style="padding:15px">ReportsService_DeleteReportScheduleV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/reports/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportAuditsById(id, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ReportsService_GetReportAuditsById</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportAudits(isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ReportsService_GetReportAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceSearchReportSummary(filterCategoryId, filterIncludeInactive, filterReportName, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ReportsService_SearchReportSummary</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceCreateReport(body, callback)</td>
    <td style="padding:15px">ReportsService_CreateReport</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceLookup(filterCategoryId, filterIncludeInactive, filterReportName, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ReportsService_Lookup</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetCategories(callback)</td>
    <td style="padding:15px">ReportsService_GetCategories</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceCreateReportCategory(body, callback)</td>
    <td style="padding:15px">ReportsService_CreateReportCategory</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetChartTypes(callback)</td>
    <td style="padding:15px">ReportsService_GetChartTypes</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/charttypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetDefaultParameters(id, callback)</td>
    <td style="padding:15px">ReportsService_GetDefaultParameters</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}/defaultparameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceSearchReportSchedules(filterIncludeDeleted, filterReportId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ReportsService_SearchReportSchedules</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceCreateReportSchedule(body, callback)</td>
    <td style="padding:15px">ReportsService_CreateReportSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceStubReportSchedule(reportId, callback)</td>
    <td style="padding:15px">ReportsService_StubReportSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/stub/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceSearchReportScheduleHistory(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, reportScheduleId, callback)</td>
    <td style="padding:15px">ReportsService_SearchReportScheduleHistory</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/{pathv1}/history/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportScheduleHistory(reportScheduleHistoryId, callback)</td>
    <td style="padding:15px">ReportsService_GetReportScheduleHistory</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportPermissions(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, reportId, callback)</td>
    <td style="padding:15px">ReportsService_GetReportPermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceUpdateReportPermissions(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, reportId, body, callback)</td>
    <td style="padding:15px">ReportsService_UpdateReportPermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportPermissionsFromCategory(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, reportCategoryId, callback)</td>
    <td style="padding:15px">ReportsService_GetReportPermissionsFromCategory</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}/permissions-from-category?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportCategoryPermissions(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, reportCategoryId, callback)</td>
    <td style="padding:15px">ReportsService_GetReportCategoryPermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceUpdateReportCategoryPermissions(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, reportCategoryId, body, callback)</td>
    <td style="padding:15px">ReportsService_UpdateReportCategoryPermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportPermissionOptions(callback)</td>
    <td style="padding:15px">ReportsService_GetReportPermissionOptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/permissions/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceGetReportCategoryPermissionOptions(callback)</td>
    <td style="padding:15px">ReportsService_GetReportCategoryPermissionOptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/categories/permissions/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceExport(body, callback)</td>
    <td style="padding:15px">ReportsService_Export</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceExecute(body, callback)</td>
    <td style="padding:15px">ReportsService_Execute</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceEmail(id, body, callback)</td>
    <td style="padding:15px">ReportsService_Email</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceDownloadHistoricalReport(reportScheduleHistoryId, callback)</td>
    <td style="padding:15px">ReportsService_DownloadHistoricalReport</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/{pathv1}/history/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceUndeleteReportSchedule(reportScheduleId, callback)</td>
    <td style="padding:15px">ReportsService_UndeleteReportSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/schedules/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportsServiceUndeleteSystemReport(reportId, callback)</td>
    <td style="padding:15px">ReportsService_UndeleteSystemReport</td>
    <td style="padding:15px">{base_path}/{version}/v1/reports/{pathv1}/undelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">roleAuditServiceGetRoleAudits(isExporting, filterAuditType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">RoleAuditService_GetRoleAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/role-audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolePermissionsServiceGetUnassigned(callback)</td>
    <td style="padding:15px">RolePermissionsService_GetUnassigned</td>
    <td style="padding:15px">{base_path}/{version}/v1/rolepermissions/unassigned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceGet(id, callback)</td>
    <td style="padding:15px">RolesService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceUpdate(id, body, callback)</td>
    <td style="padding:15px">RolesService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceGetAll(filterGroupId, filterIncludeInactive, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">RolesService_GetAll</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceCreate(body, callback)</td>
    <td style="padding:15px">RolesService_Create</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceGetRoleGroups(id, filterIncludeInactiveUsersForGroup, filterUserDomainId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">RolesService_GetRoleGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceGetRolePermissions(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">RolesService_GetRolePermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceUpdatePermissions(id, body, callback)</td>
    <td style="padding:15px">RolesService_UpdatePermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceGetAllRolePermissionsByType(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">RolesService_GetAllRolePermissionsByType</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/{pathv1}/permissions/unassigned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServiceStub(callback)</td>
    <td style="padding:15px">RolesService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rolesServicePatchGroups(roleId, body, callback)</td>
    <td style="padding:15px">RolesService_PatchGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleServiceGetSchedule(scheduleId, callback)</td>
    <td style="padding:15px">ScheduleService_GetSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleServiceUpdateSchedule(scheduleId, body, callback)</td>
    <td style="padding:15px">ScheduleService_UpdateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleServiceCreateSchedule(body, callback)</td>
    <td style="padding:15px">ScheduleService_CreateSchedule</td>
    <td style="padding:15px">{base_path}/{version}/v1/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scriptServiceSearch(filterIncludeInactive, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">ScriptService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/userscripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scriptServicePost(body, callback)</td>
    <td style="padding:15px">ScriptService_Post</td>
    <td style="padding:15px">{base_path}/{version}/v1/userscripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scriptServiceLoad(id, callback)</td>
    <td style="padding:15px">ScriptService_Load</td>
    <td style="padding:15px">{base_path}/{version}/v1/userscripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAccountsServiceSearchClientAccounts(filterOperator, filterSearchText, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SdkClientAccountsService_SearchClientAccounts</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAccountsServiceCreateClientAccount(body, callback)</td>
    <td style="padding:15px">SdkClientAccountsService_CreateClientAccount</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAccountsServiceGet(id, callback)</td>
    <td style="padding:15px">SdkClientAccountsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAccountsServiceUpdateClientAccount(id, body, callback)</td>
    <td style="padding:15px">SdkClientAccountsService_UpdateClientAccount</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAccountsServiceGetEnabled(callback)</td>
    <td style="padding:15px">SdkClientAccountsService_GetEnabled</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-accounts/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAccountsServiceToggleEnabled(callback)</td>
    <td style="padding:15px">SdkClientAccountsService_ToggleEnabled</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-accounts/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAccountsServiceRevoke(id, callback)</td>
    <td style="padding:15px">SdkClientAccountsService_Revoke</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-accounts/{pathv1}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientAuditsServiceSearchClientAudit(filterOperator, filterSearchText, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SdkClientAuditsService_SearchClientAudit</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientRulesServiceGet(id, callback)</td>
    <td style="padding:15px">SdkClientRulesService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientRulesServiceUpdateClientRule(id, body, callback)</td>
    <td style="padding:15px">SdkClientRulesService_UpdateClientRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientRulesServiceDelete(id, callback)</td>
    <td style="padding:15px">SdkClientRulesService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientRulesServiceSearchClientRules(filterOperator, filterSearchText, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SdkClientRulesService_SearchClientRules</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientRulesServiceCreateClientRule(body, callback)</td>
    <td style="padding:15px">SdkClientRulesService_CreateClientRule</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientRulesServiceGetOnboardingKey(id, callback)</td>
    <td style="padding:15px">SdkClientRulesService_GetOnboardingKey</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-rules/{pathv1}/onboarding-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sdkClientRulesServiceStub(callback)</td>
    <td style="padding:15px">SdkClientRulesService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/sdk-client-rules/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceSearchRequests(filterIsMyRequest, filterStatus, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_SearchRequests</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceUpdateRequest(body, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_UpdateRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceCreateRequest(body, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_CreateRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceGetHistory(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_GetHistory</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceGetRequestsForSecret(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_GetRequestsForSecret</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceGetRequest(id, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_GetRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceGetPendingRequest(id, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_GetPendingRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests/{pathv1}/pending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceGetOptionsBySecret(id, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_GetOptionsBySecret</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests/secrets/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretAccessRequestsServiceCreateViewComment(id, body, callback)</td>
    <td style="padding:15px">SecretAccessRequestsService_CreateViewComment</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-access-requests/secrets/{pathv1}/view-comment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceGetDependency(id, callback)</td>
    <td style="padding:15px">SecretDependenciesService_GetDependency</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceUpdateDependency(id, body, callback)</td>
    <td style="padding:15px">SecretDependenciesService_UpdateDependency</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceDelete(id, callback)</td>
    <td style="padding:15px">SecretDependenciesService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceGetGroups(secretId, callback)</td>
    <td style="padding:15px">SecretDependenciesService_GetGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceCreateDependencyGroup(secretId, body, callback)</td>
    <td style="padding:15px">SecretDependenciesService_CreateDependencyGroup</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceGetScripts(callback)</td>
    <td style="padding:15px">SecretDependenciesService_GetScripts</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceGetTemplates(callback)</td>
    <td style="padding:15px">SecretDependenciesService_GetTemplates</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceSearchDependencySummary(filterGroupId, filterIncludeInactive, filterLastRunStatus, filterSearchText, filterSecretId, filterTemplateId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretDependenciesService_SearchDependencySummary</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceCreateDependency(body, callback)</td>
    <td style="padding:15px">SecretDependenciesService_CreateDependency</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceGetDependencyRunTaskStatus(identifier, callback)</td>
    <td style="padding:15px">SecretDependenciesService_GetDependencyRunTaskStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/run/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceStub(scriptId, secretId, templateId, typeId, callback)</td>
    <td style="padding:15px">SecretDependenciesService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretDependenciesServiceExecute(body, callback)</td>
    <td style="padding:15px">SecretDependenciesService_Execute</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-dependencies/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceGetHistory(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_GetHistory</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceGetRequest(id, callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_GetRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceGetRequestSecrets(filterErased, filterSecretEraseRequestId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_GetRequestSecrets</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceInboxSearch(filterIsMyRequest, filterStatus, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_InboxSearch</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests/inbox?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceSearch(filterIsMyRequest, filterSecretId, filterStatus, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceProcessNow(callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_ProcessNow</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests/run-now?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceUpdateRequest(body, callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_UpdateRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretEraseRequestsServiceCreateRequest(body, callback)</td>
    <td style="padding:15px">SecretEraseRequestsService_CreateRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-erase-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretExtensionsServiceGetWebSecretTemplates(callback)</td>
    <td style="padding:15px">SecretExtensionsService_GetWebSecretTemplates</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-extensions/web-secret-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretExtensionsServiceSearch(body, callback)</td>
    <td style="padding:15px">SecretExtensionsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-extensions/search-by-url?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretExtensionsServiceSearchActiveDirectorySecrets(body, callback)</td>
    <td style="padding:15px">SecretExtensionsService_SearchActiveDirectorySecrets</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-extensions/search-ad-secrets-by-domain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretExtensionsServiceSearchWindowsAccountSecrets(body, callback)</td>
    <td style="padding:15px">SecretExtensionsService_SearchWindowsAccountSecrets</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-extensions/search-windows-account-secrets-by-computer-name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretExtensionsServiceGetAutoFillValues(body, callback)</td>
    <td style="padding:15px">SecretExtensionsService_GetAutoFillValues</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-extensions/autofill-values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHealthServiceGetSecretExposure(filterSecretId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretHealthService_GetSecretExposure</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrethealth/exposure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceUpdateHook(secretHookId, secretId, body, callback)</td>
    <td style="padding:15px">SecretHooksService_UpdateHook</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-detail/{pathv1}/hook/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceDeleteHook(secretHookId, secretId, callback)</td>
    <td style="padding:15px">SecretHooksService_DeleteHook</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-detail/{pathv1}/hook/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceGetHooks(secretId, callback)</td>
    <td style="padding:15px">SecretHooksService_GetHooks</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-detail/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceStubHook(scriptId, secretId, callback)</td>
    <td style="padding:15px">SecretHooksService_StubHook</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-detail/{pathv1}/hook/stub/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceGetHook(secretHookId, secretId, callback)</td>
    <td style="padding:15px">SecretHooksService_GetHook</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-detail/{pathv1}/hook/get/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceGetHooksV2(secretId, callback)</td>
    <td style="padding:15px">SecretHooksService_GetHooksV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-detail/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceStubHookV2(scriptId, secretId, callback)</td>
    <td style="padding:15px">SecretHooksService_StubHookV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-detail/{pathv1}/hook/stub/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceGetHookV2(secretHookId, secretId, callback)</td>
    <td style="padding:15px">SecretHooksService_GetHookV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-detail/{pathv1}/hook/get/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceCreateHook(secretId, body, callback)</td>
    <td style="padding:15px">SecretHooksService_CreateHook</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-detail/{pathv1}/hook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceCreateHookV2(secretId, body, callback)</td>
    <td style="padding:15px">SecretHooksService_CreateHookV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-detail/{pathv1}/hook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretHooksServiceUpdateHookV2(secretHookId, secretId, body, callback)</td>
    <td style="padding:15px">SecretHooksService_UpdateHookV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-detail/{pathv1}/hook/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPermissionsServiceGet(includeInactive, id, callback)</td>
    <td style="padding:15px">SecretPermissionsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPermissionsServiceUpdateSecretPermission(id, body, callback)</td>
    <td style="padding:15px">SecretPermissionsService_UpdateSecretPermission</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPermissionsServiceDelete(id, callback)</td>
    <td style="padding:15px">SecretPermissionsService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPermissionsServiceSearchSecretPermissions(filterDomainName, filterGroupId, filterGroupName, filterSecretId, filterUserId, filterUserName, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretPermissionsService_SearchSecretPermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPermissionsServiceAddSecretPermission(body, callback)</td>
    <td style="padding:15px">SecretPermissionsService_AddSecretPermission</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPermissionsServiceStub(secretId, callback)</td>
    <td style="padding:15px">SecretPermissionsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-permissions/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPermissionsServiceUpdateSecretShare(secretId, body, callback)</td>
    <td style="padding:15px">SecretPermissionsService_UpdateSecretShare</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/share?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPolicyServiceSearchSecretPolicies(filterIncludeInactive, filterSecretPolicyName, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretPolicyService_SearchSecretPolicies</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-policy/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPolicyServiceGetSecretPolicyStubV2(callback)</td>
    <td style="padding:15px">SecretPolicyService_GetSecretPolicyStubV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-policy/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPolicyServiceGetSecretPolicyV2(id, callback)</td>
    <td style="padding:15px">SecretPolicyService_GetSecretPolicyV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPolicyServiceUpdateSecretPolicyV2(id, body, callback)</td>
    <td style="padding:15px">SecretPolicyService_UpdateSecretPolicyV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPolicyServiceGetSecretPolicyAudit(id, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretPolicyService_GetSecretPolicyAudit</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-policy/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPolicyServiceGetSecretPolicyStatus(id, callback)</td>
    <td style="padding:15px">SecretPolicyService_GetSecretPolicyStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-policy/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretPolicyServiceCreateSecretPolicyV2(body, callback)</td>
    <td style="padding:15px">SecretPolicyService_CreateSecretPolicyV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateSecret(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateSecret</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceDelete(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetListFieldListDefinitions(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, slug, callback)</td>
    <td style="padding:15px">SecretsService_GetListFieldListDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/fields/{pathv2}/listdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateListFieldListDefinitions(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, slug, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateListFieldListDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/fields/{pathv2}/listdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceDeleteListFieldListDefinitions(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, listGuid, slug, callback)</td>
    <td style="padding:15px">SecretsService_DeleteListFieldListDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/fields/{pathv2}/listdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceSearchV2(filterAllowDoubleLocks, filterDoNotCalculateTotal, filterDoubleLockId, filterExtendedFields, filterExtendedTypeId, filterFolderId, filterHasLauncher, filterHeartbeatStatus, filterIncludeActive, filterIncludeInactive, filterIncludeRestricted, filterIncludeSubFolders, filterIsExactMatch, filterOnlyRPCEnabled, filterOnlySharedWithMe, filterPasswordTypeIds, filterPermissionRequired, filterScope, filterSearchField, filterSearchFieldSlug, filterSearchText, filterSecretTemplateIds, filterSiteId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretsService_SearchV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceSearchTotalV2(filterAllowDoubleLocks, filterDoNotCalculateTotal, filterDoubleLockId, filterExtendedFields, filterExtendedTypeId, filterFolderId, filterHasLauncher, filterHeartbeatStatus, filterIncludeActive, filterIncludeInactive, filterIncludeRestricted, filterIncludeSubFolders, filterIsExactMatch, filterOnlyRPCEnabled, filterOnlySharedWithMe, filterPasswordTypeIds, filterPermissionRequired, filterScope, filterSearchField, filterSearchFieldSlug, filterSearchText, filterSecretTemplateIds, filterSiteId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretsService_SearchTotalV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets/search-total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceSearchSecretLookup(filterAllowDoubleLocks, filterDoNotCalculateTotal, filterDoubleLockId, filterExtendedFields, filterExtendedTypeId, filterFolderId, filterHeartbeatStatus, filterIncludeActive, filterIncludeInactive, filterIncludeRestricted, filterIncludeSubFolders, filterIsExactMatch, filterOnlyRPCEnabled, filterOnlySharedWithMe, filterPasswordTypeIds, filterPermissionRequired, filterScope, filterSearchField, filterSearchFieldSlug, filterSearchText, filterSecretTemplateId, filterSiteId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretsService_SearchSecretLookup</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetLookup(id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_GetLookup</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/lookup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceStub(folderId, secretTemplateId, callback)</td>
    <td style="padding:15px">SecretsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretV2(includeInactive, noAutoCheckout, autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSummary(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_GetSummary</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetField(includeInactive, noAutoCheckout, autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, slug, callback)</td>
    <td style="padding:15px">SecretsService_GetField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/fields/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServicePutField(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, slug, body, callback)</td>
    <td style="padding:15px">SecretsService_PutField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/fields/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetListField(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, slug, callback)</td>
    <td style="padding:15px">SecretsService_GetListField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/fields/{pathv2}/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetFavorites(callback)</td>
    <td style="padding:15px">SecretsService_GetFavorites</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/favorite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretAudits(id, isExporting, filterIncludePasswordChangeLog, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretState(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretState</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetGeneral(id, isEditMode, loadReadOnlyFlags, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_GetGeneral</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/secret-detail/{pathv1}/general/{isEditMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretSettings(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretSettings</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetActiveSecretSessions(filterSecretId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretsService_GetActiveSecretSessions</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/launcher-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateSecretSession(body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateSecretSession</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/launcher-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSshRestrictedCommands(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, callback)</td>
    <td style="padding:15px">SecretsService_GetSshRestrictedCommands</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/ssh-restricted-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateGeneral(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateGeneral</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/general?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateEmail(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateEmail</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateSecurity(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateSecurity</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/security-general?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateGeneralV2(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateGeneralV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets/{pathv1}/general?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateEmailV2(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateEmailV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets/{pathv1}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateSecurityCheckoutV3(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateSecurityCheckoutV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/secrets/{pathv1}/security-checkout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateSecurityV2(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateSecurityV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets/{pathv1}/security-general?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretExtendedSearchDetails(body, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretExtendedSearchDetails</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/extended-search-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetRestricted(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_GetRestricted</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/restricted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceCreateSecret(body, callback)</td>
    <td style="padding:15px">SecretsService_CreateSecret</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceRunHeartBeat(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_RunHeartBeat</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/heartbeat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceExpire(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_Expire</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/expire?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceChangePassword(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_ChangePassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/change-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceCheckIn(id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_CheckIn</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/check-in?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceCheckOut(id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_CheckOut</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/check-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceExtendCheckOut(id, body, callback)</td>
    <td style="padding:15px">SecretsService_ExtendCheckOut</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/extend-check-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceRestrictedField(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, slug, body, callback)</td>
    <td style="padding:15px">SecretsService_RestrictedField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/restricted/fields/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretRdpProxyInfo(autoCheckIn, autoCheckout, autoComment, forceCheckIn, body, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretRdpProxyInfo</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/rdpproxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretSshProxyInfo(autoCheckIn, autoCheckout, autoComment, forceCheckIn, body, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretSshProxyInfo</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/sshproxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceGetSecretSshTerminalDetails(autoCheckIn, autoCheckout, autoComment, forceCheckIn, body, callback)</td>
    <td style="padding:15px">SecretsService_GetSecretSshTerminalDetails</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/sshterminal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceFavorite(secretId, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_Favorite</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/favorite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceStopPasswordChange(id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_StopPasswordChange</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/stop-password-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceExportSecrets(body, callback)</td>
    <td style="padding:15px">SecretsService_ExportSecrets</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateRpcScriptSecrets(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateRpcScriptSecrets</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/rpc-script-secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUndeleteSecret(id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_UndeleteSecret</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateRpcScriptSecretsV2(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateRpcScriptSecretsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets/{pathv1}/rpc-script-secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateSecurityApprovalV3(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateSecurityApprovalV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/secrets/{pathv1}/security-approval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUndeleteSecretV2(id, secretPath, callback)</td>
    <td style="padding:15px">SecretsService_UndeleteSecretV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secrets/{pathv1}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateExpiration(autoCheckIn, autoCheckout, autoComment, forceCheckIn, id, secretPath, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateExpiration</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/expiration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateSshRestrictedCommands(autoCheckIn, autoCheckout, autoComment, forceCheckIn, secretId, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateSshRestrictedCommands</td>
    <td style="padding:15px">{base_path}/{version}/v1/secrets/{pathv1}/update-ssh-restricted-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretsServiceUpdateJumpboxRouteSelection(secretId, body, callback)</td>
    <td style="padding:15px">SecretsService_UpdateJumpboxRouteSelection</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-detail/{pathv1}/jumpbox-route-selection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretServerSettingsServiceGetExport(loadAdvancedSettings, loadAll, loadApplicationSettings, loadEmail, loadFolderSettings, loadLauncherSettings, loadLicenses, loadLocalUserPasswords, loadLogin, loadPermissionOptions, loadProtocolHandlerSettings, loadSaml, loadSecurity, loadSessionRecording, loadSshCommands, loadTicketSystem, loadUserExperience, loadUserInterface, callback)</td>
    <td style="padding:15px">SecretServerSettingsService_GetExport</td>
    <td style="padding:15px">{base_path}/{version}/v1/secretserversettings/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretServerSettingsServiceGetExportStub(callback)</td>
    <td style="padding:15px">SecretServerSettingsService_GetExportStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/secretserversettings/export/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretServerSettingsServiceGetExportImportCapabilities(body, callback)</td>
    <td style="padding:15px">SecretServerSettingsService_GetExportImportCapabilities</td>
    <td style="padding:15px">{base_path}/{version}/v1/secretserversettings/capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretServerSettingsServiceImportSetting(body, callback)</td>
    <td style="padding:15px">SecretServerSettingsService_ImportSetting</td>
    <td style="padding:15px">{base_path}/{version}/v1/secretserversettings/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretSessionsServiceSearchSessions(filterActive, filterDateRange, filterEndDate, filterEndTime, filterFolderId, filterGroupIds, filterIncludeNonSecretServerSessions, filterIncludeOnlyLaunchedSuccessfully, filterIncludeRestricted, filterIncludeSubFolders, filterLauncherTypeId, filterSearchText, filterSearchTypes, filterSecretIds, filterSiteId, filterStartDate, filterStartTime, filterUserIds, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretSessionsService_SearchSessions</td>
    <td style="padding:15px">{base_path}/{version}/v1/recorded-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretSessionsServiceGet(id, callback)</td>
    <td style="padding:15px">SecretSessionsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/recorded-sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretSessionsServiceGetSummary(id, callback)</td>
    <td style="padding:15px">SecretSessionsService_GetSummary</td>
    <td style="padding:15px">{base_path}/{version}/v1/recorded-sessions/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretSessionsServiceSearchPointsOfInterest(id, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretSessionsService_SearchPointsOfInterest</td>
    <td style="padding:15px">{base_path}/{version}/v1/recorded-sessions/{pathv1}/points-of-interest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretSessionsServiceSearchPointsOfInterestSummary(id, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretSessionsService_SearchPointsOfInterestSummary</td>
    <td style="padding:15px">{base_path}/{version}/v1/recorded-sessions/{pathv1}/points-of-interest-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretSessionsServiceGetSessionRecording(id, callback)</td>
    <td style="padding:15px">SecretSessionsService_GetSessionRecording</td>
    <td style="padding:15px">{base_path}/{version}/v1/recorded-sessions/{pathv1}/session-recordings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretSessionsServiceProcessSession(id, callback)</td>
    <td style="padding:15px">SecretSessionsService_ProcessSession</td>
    <td style="padding:15px">{base_path}/{version}/v1/recorded-sessions/{pathv1}/request-processing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatePermissionsServiceSearchTemplatePermissions(filterGroupId, filterTemplateId, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatePermissionsService_SearchTemplatePermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-template-permissions/grouped?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatePermissionsServiceSearch(filterGroupId, filterSearchText, filterSecretTypeId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatePermissionsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-template-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatePermissionsServiceUpdate(body, callback)</td>
    <td style="padding:15px">SecretTemplatePermissionsService_Update</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-template-permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatePermissionsServiceGetTemplatePermissionRoles(callback)</td>
    <td style="padding:15px">SecretTemplatePermissionsService_GetTemplatePermissionRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-template-permissions/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatePermissionsServiceUpdateTemplatePermissions(secretTypeId, body, callback)</td>
    <td style="padding:15px">SecretTemplatePermissionsService_UpdateTemplatePermissions</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-template-permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceDisableField(templateFieldId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_DisableField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetPasswordDictionary(id, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetPasswordDictionary</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/password-requirements/password-dictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceDeletePasswordDictionary(id, callback)</td>
    <td style="padding:15px">SecretTemplatesService_DeletePasswordDictionary</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/password-requirements/password-dictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetSecretTemplateExtendedMapping(extendedTypeId, secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetSecretTemplateExtendedMapping</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/extended-mappings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceCreateSecretTemplateExtendedMapping(extendedTypeId, secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_CreateSecretTemplateExtendedMapping</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/extended-mappings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceDeleteExtendedMapping(extendedTypeId, secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_DeleteExtendedMapping</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/extended-mappings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceUpdateSecretTemplateExtendedMapping(extendedTypeId, secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_UpdateSecretTemplateExtendedMapping</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/extended-mappings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetSecretTemplateLauncher(launcherTypeId, secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetSecretTemplateLauncher</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/launchers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceCreateSecretTemplateLauncher(launcherTypeId, secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_CreateSecretTemplateLauncher</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/launchers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceDeleteSecretTemplateLauncher(launcherTypeId, secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_DeleteSecretTemplateLauncher</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/launchers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceUpdateSecretTemplateLauncher(launcherTypeId, secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_UpdateSecretTemplateLauncher</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/launchers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceSearch(filterIncludeInactive, filterIncludeSecretCount, filterPasswordTypeIds, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatesService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceCreateTemplate(body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_CreateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceExport(id, callback)</td>
    <td style="padding:15px">SecretTemplatesService_Export</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetPasswordDictionaries(callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetPasswordDictionaries</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/password-requirements/password-dictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceUpdatePasswordDictionary(body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_UpdatePasswordDictionary</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/password-requirements/password-dictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceAddPasswordDictionary(body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_AddPasswordDictionary</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/password-requirements/password-dictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceSearchSecretTemplateExtendedTypes(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatesService_SearchSecretTemplateExtendedTypes</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/extended-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceSearchSecretTemplateExtendedMappings(filterSecretTemplateId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatesService_SearchSecretTemplateExtendedMappings</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/extended-mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceStubSecretTemplateExtendedMapping(extendedTypeId, secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_StubSecretTemplateExtendedMapping</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/extended-mappings/{pathv2}/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceSearchLauncherTypes(filterApplicationName, filterIncludeInactive, filterIncludeSystemLaunchers, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatesService_SearchLauncherTypes</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/launcher-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceSearchSecretTemplateLaunchers(filterSecretTemplateId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatesService_SearchSecretTemplateLaunchers</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/launchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceStubSecretTemplateLauncher(launcherTypeId, secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_StubSecretTemplateLauncher</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/launchers/{pathv2}/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetSecretTemplatePasswordType(secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetSecretTemplatePasswordType</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/password-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServicePatchSecretTemplatePasswordChanger(secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_PatchSecretTemplatePasswordChanger</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/password-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetTemplates(folderId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetTemplates</td>
    <td style="padding:15px">{base_path}/{version}/v1/templates/{folderId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetV2(secretTemplateId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServicePatchTemplateV2(secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_PatchTemplateV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/secret-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceSearchTemplateFields(filterIncludeInactive, filterSecretTemplateId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecretTemplatesService_SearchTemplateFields</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/fields/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceStubTemplateField(callback)</td>
    <td style="padding:15px">SecretTemplatesService_StubTemplateField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/fields/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGetTemplateField(secretFieldId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GetTemplateField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServicePatchTemplateField(secretTemplateFieldId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_PatchTemplateField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceCopy(id, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_Copy</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/copy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServicePut(templateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_Put</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceCreateField(templateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_CreateField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceGeneratePassword(secretfieldId, callback)</td>
    <td style="padding:15px">SecretTemplatesService_GeneratePassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/generate-password/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceImportSecretTemplate(body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_ImportSecretTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceCreateTemplateField(secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_CreateTemplateField</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceSortTemplateFields(secretTemplateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_SortTemplateFields</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/{pathv1}/fields/sort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">secretTemplatesServiceUpdateSecretTemplatePasswordType(templateId, body, callback)</td>
    <td style="padding:15px">SecretTemplatesService_UpdateSecretTemplatePasswordType</td>
    <td style="padding:15px">{base_path}/{version}/v1/secret-templates/password-type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityAuditLogsServiceSearchLogs(filterIsExporting, filterSearchText, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SecurityAuditLogsService_SearchLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/security-audit-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverNodesServiceGetList(callback)</td>
    <td style="padding:15px">ServerNodesService_GetList</td>
    <td style="padding:15px">{base_path}/{version}/v1/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverNodesServiceGet(nodeId, callback)</td>
    <td style="padding:15px">ServerNodesService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serverNodesServiceUpdateNodeConfiguration(nodeId, body, callback)</td>
    <td style="padding:15px">ServerNodesService_UpdateNodeConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/v1/nodes/{pathv1}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sitesServiceGet(includeInactive, callback)</td>
    <td style="padding:15px">SitesService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">slackServiceGetConfigurationV3(callback)</td>
    <td style="padding:15px">SlackService_GetConfigurationV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/slack-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">slackServiceUpdateConfigurationV3(body, callback)</td>
    <td style="padding:15px">SlackService_UpdateConfigurationV3</td>
    <td style="padding:15px">{base_path}/{version}/v3/slack-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">slackServiceSendTestSlackMessage(callback)</td>
    <td style="padding:15px">SlackService_SendTestSlackMessage</td>
    <td style="padding:15px">{base_path}/{version}/v2/slack-test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">slackServiceHandleEvent(callback)</td>
    <td style="padding:15px">SlackService_HandleEvent</td>
    <td style="padding:15px">{base_path}/{version}/v1/slack/event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">slackServiceHandleInteraction(callback)</td>
    <td style="padding:15px">SlackService_HandleInteraction</td>
    <td style="padding:15px">{base_path}/{version}/v1/slack/interaction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandServiceGetSshCommand(id, callback)</td>
    <td style="padding:15px">SshCommandService_GetSshCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandServiceGetSshCommands(filterCommandPermissionType, filterNameOrCommand, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SshCommandService_GetSshCommands</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandServiceGetSshCommandStub(callback)</td>
    <td style="padding:15px">SshCommandService_GetSshCommandStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandServiceUpdateSshCommand(sshCommandId, body, callback)</td>
    <td style="padding:15px">SshCommandService_UpdateSshCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandServiceCreateSshCommand(body, callback)</td>
    <td style="padding:15px">SshCommandService_CreateSshCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandBlocklistServiceGetSshCommandBlocklist(id, callback)</td>
    <td style="padding:15px">SshCommandBlocklistService_GetSshCommandBlocklist</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-blocklist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandBlocklistServiceGetSshCommandBlocklists(filterIncludeActive, filterIncludeInactive, filterName, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SshCommandBlocklistService_GetSshCommandBlocklists</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-blocklist/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandBlocklistServiceGetSshCommandBlocklistStub(callback)</td>
    <td style="padding:15px">SshCommandBlocklistService_GetSshCommandBlocklistStub</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-blocklist/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandBlocklistServiceGetSshCommandBlocklistPolicies(filterSshCommandBlocklistId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SshCommandBlocklistService_GetSshCommandBlocklistPolicies</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-blocklist/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandBlocklistServicePatchSshCommandBlocklist(sshCommandBlocklistId, body, callback)</td>
    <td style="padding:15px">SshCommandBlocklistService_PatchSshCommandBlocklist</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-blocklist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandBlocklistServiceCreateSshCommandBlocklist(body, callback)</td>
    <td style="padding:15px">SshCommandBlocklistService_CreateSshCommandBlocklist</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-blocklist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandMenuServiceSearchSshCommandMenu(filterIncludeDisabled, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SshCommandMenuService_SearchSshCommandMenu</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-menu/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandMenuServiceSearchAudits(isExporting, filterSshCommandMenuName, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SshCommandMenuService_SearchAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-menu/audit/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandMenuServiceSearchItemAudits(isExporting, filterSshCommandItemNameOrNotes, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">SshCommandMenuService_SearchItemAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-item/audit/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandMenuServiceGetSshCommandMenu(sshCommandMenuId, callback)</td>
    <td style="padding:15px">SshCommandMenuService_GetSshCommandMenu</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-menu/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandMenuServicePatchSshCommandMenu(sshCommandMenuId, body, callback)</td>
    <td style="padding:15px">SshCommandMenuService_PatchSshCommandMenu</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-menu/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandMenuServiceGetSshCommandState(callback)</td>
    <td style="padding:15px">SshCommandMenuService_GetSshCommandState</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-menu/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sshCommandMenuServiceAddSshCommandMenu(body, callback)</td>
    <td style="padding:15px">SshCommandMenuService_AddSshCommandMenu</td>
    <td style="padding:15px">{base_path}/{version}/v1/ssh-command-menu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceSearch(filterIncludeInactive, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">TeamsService_Search</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceCreateTeam(body, callback)</td>
    <td style="padding:15px">TeamsService_CreateTeam</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceStub(callback)</td>
    <td style="padding:15px">TeamsService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceGet(id, callback)</td>
    <td style="padding:15px">TeamsService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceUpdateTeam(id, body, callback)</td>
    <td style="padding:15px">TeamsService_UpdateTeam</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceGetTeamAudits(id, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">TeamsService_GetTeamAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceGetTeamMembers(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">TeamsService_GetTeamMembers</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceAddTeamMember(id, body, callback)</td>
    <td style="padding:15px">TeamsService_AddTeamMember</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceGetTeamSites(id, includeInactive, callback)</td>
    <td style="padding:15px">TeamsService_GetTeamSites</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceAddTeamSite(id, body, callback)</td>
    <td style="padding:15px">TeamsService_AddTeamSite</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceGetTeamLists(id, callback)</td>
    <td style="padding:15px">TeamsService_GetTeamLists</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">teamsServiceAddListsToTeam(id, body, callback)</td>
    <td style="padding:15px">TeamsService_AddListsToTeam</td>
    <td style="padding:15px">{base_path}/{version}/v1/teams/{pathv1}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketSystemsServiceGetTicketSystems(includeInactive, callback)</td>
    <td style="padding:15px">TicketSystemsService_GetTicketSystems</td>
    <td style="padding:15px">{base_path}/{version}/v1/ticket-systems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketSystemsServiceCreateTicketSystem(body, callback)</td>
    <td style="padding:15px">TicketSystemsService_CreateTicketSystem</td>
    <td style="padding:15px">{base_path}/{version}/v1/ticket-systems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketSystemsServiceGetTicketSystem(id, callback)</td>
    <td style="padding:15px">TicketSystemsService_GetTicketSystem</td>
    <td style="padding:15px">{base_path}/{version}/v1/ticket-systems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketSystemsServiceUpdateTicketSystem(id, body, callback)</td>
    <td style="padding:15px">TicketSystemsService_UpdateTicketSystem</td>
    <td style="padding:15px">{base_path}/{version}/v1/ticket-systems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketSystemsServiceGetTicketSystemsV2(includeInactive, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">TicketSystemsService_GetTicketSystemsV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ticket-systems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketSystemsServiceGetTicketSystemV2(id, callback)</td>
    <td style="padding:15px">TicketSystemsService_GetTicketSystemV2</td>
    <td style="padding:15px">{base_path}/{version}/v2/ticket-systems/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGet(includeInactive, id, callback)</td>
    <td style="padding:15px">UsersService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceUpdateUser(id, body, callback)</td>
    <td style="padding:15px">UsersService_UpdateUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceDelete(id, callback)</td>
    <td style="padding:15px">UsersService_Delete</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServicePatchUser(id, body, callback)</td>
    <td style="padding:15px">UsersService_PatchUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetUserOwner(id, ownerId, callback)</td>
    <td style="padding:15px">UsersService_GetUserOwner</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/owners/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceDeleteUserOwner(id, ownerId, callback)</td>
    <td style="padding:15px">UsersService_DeleteUserOwner</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/owners/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetRoles(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, id, callback)</td>
    <td style="padding:15px">UsersService_GetRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceUpdateUserRoles(id, body, callback)</td>
    <td style="padding:15px">UsersService_UpdateUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceCreateUserRoles(id, body, callback)</td>
    <td style="padding:15px">UsersService_CreateUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceDeleteUserRoles(id, callback)</td>
    <td style="padding:15px">UsersService_DeleteUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetUserGroups(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_GetUserGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceUpdateUserGroups(id, body, callback)</td>
    <td style="padding:15px">UsersService_UpdateUserGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceAddUserToGroups(id, body, callback)</td>
    <td style="padding:15px">UsersService_AddUserToGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceRemoveUserGroups(groupIds, id, callback)</td>
    <td style="padding:15px">UsersService_RemoveUserGroups</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceSearchUsers(filterDomainId, filterExcludeInboxRuleIdSubscribers, filterIncludeInactive, filterSearchFields, filterSearchText, filterUserIds, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_SearchUsers</td>
    <td style="padding:15px">{base_path}/{version}/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceCreateUser(body, callback)</td>
    <td style="padding:15px">UsersService_CreateUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetCurrentUser(callback)</td>
    <td style="padding:15px">UsersService_GetCurrentUser</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceLookup(filterDomainId, filterExcludeInboxRuleIdSubscribers, filterIncludeInactive, filterSearchFields, filterSearchText, filterUserIds, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_Lookup</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceStub(callback)</td>
    <td style="padding:15px">UsersService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetUserPublicSshKeys(filterIncludeExpired, filterIncludeInactive, filterSearchText, filterUserId, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_GetUserPublicSshKeys</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/public-ssh-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceCreateUserPublicSshKey(body, callback)</td>
    <td style="padding:15px">UsersService_CreateUserPublicSshKey</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/public-ssh-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceSearchUserOwners(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_SearchUserOwners</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/owners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceAddUserOwner(id, body, callback)</td>
    <td style="padding:15px">UsersService_AddUserOwner</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/owners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServicePatchUserOwners(id, body, callback)</td>
    <td style="padding:15px">UsersService_PatchUserOwners</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/owners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetDomains(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_GetDomains</td>
    <td style="padding:15px">{base_path}/{version}/v1/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetUserRoles(skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, userId, callback)</td>
    <td style="padding:15px">UsersService_GetUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/roles-assigned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetSiteAudits(isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, userId, callback)</td>
    <td style="padding:15px">UsersService_GetSiteAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetUserActionAudits(actions, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_GetUserActionAudits</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/action/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetUserTeams(filterIncludeGroupMemberships, filterIncludeInactive, filterSearchTerm, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, userId, callback)</td>
    <td style="padding:15px">UsersService_GetUserTeams</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetPreference(isLegacy, settingCode, settingName, callback)</td>
    <td style="padding:15px">UsersService_GetPreference</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/preference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceUpdatePreference(body, callback)</td>
    <td style="padding:15px">UsersService_UpdatePreference</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/preference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceGetCurrentUserSessions(isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">UsersService_GetCurrentUserSessions</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceDeactivateUserPublicSshKey(id, callback)</td>
    <td style="padding:15px">UsersService_DeactivateUserPublicSshKey</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/public-ssh-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceChangePassword(body, callback)</td>
    <td style="padding:15px">UsersService_ChangePassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/change-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceVerifyPassword(body, callback)</td>
    <td style="padding:15px">UsersService_VerifyPassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/verify-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceResetTwoFactor(userId, body, callback)</td>
    <td style="padding:15px">UsersService_ResetTwoFactor</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/reset-two-factor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceResetUserPassword(userId, body, callback)</td>
    <td style="padding:15px">UsersService_ResetUserPassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/password-reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceUserPersonalInfoDeleteCommand(id, callback)</td>
    <td style="padding:15px">UsersService_UserPersonalInfoDeleteCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/delete-pii/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceLockOut(userId, body, callback)</td>
    <td style="padding:15px">UsersService_LockOut</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/{pathv1}/lock-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceTerminateCurrentUserSessions(body, callback)</td>
    <td style="padding:15px">UsersService_TerminateCurrentUserSessions</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/sessions/terminate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">usersServiceSetUserDoubleLockPassword(body, callback)</td>
    <td style="padding:15px">UsersService_SetUserDoubleLockPassword</td>
    <td style="padding:15px">{base_path}/{version}/v1/users/doublelock-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">versionServiceGet(callback)</td>
    <td style="padding:15px">VersionService_Get</td>
    <td style="padding:15px">{base_path}/{version}/v1/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowInstancesServiceGetByTemplateId(id, callback)</td>
    <td style="padding:15px">WorkflowInstancesService_GetByTemplateId</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/instances/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowStepTemplatesServiceGetTemplateSteps(id, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">WorkflowStepTemplatesService_GetTemplateSteps</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/steps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowStepTemplatesServiceUpdateStep(id, body, callback)</td>
    <td style="padding:15px">WorkflowStepTemplatesService_UpdateStep</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/steps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowStepTemplatesServiceCreateStep(id, body, callback)</td>
    <td style="padding:15px">WorkflowStepTemplatesService_CreateStep</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/steps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowStepTemplatesServiceStub(callback)</td>
    <td style="padding:15px">WorkflowStepTemplatesService_Stub</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/steps/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowStepTemplatesServiceGetTemplateStep(id, stepNum, callback)</td>
    <td style="padding:15px">WorkflowStepTemplatesService_GetTemplateStep</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/steps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowStepTemplatesServiceUpdateStepModel(id, stepNum, body, callback)</td>
    <td style="padding:15px">WorkflowStepTemplatesService_UpdateStepModel</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/steps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceSearchWorkflowTemplates(filterIncludeInactive, filterWorkflowType, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_SearchWorkflowTemplates</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceCreateWorkflowTemplate(body, callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_CreateWorkflowTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceStubWorkflowTemplate(callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_StubWorkflowTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceGetTemplate(id, callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_GetTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceUpdateWorkflowTemplate(id, body, callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_UpdateWorkflowTemplate</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceSearchTemplateAudit(id, isExporting, skip, sortBy0Direction, sortBy0Name, sortBy0Priority, take, callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_SearchTemplateAudit</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/audits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceGetWorkflowEntities(id, includeAll, callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_GetWorkflowEntities</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/entities/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">workflowTemplatesServiceCancelRequest(id, callback)</td>
    <td style="padding:15px">WorkflowTemplatesService_CancelRequest</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/templates/{pathv1}/cancel-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
