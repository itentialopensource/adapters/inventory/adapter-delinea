# Adapter for Delinea

Vendor: Delinea
Homepage: https://delinea.com/

Product: Secret Server
Product Page: https://delinea.com/products/secret-server

## Introduction
We classify Delinea into the Security (SASE) domain as Delinea provides the capability to get information about and make changes to credentials, accounts, access, etc. 

"Delinea Secret Server empowers security and IT ops teams to secure and manage all types of privileged accounts" 

## Why Integrate
The Delinea adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Delinea Secret Server. With this adapter you have the ability to perform operations such as:

- Discover, Manage, Protect and Audit privileged account access.

## Additional Product Documentation
The [API documents for Delinea](https://docs.delinea.com/online-help/secret-server/api-scripting/rest-api-reference-download/index.htm)
