
## 2.2.4 [10-14-2024]

* Changes made at 2024.10.14_19:44PM

See merge request itentialopensource/adapters/adapter-delinea!15

---

## 2.2.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-delinea!13

---

## 2.2.2 [08-14-2024]

* Changes made at 2024.08.14_17:50PM

See merge request itentialopensource/adapters/adapter-delinea!12

---

## 2.2.1 [08-07-2024]

* Changes made at 2024.08.06_18:46PM

See merge request itentialopensource/adapters/adapter-delinea!11

---

## 2.2.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-delinea!10

---

## 2.1.6 [03-26-2024]

* Changes made at 2024.03.26_14:40PM

See merge request itentialopensource/adapters/inventory/adapter-delinea!9

---

## 2.1.5 [03-21-2024]

* Changes made at 2024.03.21_14:40PM

See merge request itentialopensource/adapters/inventory/adapter-delinea!8

---

## 2.1.4 [03-13-2024]

* Changes made at 2024.03.13_14:00PM

See merge request itentialopensource/adapters/inventory/adapter-delinea!7

---

## 2.1.3 [03-11-2024]

* Changes made at 2024.03.11_14:09PM

See merge request itentialopensource/adapters/inventory/adapter-delinea!6

---

## 2.1.2 [02-26-2024]

* Changes made at 2024.02.26_13:36PM

See merge request itentialopensource/adapters/inventory/adapter-delinea!5

---

## 2.1.1 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/inventory/adapter-delinea!4

---

## 2.1.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-delinea!3

---

## 2.0.1 [04-19-2023]

* Update authentication

See merge request itentialopensource/adapters/inventory/adapter-delinea!2

---

## 2.0.0 [04-04-2023]

* Major/adapt 2571

See merge request itentialopensource/adapters/inventory/adapter-delinea!1

---

## 1.0.0 [02-10-2023]

* Major/adapt 2571

See merge request itentialopensource/adapters/inventory/adapter-delinea!1

---

## 0.1.1 [01-04-2023]

* Bug fixes and performance improvements

See commit d502167

---
