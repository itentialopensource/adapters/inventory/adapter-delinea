
## 2.0.1 [04-19-2023]

* Update authentication

See merge request itentialopensource/adapters/inventory/adapter-delinea!2

---

## 2.0.0 [04-04-2023]

* Major/adapt 2571

See merge request itentialopensource/adapters/inventory/adapter-delinea!1

---

## 1.0.0 [02-10-2023]

* Major/adapt 2571

See merge request itentialopensource/adapters/inventory/adapter-delinea!1

---

## 0.1.1 [01-04-2023]

* Bug fixes and performance improvements

See commit d502167

---
